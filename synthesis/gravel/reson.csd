<CsoundSynthesizer>
; vi:set ts=4 nowrap:
;
; $Id$
;
<CsInstruments>
	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

	instr	1

asig	oscil	20000,5000,1
kamp	expon	1,p3/10,0.000001
asigk	=		asig*kamp
aout	reson	asigk,500,4
aout	balance	aout,asig

		out	aout
	endin

</CsInstruments>
; ==============================================
<CsScore>
f1 0 4096 11 60 1 1

i1 0 10
</CsScore>
</CsoundSynthesizer>

