; vi:set nowrap ts=4:
;
; r-voices.orc,v 0.14 2000/06/26 07:29:06 nicb Exp
;
; THESE COMMENTS ARE OUTDATED
;
; 		Global organization:
;
;                         instr 100
;      (soundfile readers - writes into 50 different loc)
;                             |
;                         instr 150
;               (50 mixers envelope flatteners)
;                             |	
;                             |
;                     +-------+------------------+
;                     |                          |
;            zaw 0-39 (choir)           zaw 40-49 (solos)
;                     |                          |
;               instr 200-239                    |
;         (40 movement interpolators)            |
;                     |                          |
;     zkw 0-39(amp),40-79(x),80-119(y)           |
;                     |                          |
;             +-------+-------+           instr 400-409
;             |               |               (babo)
;             |               |                  |
;             v               v                  |
;       instr 300-339     instr 500              |
;          (room)           (rev)                v
;
; 
;
	sr=44100
	kr=44100
	ksmps=1
	nchnls=2

;
; the zak a variables hold the variables for the audio outputs
; of instruments 1-50, like this:
;
; zar[0]  = instr 1 audio output
; ...
; zar[49] = instr 50 audio output
;
; the zak k variables hold the position variables in the
; following way:
;
; zkr[0] = instr 200 (instr 1 dynamic and position controller) cur amp
; ...
; zkr[39]= instr 239 (instr 40 dynamic and position controller) cur amp
; zkr[40]= instr 200 (instr 1 dynamic and position controller) cur x position
; ...
; zkr[79]= instr 239 (instr 40 dynamic and position controller) cur x position
; zkr[80]= instr 200 (instr 1 dynamic and position controller) cur y position
; ...
; zkr[119]=instr 239 (instr 40 dynamic and position controller) cur y position
;
;
zakinit					150,140
garevl			init	0
garevr			init	0
givel			init	340

giampoffset		init	 1
gixposoff		init	40
giyposoff		init	80

giroomfunc		init	1
gibabofunoff	init	2
giaudioidx		init	50

	;
	; zak system (movement) initialization system
	; (50 instruments)
	;
	instr   1,  2,  3,  4,  5,  6,  7,  8,  9, 10, \
	       11, 12, 13, 14, 15, 16, 17, 18, 19, 20, \
		   21, 22, 23, 24, 25, 26, 27, 28, 29, 30, \
		   31, 32, 33, 34, 35, 36, 37, 38, 39, 40, \
		   41, 42, 43, 44, 45, 46, 47, 48, 49, 50
	ifirst init 1	; first instrument number
	ixstart=p4
	ixend=p5
	iystart=p6
	iyend=p7
	ixindex=p1+gixposoff-ifirst
	iyindex=p1+giyposoff-ifirst

kx	line	ixstart,p3,ixend
ky	line	iystart,p3,iyend

	zkw	kx,ixindex
	zkw	ky,iyindex

	endin

	;
	; soundin reader
	;
	instr 100
	ifirst	init	100			; first instrument number
	index=p1-ifirst
	iamp=ampdb(0)
	izerodB=32767*ampdb(-36)
	isound=p4
	iskip=p5
	ifunc=p6
	index=p7+giaudioidx

	;
	; file reading section
	;
kamp	oscil1i	0,iamp,p3,ifunc
acomp	=		kamp*izerodB
aout	soundin	isound,iskip	

	;
	; amplitude equalization section
	;
ilort	=		10				; low  response rate
ihirt	=		200				; high response rate
ilowp	=		0.25/ihirt		; low-pass filtering time
iexpd	=		ampdb(+6)
icomp	=		ampdb(-6)
kdiff	init	1
kcomp	rms		acomp,ilort
ksig	rms		aout,ilort
kdiff	=		kcomp/ksig
kdiff	limit	kdiff,icomp,iexpd
kdiff	port	kdiff,ilowp

aout	=		aout*kdiff
aout	=		aout*kamp

		zaw	aout,index
	
	endin

	;
	; mixers
	;
	instr 150
	izerodB=32767
	ioutindex=p4
	input1=p5+giaudioidx
	input2=p6+giaudioidx
	input3=p7+giaudioidx

a1		zar		input1
a2		zar		input2
a3		zar		input3

aout	=		a1+a2+a3

	;
	; amplitude eq section
	;
ilort	=	5					; low  response rate
ihirt	=	100					; high response rate
ilowp	=	0.25/ihirt			; low-pass filtering time
iexpd	=	ampdb(+6)
icomp	=	ampdb(-3)
kdiff	init	1
kslow	rms		aout,ilort
kfast	rms		aout,ihirt
kdiff	=		kslow/kfast
kdiff	limit	kdiff,icomp,iexpd
kdiff	port	kdiff,ilowp

;ktime	times
;		dumpk4	ktime,kslow,kfast,kdiff,"ampeq-1.log",8,0.01

aout	=		aout*kdiff
aout	dcblock	aout

		zaw		aout,ioutindex

	endin

	;
	; linear amplitude controller (30 instruments)
	;
	instr	200,201,202,203,204,205,206,207,208,209, \
	        210,211,212,213,214,215,216,217,218,219, \
			220,221,222,223,224,225,226,227,228,229, \
			230,231,232,233,234,235,236,237,238,239, \
			240,241,242,243,244,245,246,247,248,249
	ifirst	init	200			; first instrument number
	izerodB=ampdb(0)
	istart=izerodB*ampdb(p4)
	iend=izerodB*ampdb(p5)
	iampindex=p1-ifirst

	;
	;	interpolate
	;
kamp	line	istart,p3,iend
	;
	;	output
	;
		zkw		kamp,iampindex

	endin

	;-------------------------------------------
	;	    ROOM INSTRUMENTS
	;-------------------------------------------

	;
	; 
	instr 300,301,302,303,304,305,306,307,308,309, \
	      310,311,312,313,314,315,316,317,318,319, \
		  320,321,322,323,324,325,326,327,328,329, \
		  330,331,332,333,334,335,336,337,338,339, \
		  340,341,342,343,344,345,346,347,348,349
	ifirst	init	300				; first instrument number
	;
	; input indexings
	;
	iaindex=p1-ifirst				; audio channels
	iampindex=p1-ifirst				; amplitude controls
	ixindex=iampindex+gixposoff		; x position controls
	iyindex=iampindex+giyposoff		; y position controls
	;
	; p4	 p5
	;atten attnrev
	;
	idur  = p3
	iattadir = ampdb(p4)			; attenuazione di ampiezza [0]
 	iattarev = ampdb(p5)			; attenuazione riverbero [-30]
	iroom = giroomfunc				; table room def (set by engines)
	;
	; audio input
	;
asend	zar	iaindex					; signal
kamp	zkr	iampindex				; amplitude envelope

asig	=		asend*iattadir*kamp	;
asend	=		0
		zacl	iaindex,iaindex		; clear input

kx		zkr		ixindex				; coming from the movement engines
ky		zkr		iyindex				; coming from the movement engines

	                ;DEFINIZIONE INNER ROOM
	ixl   table 0,iroom     ;coordinate(in m) altoparlante
	iyl   table 1,iroom		;sinistro 
	ixr   table 2,iroom     ;coordinate(in m) altoparlante 
	iyr   table 3,iroom     ;destro       
	                ;DEFINIZIONE OUTER ROOM       
	ixmax table 4,iroom     ;larghezza max positiva (in m) 
	ixmin table 5,iroom     ;larghezza max negativa (in m) 
	iymax table 6,iroom     ;lunghezza max positiva (in m)
	iymin table 7,iroom     ;lunghezza max negativa (in m) 
	ix    table 8,iroom		; max coordinata x della sorgente
	iy    table 9,iroom		; max coordinata y della sorgente
	print ixl,iyl,ixr,iyr,ixmax,ixmin,iymax,iymin
 
	ideltaf = sr/2 - 2000
	kfilt = sr/2 + ideltaf*ky/iy
	af0 tone asig, kfilt 
    
	asigf = (ky >= 0 ? asig : af0) 
    
	k1     = kx-ixl                 
	k2     = ky-iyl 
	k3     = kx-ixr
	k4     = ky-iyr 
	k1q    = k1*k1
	k2q    = k2*k2
	k3q    = k3*k3
	k4q    = k4*k4     
	kxmax  = 2*(ixmax-kx)
	kymax  = 2*(iymax-ky)
	kxmin  = 2*(kx-ixmin)
	kymin  = 2*(ky-iymin)
	                          ;CALCOLO SEGNALE DIRETTO 
	kdl = sqrt(k1q+k2q)       ;dist dir>L
	kdr = sqrt(k3q+k4q)       ;dist dir>R
	
;i muri sono numerati in senso orario a partire da quello di fronte 
;all'ascoltatore (m1)
	
	k5  = k2+kymax            ;CALCOLO RIFLESSIONI
	k1l = sqrt(k1q+(k5*k5))   ;dist rif m1>l
	                
	k6  = k4+kymax
	k1r = sqrt(k3q+(k6*k6))   ;dist rif m1>r  
	
	k7  = k1+kxmax          
	k2l = sqrt(k2q+(k7*k7))   ;dist rif m2>l
	
	k8  = k3+kxmax
	k2r = sqrt(k4q+(k8*k8))   ;dist rif m2>r 
	
	k9  = kymin-k2
	k3l = sqrt(k1q+(k9*k9))   ;dist rif m3>l
 
	k10 = kymin-k4
	k3r = sqrt(k3q+(k10*k10)) ;dist rif m3>r 
	
	k11 = kxmin-k1
	k4l = sqrt(k2q+(k11*k11)) ;dist rif m4>l
	
	k12 = kxmin-k3
	k4r = sqrt(k4q+(k12*k12)) ;dist rif m4>r
	
	kdel1  port  kdl/givel,0.1
	kdel2  port  kdr/givel,0.1
	kdel3  port  k1l/givel,0.1
	kdel4  port  k1r/givel,0.1 
	kdel5  port  k2l/givel,0.1
	kdel6  port  k2r/givel,0.1
	kdel7  port  k3l/givel,0.1
	kdel8  port  k3r/givel,0.1
	kdel9  port  k4l/givel,0.1
	kdel10 port  k4r/givel,0.1  
	
	adel   delayr   1          ;Path diretto
	a1     deltapi  kdel1      ;dir l
	a2     deltapi  kdel2      ;dir r
	       delayw   asigf       ;filtrato se passa dietro 

; enfasi toni medi per il passaggio laterale

	k0 = 0
	kk = 1
	ienfasi = 3  ; 0= enfasi nulla, 1 = regolare
	
	afilt butterbp asig, 3000, 4000
	afilte = afilt*ienfasi
	
	kabsy  = abs(ky)
	kafilt = (kabsy > 1 ? k0 : kk-kabsy)
	kbal   = (kx<0 ? k0 : kk)
	kampl = kafilt*(1-kbal)
	kampr = kafilt*kbal
	al = afilte*kampl     
	ar = afilte*kampr

	adel2  delayr   1          ;Path riflessi
	a3     deltapi  kdel3      ;rif m1>l  
	a4     deltapi  kdel4      ;rif m1>r
	a5     deltapi  kdel5      ;rif m2>l  
	a6     deltapi  kdel6      ;rif m2>r             
	a7     deltapi  kdel7      ;rif m3>l  
	a8     deltapi  kdel8      ;rif m3>r
	a9     deltapi  kdel9      ;rif m4>l  
	a10    deltapi  kdel10     ;rif m4>r          
	       delayw   asig                
	 
	alpf3l tone a7, 1400     ;filtro lpf su riflessioni muro 3
	alpf3r tone a8, 1400     ;20000/1+65%(ixmin) 
	 	 
	aleft  = (a1+al)/kdl+a3/k1l+a5/k2l+alpf3l/k3l+a9/k4l	 
	aright = (a2+ar)/kdr+a4/k1r+a6/k2r+alpf3r/k3r+a10/k4r

	outs	aleft, aright
       
garevl	=		garevl+aleft*iattarev*(1/sqrt(kdl)) ;intensita' riverb l 
garevr	=		garevr+aright*iattarev*(1/sqrt(kdr));intensita' riverb R 

	endin

	instr	400,401,402,403,404,405,406,407,408,409
	ifirst	init	400				; first instrument number
	index	=		p1-ifirst
	ifunc	=		index+gibabofunoff
	ix		=		p4
	iy		=		p5
	iz		=		p6
	iwidth	=		p7
	idepth	=		p8
	iheight	=		p9

asend		zar		index
al,ar		babo	asend,ix,iy,iz,iwidth,idepth,iheight
			outs	al,ar

	endin

	instr 500  		; ***** riverberatore *****
	irevt = p4
	idrevt = p5
 	iatl=0.92		; attenuazione f_acute left
 	iatr=0.92 		; attenuazione f_acute right
	
arsl	butterlp	garevl,4000 
arsr	butterlp	garevr,4000 
arevl	nreverb		arsl, irevt, iatl
arevr   nreverb		arsr, irevt+idrevt, iatr
		outs		arevl,arevr     
        
garevl	=			0 
garevr	=			0
     
	endin
;
; r-voices.orc,v
; Revision 0.14  2000/06/26 07:29:06  nicb
; re-worked amplitude equalization sections
; ready for build 6
;
; Revision 0.13  2000/06/25 06:55:03  nicb
; corrected normal balancing
;
; Revision 0.12  2000/06/24 18:32:01  nicb
; modified to fit mixing features and amplitude equalizations
;
; Revision 0.11  2000/06/23 16:38:10  nicb
; more corrections on reverbs etc.
; about to change radically audio indices
;
; Revision 0.10  2000/06/20 17:14:34  nicb
; modified attenuation of reverberations
;
; Revision 0.9  2000/06/20 11:35:00  nicb
; minor corrections
;
; Revision 0.8  2000/06/04 07:41:28  nicb
; augmented choir voice number (now 40+10)
;
; Revision 0.7  2000/06/04 07:19:24  nicb
; added babo instrument
; increased number of separate instruments (now 30+10)
;
; Revision 0.6  2000/05/20 08:18:51  nicb
; doubled number of voices
;
; Revision 0.5  1999/09/06 07:51:16  nicb
; added interpolating position instruments
;
; Revision 0.4  1999/09/03 10:24:10  nicb
; added balance with reference (not used and commented out)
;
; Revision 0.3  1999/09/02 17:47:21  nicb
; corrected bug in instrument numbering
;
; Revision 0.2  1999/09/02 17:45:07  nicb
; added position instrument initialization
; bumped all instrument number by one
;
; Revision 0.1  1999/09/02 16:01:06  nicb
; added room and reverberation instruments
; modified instrument numbers to fit all instances
;
; Revision 0.0  1999/08/23 16:51:49  nicb
; Initial Revision
;
