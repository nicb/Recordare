<CsoundSynthesizer>
; vi:set ts=4 nowrap:
;
; $Id$
;
<CsInstruments>
	sr=44100
	kr=441
	ksmps=100
	nchnls=2

gkl	init	0
gkr	init	0

	instr 1
al,ar	soundin	100

kleft	rms		al,100
kright	rms		ar,100

ktime	times
	dumpk3	ktime,kleft,kright,"avg.log",8,0.1

	outs	al,ar
	endin

</CsInstruments>
; ==============================================
<CsScore>
i1 0	420
</CsScore>
</CsoundSynthesizer>

