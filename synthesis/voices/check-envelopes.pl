#!	/usr/bin/perl
# vi:set ts=4 nowrap:
# 
# check-envelopes.pl,v 0.0 2000/07/02 16:50:30 nicb Exp
#
#

use strict;

#
# main
#
	my $line = "";
	my @funcs = ();

	while($line = <>)
	{
		process($line, \@funcs) unless $line =~ /^;/;
	}
#
# end of main
#
sub process($$)
{
	my ($line, $funcs) = @_;

	chop $line;
	$line =~ s/;.*$//;

	if ($line =~ /^i[1-4]\s+/)
	{
		process_instrument($line, $funcs);
	}

	if ($line =~ /^f[0-9]/)
	{
		process_function($line, $funcs);
	}
}

sub process_instrument($$)
{
	my ($line, $funcs) = @_;
	my ($ins, $at, $dur, $soundin, $offset, $funcno) = split(/\s+/, $line);

	if (defined($funcs->[$funcno]) && $dur != $funcs->[$funcno])
	{
		my $string = sprintf("doubly defined %3d duration (\$dur = %8.6f while \$func->[%d] = %8.6f",
			$funcno, $dur, $funcno, $funcs->[$funcno]);

		die($string);
	}

	$funcs->[$funcno] = $dur;
}

sub process_function($$)
{
	my ($line, $funcs) = @_;
	my ($fno, $at, $size, $gen, @xy) = split(/\s+/, $line);
	my $edge = $xy[1] + $xy[3];
	my $sustain = $xy[5] + $xy[7];
	my $npoints = ($edge*2)+$sustain;
	
	$fno =~ s/^f//;

	die("function $fno not defined") unless defined($funcs->[$fno]);

	my $edge_dur = ($edge/$size)*$funcs->[$fno];

	printf("Function n.%-4d: %8.6f (num points: %4d)\n", $fno, $edge_dur,
		$npoints);
}
#
# check-envelopes.pl,v
# Revision 0.0  2000/07/02 16:50:30  nicb
# Initial Revision
#
