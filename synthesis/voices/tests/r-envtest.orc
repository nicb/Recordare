	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

gamp	init	32000

	instr 1,2,3,4
	iamp=ampdb(80)
	ifunc=p6

kamp	oscil1i	0,iamp,p3,ifunc
amp	=	kamp
	out	amp
	
	endin

	instr 30,31,32,33
	istart=32000*ampdb(p4)
	iend=32000*ampdb(p5)
	ix=p6
	iy=p7

gamp	line	istart,p3,iend
	out	gamp
	endin
