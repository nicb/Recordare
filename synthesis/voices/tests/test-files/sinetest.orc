	sr=44100
	kr=4410
	ksmps=10
	nchnls=1
;
; amplitude modulated instrument (tremolo)
;
	instr 1
	iamp=ampdb(p4)
	iampmod=ampdb(p5)

kamp	oscil	iampmod,p7,1
aout	oscil	iamp+kamp,p6,1
	out	aout
	endin
