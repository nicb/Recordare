	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

	instr 1
	iamp=ampdb(80)
	iedge=0.01
	idur=p3-(iedge-2)
	iend=p3-iedge
	ipch=cpspch(p4)
	ileft=p5
	iright=1-p5

kamp	oscil1i	0,iamp,p3,1
amp	=	kamp
amp	oscil	kamp,ipch,10
	;outs	amp*ileft,amp*iright
	out	amp
	
	endin
