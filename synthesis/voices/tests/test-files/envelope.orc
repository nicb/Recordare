	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

gkamp	init	0

	instr 1
	iamp=ampdb(80)
	iedge=0.01
	idur=p3-(iedge-2)
	iend=p3-iedge
	ipch=cpspch(p4)

	timout	0,iedge,start
	timout	iedge,idur,sustain
	timout	iend,p3,end

start:
kphase	phasor	1/iedge
gkamp	table3	kphase,1
	goto	rest

sustain:
gkamp	=	1
	goto	rest

end:
kphase	phasor	-(1/iedge),1
gkamp	table3	kphase,1
	goto	rest

rest:
itime	times
aout	oscil	gkamp*iamp,ipch,100
	out	aout
	endin

	instr	10
	iamp=ampdb(80)
ktime	times
	dumpk2	ktime,gkamp*iamp,"./envelope.ascii",8,0
	endin
