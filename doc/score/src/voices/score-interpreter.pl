#!/usr/bin/perl
# vi:set nowrap ts=4:
#
# score-interpreter.pl,v 0.3 1999/09/02 17:48:50 nicb Exp
#
# 
#

use Recordare::Score2Csound;
use Recordare::Paths;
use Recordare::Globals;

use strict;

#
# start of main
#

srand(140856);							# set a fixed random seed

my $paths = new Recordare::Paths();
$paths->data_path('../../text/segmentation/real/data');
my $file = shift || 'score.data';
my $globalfile = shift || 'global.data';

my $globals = new Recordare::Globals($globalfile);
my $si = new Recordare::Score2Csound($file);

$si->print_header();
$si->print();
#
# end of main
#
#
# score-interpreter.pl,v
# Revision 0.3  1999/09/02 17:48:50  nicb
# removed global initialization
# and substituted by global data file
#
# Revision 0.2  1999/09/02 16:03:13  nicb
# added more globals
#
# Revision 0.1  1999/09/02 13:49:37  nicb
# added more global variable handling
#
# Revision 0.0  1999/08/23 16:14:20  nicb
# Initial Revision
#
