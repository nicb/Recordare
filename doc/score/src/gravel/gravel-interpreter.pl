#!/usr/bin/perl
# vi:set nowrap ts=4:
#
# $Id$
#
# 
#

use Recordare::Tools;
use Recordare::Gravel2Csound;
use Recordare::Globals;

use strict;

#
# start of main
#

srand(140856);							# set a fixed random seed

my $file = shift || 'gravel.data';
my $globalfile = shift || 'global.data';
my $g = new Recordare::Globals($globalfile);

my $gi = new Recordare::Gravel2Csound($file);

$gi->print_header();
$gi->print();
#
# end of main
#
#
# $Log$
