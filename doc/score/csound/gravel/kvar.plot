set multiplot
set size 1,0.3
set origin 0,0.7
plot "kvar.log" using 1:4 title "amp" w lines
set origin 0,0.3
set size 1,0.4
plot "kvar.log" using 1:2 title "gap" w lines
set size 1,0.3
set origin 0,0
set logscale y
plot "kvar.log" using 1:3 title "dur" w lines
set nomultiplot
pause -1
