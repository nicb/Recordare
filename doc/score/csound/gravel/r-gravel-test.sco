; vi:set nowrap:
;
; r-gravel-test.sco,v 0.3 1999/09/11 16:23:13 nicb Exp
;
; r-gravel test score
;
; the following instruments are defined:
;
;  1-20  - amplitude instruments:
;	p4 = end amplitude attenuation (dB, 0=unity gain)
;
; 21-40  - grain gap instruments:
;	p4 = gap duration hi
;	p5 = gap duration lo
;
; 41-60  - grain gap instruments:
;	p4 = grain duration end
;
; 81-100 - granule instruments:
;	p4 = absolute amplitude (dB)
;	p5 = sound ftable number (10 = bf4 female, 11 = e4 male)
;	p6 = pitch ratio (1 = original pitch)
;	p7 = gap duration hi
;	p8 = gap duration lo
;	p9 = pitch shift control pitch 1
;       p10= pitch shift control pitch 2
;       p11= pitch shift control pitch 3
;       p12= pitch shift control pitch 4
;            (use ratios 0.75-7 for gravel, 0.975-1.035 ratios for singing)
;	p13= skip time (0 for gravel, 1 for voices)
;
;101-120 - output instruments:
;	p4  = direct attenuation (usually 0)
;	p5  = reverb attenuation (usually -30/-40)
;       p6  = fixed x position
;       p7  = fixed y position
;
;200     - reverb instrument:
;

;
; f1  = room definition function
; Room definition data
;                 xL   yL   xR   yR xmax xmin ymax  ymin   X   Y
f1   0 16 -2 0  -1.25  2  1.25  2   34  -34    40   -40   20   20 ; room
;
; f30 = gravel density function
; f10 = female voice function
; f11 = male voice function
;f30  0 8193 5 0.5 8182 1 10  0.001	; reverse exponential function
f30  0 8193 5 0.001 8182 1 10  0.001	; reverse exponential function
f10 0 524288 1 "fof-bf4female.wav" 0 0 0
f11 0 524288 1 "fof-e4male.wav" 0 0 0

i200	0 	18

;
; voice 0
;
i1	0	0.1	0		; amplitude scaler
i41	0	0.1	0.0045		; grain duration (initialization)

i101	0.5	16	0 -6 -9 4	; room info
i180	0.5	16	0		; display k-variables

i41	14	2	0.4

i81	0.5 .5	80 8.10 10 0.1 0.02 0.76 1  0.86  1.4     0
i81	5   .5	80 8.10 10 0.1 0.02 0.76 1  0.86  1.4     0
i81	8   .5	80 8.10 10 0.1 0.02 0.76 1  0.85  1.4     0
i81	12  .5	80 8.10 10 0.1 0.02 0.76 1  0.85  1.4     0
i81	15  .5	80 8.10 10 0.1 0.02 1.01 0.99 1.035 0.975 1
e
;
; r-gravel-test.sco,v
; Revision 0.3  1999/09/11 16:23:13  nicb
; added room calculation handling
;
; Revision 0.2  1999/09/11 15:47:21  nicb
; reduced to 13 p-fields
;
; Revision 0.1  1999/09/11 15:36:16  nicb
; second version freeze (with single granule shots)
;
; Revision 0.0  1999/09/11 12:36:50  nicb
; Initial Revision
;
