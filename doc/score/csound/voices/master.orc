;
; master.orc,v 0.5 1997/10/03 06:54:37 nicb Exp
;
;=========================================================
;=========  SIMULAZIONE FONTI SONORE IN MOVIMENTO ========
;===== CALCOLO RIFLESSIONI DI UNA SORGENTE SONORA ========
;====== IN UNA STANZA DEFINIBILE NELLE DIMENSIONI ========
;========== E NELLA POSIZIONE DEGLI ALTOPARLANTI =========
;===== CON AGGIUNTA RIVERBERO GLOBALE (MOORE, 1983) ======
;======= FILTRI PER SIMULAZIONI RIFLESSIONI ==============
;========== POSTERIORI E LATERALI ========================   
;=========================================================
;
;
; 		Global organization:
;
;		   instr 1,2
;	        (soundfile reader)
;			|	
;			|
;		  1(L)  |   2(R)
;		   +----+----+
;		   |         |
;		   |         |
;		 gasend    gasend
;		   |         |
;		   |         |
;		   +----+----+
;		        |
;      +-----------+----+----+----------+
;      |           |         |	        |
;      v           v         v	        v
;   instr20     instr21   instr22    instr23
;   (static     (circ.    (front-    (random
;  position)     space)    back       space)
;      |	   |	   space)	|
;      |           |         |	        |
;      +-----------+----+----+----------+
;			|
;	 	  gkcurx,gkcury
;			|
;			|
;		+-------+-------+
;		|		|
;		|		|
;		v		v
;	     instr90         instr91
;	     (room)	      (rev)
;
; The whole orchestra is monophonic, that is: only one movement can
; be performed by a single sound at a time; this means that the gasend
; global variable gets wiped out at each a-rate sample
;
;
	sr      =  44100
	kr      =   8820
	ksmps   =      5
	nchnls	=      2
	
	garevl	init 0
	garevr	init 0
	giamax	init 32678
	givel	init 340     ;velocita' del suono

	gasend		init 0		; space instrument send
	gkx		init 0		; x position holder (center)
	gky		init 1.5	; y position holder (front)
	gkcurx		init 0		;
	gkcury		init 0		;
	giroom		init 0		; room parameters table

instr	1 			;  lettore dei files esterni left
	
	;p1	p2	p3	 p4	p5
	;ins	at	dur	nfile skiptime

	idur	=	p3
	iskip	=	p5
	al,ar	soundin	p4,iskip
	gasend	linen	al,0.02,idur,0.02

endin

instr	2 			;  lettore dei files esterni right
	
	;p1	p2	p3	 p4	p5
	;ins	at	dur	nfile skiptime

	idur	=	p3
	iskip	=	p5
	al,ar	soundin	p4,iskip
	gasend	linen	ar,0.02,idur,0.02

endin

instr	3 			;  sync instrument
	
	;p1	p2	p3	 p4
	;ins	at	dur	nfile

	idur	=	p3
	al,ar	soundin	p4
	outs al,ar
endin

instr	4 			;  lettore dei files mono
	
	;p1	p2	p3	 p4	p5
	;ins	at	dur	nfile skiptime

	idur	=	p3
	iskip	=	p5
	amono	soundin	p4,iskip
	gasend	linen	amono,0.02,idur,0.02

endin

;----------------------------------------------------------------------
;			    MOVEMENT ENGINES
;----------------------------------------------------------------------

instr	20			;	static space engine

	; p4	 p5	p6
	; x       y    ntab

	idur    = p3		; movement duration 
	ixpos	= p4		; ixposition [-1...1]
	iypos	= p5		; iyposition [-1...1]

	iroom = p6		; table room def
	giroom = iroom		; (sent to room calculator)

				; seconds for the y position to come into play
				; (10% of cicle time)
	ilatency = abs(0.05/idur)
 
	ix    table 9,iroom     ;coordinate  della sorgente
	iy    table 10,iroom

	ixosc = ixpos*(ix/2)	; full left = -1, full right = 1
	iyosc = iypos*(iy/2)	; full front = 1, full back = -1

	; all of this is done to smooth transitions from one movement
	; to the other:
	; the initial position is held at the beginning and then slowly
	; the oscillators enter into play
	;
	; after ilatency has elapsed, the global variables gkx and gky
	; start holding the current position of the source
	;

	kposdyn	linseg	0,ilatency,1,idur,1	;static/dynamic crossfade
	kposstat = 1-kposdyn

	kx = (ixosc*kposdyn)+(gkx*kposstat)
	ky = (iyosc*kposdyn)+(gky*kposstat)
	
	timout	0,ilatency,skipglobset		; skip to skipglobset label

	gkx = kx	
	gky = ky

skipglobset:

	gkcurx = kx
	gkcury = ky
endin

instr	21			;	circular space engine

	; p4	 p5	p6
	;speed  fase   ntab

	idur	    = p3	; movement duration 
	iciclof   = p4		; frequenza ciclo left-right e dist [0.3]
	ifase1 = p5		; fase iniziale distanza [0..1]

	iroom = p6		; table room def
	giroom = iroom		; (sent to room calculator)

				; seconds for the y position to come into play
				; (10% of cicle time)
	ilatency = abs(0.1/iciclof)
 
	kxosc init 0
	kyosc init 0


	ix    table 9,iroom     ;coordinate  della sorgente
	iy    table 10,iroom

	kxosc oscili ix, iciclof,2,0
	kyosc oscili iy, iciclof,2,ifase1

	; all of this is done to smooth transitions from one movement
	; to the other:
	; the initial position is held at the beginning and then slowly
	; the oscillators enter into play
	;
	; after ilatency has elapsed, the global variables gkx and gky
	; start holding the current position of the source
	;

	kposdyn	linseg	0,ilatency,1,idur,1	;static/dynamic crossfade
	kposstat = 1-kposdyn

	kx = (kxosc*kposdyn)+(gkx*kposstat)
	ky = (kyosc*kposdyn)+(gky*kposstat)
	
	timout	0,ilatency,skipglobset		; skip to skipglobset label

	gkx = kx	
	gky = ky

skipglobset:

	gkcurx = kx
	gkcury = ky
endin

instr	22			;	front/back space engine

	; p4	 p5	p6	p7
	;speed    x     ntab    fase

	idur	= p3		; movement duration 
	iciclof	= p4		; frequenza ciclo front-back e dist [0.3]
	ixpos	= p5		; x position
	ifase	= p7		; fase position [0...1]

	iroom = p6		; table room def
	giroom = iroom		; (sent to room calculator)

				; seconds for the y position to come into play
				; (10% of cicle time)
	ilatency = abs(0.1/iciclof)
 
	kxosc init 0
	kyosc init 0

	ix    table 9,iroom     ;coordinate  della sorgente
	iy    table 10,iroom

	kxosc = ix * ixpos	; x is blocked along one axis
	kry phasor iciclof
	kyosc tablei kry, 2, 1, 0, 1
	kyosc = iy * kyosc

	; all of this is done to smooth transitions from one movement
	; to the other:
	; the initial position is held at the beginning and then slowly
	; the oscillators enter into play
	;
	; after ilatency has elapsed, the global variables gkx and gky
	; start holding the current position of the source
	;

	kposdyn	linseg	0,ilatency,1,idur,1	;static/dynamic crossfade
	kposstat = 1-kposdyn

	kx = (kxosc*kposdyn)+(gkx*kposstat)
	ky = (kyosc*kposdyn)+(gky*kposstat)
	
	timout	0,ilatency,skipglobset		; skip to skipglobset label

	gkx = kx	
	gky = ky

skipglobset:

	gkcurx = kx
	gkcury = ky
endin

instr	23 			;	random engine

	; p4	 p5
	;freq   ntab

	idur    = p3		; movement duration 
	irandf  = p4		; frequenza ciclo left-right e dist [0.3]

	iroom 	= p5		; table room def
	iminmod = p6		; minimum modulo size (absolute value in m.)
	giroom 	= iroom		; (sent to room calculator)

				; seconds for the y position to come into play
				; (10% of cicle time)
	ilatency = abs(0.1/irandf)
	iseed1  = 0.7		; must differenciate seeds in order to pick
	iseed2	= 0.3		; up different sequences
 
	kxosc init 0
	kyosc init 0

	ix    table 9,iroom     ;coordinate  della sorgente
	iy    table 10,iroom

	imaxmod = (((ix > iy ? ix : iy))/2);
	imodrandamp = abs(imaxmod-iminmod)/2;

	; the random movement is produced with two random numbers per
	; draw: one is the radius and the other is the angle to avoid
	; random points to be too near the listener (or to go straight
	; through him
	; by using randi we interpolate through successive moduli and
	; phases thus avoiding automatically to create paths that
	; cross the minimum distance circle

	kphase randi 4,irandf,iseed1		; phase between pi and -pi
						; (we use 4 instead of 3.14
						; so random numbers cross
						; the back azimuth)
	kmodulo randi imodrandamp,irandf,iseed2	; modulo (relative number)
	kmodulo = kmodulo+iminmod+imodrandamp	; modulo (abs pos. value)

	kxosc = kmodulo*sin(kphase)		; polar to cartesian conv.
	kyosc = kmodulo*cos(kphase)		; polar to cartesian conv.

	; all of what follows is done to smooth transitions from one movement
	; to the other:
	; the initial position is held at the beginning and then slowly
	; the oscillators enter into play
	;
	; after ilatency has elapsed, the global variables gkx and gky
	; start holding the current position of the source
	;

	kposdyn	linseg	0,ilatency,1,idur,1	;static/dynamic crossfade
	kposstat = 1-kposdyn

	kx = (kxosc*kposdyn)+(gkx*kposstat)
	ky = (kyosc*kposdyn)+(gky*kposstat)
	
	timout	0,ilatency,skipglobset		; skip to skipglobset label

	gkx = kx	
	gky = ky

skipglobset:

	gkcurx = kx
	gkcury = ky

endin

;----------------------------------------------------------------------
;			    ROOM INSTRUMENTS
;----------------------------------------------------------------------

instr 90					; spatializer

	; p4	 p5
	;atten attnrev

	idur  = p3
	iattadir = ampdb(p4)	; attenuazione di ampiezza [0]
 	iattarev = ampdb(p5)	; attenuazione riverbero [-30]
	iroom = giroom		; table room def (set by engines)

	asig = gasend * iattadir;
	gasend = 0		; clear send signal

	kx = gkcurx		; coming from the movement engines
	ky = gkcury		; coming from the movement engines

	                ;DEFINIZIONE INNER ROOM
	ixl   table 1,iroom     ;coordinate(in m) altoparlante
	iyl   table 2,iroom	;sinistro 
	ixr   table 3,iroom     ;coordinate(in m) altoparlante 
	iyr   table 4,iroom     ;destro       
	                ;DEFINIZIONE OUTER ROOM       
	ixmax table 5,iroom     ;larghezza max positiva (in m) 
	ixmin table 6,iroom     ;larghezza max negativa (in m) 
	iymax table 7,iroom     ;lunghezza max positiva (in m)
	iymin table 8,iroom     ;lunghezza max negativa (in m) 
	iy    table 10,iroom	; max coordinata y della sorgente
	print ixl,iyl,ixr,iyr,ixmax,ixmin,iymax,iymin
 
	ideltaf = sr/2 - 2000
	kfilt = sr/2 + ideltaf*ky/iy
	af0 tone asig, kfilt 
    
	asigf = (ky >= 0 ? asig : af0) 
    
	k1     = kx-ixl                 
	k2     = ky-iyl 
	k3     = kx-ixr
	k4     = ky-iyr 
	k1q    = k1*k1
	k2q    = k2*k2
	k3q    = k3*k3
	k4q    = k4*k4     
	kxmax  = 2*(ixmax-kx)
	kymax  = 2*(iymax-ky)
	kxmin  = 2*(kx-ixmin)
	kymin  = 2*(ky-iymin)
	                          ;CALCOLO SEGNALE DIRETTO 
	kdl = sqrt(k1q+k2q)       ;dist dir>L
	kdr = sqrt(k3q+k4q)       ;dist dir>R
	
;i muri sono numerati in senso orario a partire da quello di fronte 
;all'ascoltatore (m1)
	
	k5  = k2+kymax            ;CALCOLO RIFLESSIONI
	k1l = sqrt(k1q+(k5*k5))   ;dist rif m1>l
	                
	k6  = k4+kymax
	k1r = sqrt(k3q+(k6*k6))   ;dist rif m1>r  
	
	k7  = k1+kxmax          
	k2l = sqrt(k2q+(k7*k7))   ;dist rif m2>l
	
	k8  = k3+kxmax
	k2r = sqrt(k4q+(k8*k8))   ;dist rif m2>r 
	
	k9  = kymin-k2
	k3l = sqrt(k1q+(k9*k9))   ;dist rif m3>l
 
	k10 = kymin-k4
	k3r = sqrt(k3q+(k10*k10)) ;dist rif m3>r 
	
	k11 = kxmin-k1
	k4l = sqrt(k2q+(k11*k11)) ;dist rif m4>l
	
	k12 = kxmin-k3
	k4r = sqrt(k4q+(k12*k12)) ;dist rif m4>r
	
	kdel1  port  kdl/givel,0.1
	kdel2  port  kdr/givel,0.1
	kdel3  port  k1l/givel,0.1
	kdel4  port  k1r/givel,0.1 
	kdel5  port  k2l/givel,0.1
	kdel6  port  k2r/givel,0.1
	kdel7  port  k3l/givel,0.1
	kdel8  port  k3r/givel,0.1
	kdel9  port  k4l/givel,0.1
	kdel10 port  k4r/givel,0.1  
	
	adel   delayr   1          ;Path diretto
	a1     deltapi  kdel1      ;dir l
	a2     deltapi  kdel2      ;dir r
	       delayw   asigf       ;filtrato se passa dietro 

; enfasi toni medi per il passaggio laterale

	k0 = 0
	kk = 1
	ienfasi = 3  ; 0= enfasi nulla, 1 = regolare
	
	afilt butterbp asig, 3000, 4000
	afilte = afilt*ienfasi
	
	kabsy  = abs(ky)
	kafilt = (kabsy > 1 ? k0 : kk-kabsy)
	kbal   = (kx<0 ? k0 : kk)
	kampl = kafilt*(1-kbal)
	kampr = kafilt*kbal
	al = afilte*kampl     
	ar = afilte*kampr
	
	adel2  delayr   1          ;Path riflessi
	a3     deltapi  kdel3      ;rif m1>l  
	a4     deltapi  kdel4      ;rif m1>r
	a5     deltapi  kdel5      ;rif m2>l  
	a6     deltapi  kdel6      ;rif m2>r             
	a7     deltapi  kdel7      ;rif m3>l  
	a8     deltapi  kdel8      ;rif m3>r
	a9     deltapi  kdel9      ;rif m4>l  
	a10    deltapi  kdel10     ;rif m4>r          
	       delayw   asig                
	 
	alpf3l tone a7, 1400     ;filtro lpf su riflessioni muro 3
	alpf3r tone a8, 1400     ;20000/1+65%(ixmin) 
	 	 
	aleft  = (a1+al)/kdl+a3/k1l+a5/k2l+alpf3l/k3l+a9/k4l	 
	aright = (a2+ar)/kdr+a4/k1r+a6/k2r+alpf3r/k3r+a10/k4r
       
	outs aleft, aright
       
	garevl   = garevl+aleft*iattarev*(1/sqrt(kdl)) ;intensita' riverb l 
	garevr   = garevr+aright*iattarev*(1/sqrt(kdr));intensita' riverb R 

endin

instr 91     			; ***** riverberatore *****
	irevt = p4
 	idrevt = p5
 	iatl=0.55		; attenuazione f_acute left
 	iatr=0.52 		; attenuazione f_acute right
	
 
	arevl	reverb2  garevl, irevt, iatl
	arevr   reverb2  garevr, irevt+idrevt, iatr
	outs    arevl,arevr     
        
	garevl = 0 
	garevr = 0
     
endin

;
; master.orc,v
; Revision 0.5  1997/10/03 06:54:37  nicb
; random position generator completely overhauled
; (now using a polar random generator)
;
; Revision 0.4  1997/09/09 15:21:41  nicb
; added mono reader instrument
;
; Revision 0.3  1997/09/08 16:02:57  nicb
; added skiptime handling for readers
;
; Revision 0.2  1997/09/08 11:16:26  nicb
; removed display instructions (they crash the program on long files)
;
; Revision 0.1  1997/09/08 10:45:38  nicb
; added sync instr 3
; removed atten and revsend fields from movement engines (useless)
; added phase to random instrument
;
; Revision 0.0  1997/09/07 19:38:26  nicb
; Initial Revision
;
