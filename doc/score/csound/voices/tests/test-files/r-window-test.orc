;
;
	sr=44100
	kr=44100
	ksmps=1
	nchnls=2

gitoggle	init	0

	instr 1
	icthreshold=1000
	ilthreshold=30000
	gitoggle = (gitoggle == 0 ? 1 : 0)

	print	gitoggle

kamp	oscil1	0,1,p3,1
aout	=	kamp*16000

	if	gitoggle == 0 goto left
right:
	aleft	= 0
	aright	= aout
	goto	end

left:
	aleft	= aout
	aright	= 0
	goto	end
	
;aref	soundin	p6,p7
;aout	soundin	p4,p5

;aout	balance	aout*kamp,aref
;aout	dam	aout,icthreshold,1,100,0.2,0.2
;aout	dam	aout,icthreshold,0.0001,1,2,2
;aout	limit	aout,-ilthreshold,ilthreshold

;aout	butlp	aout,5000

end:
	outs	aleft,aright

	endin
