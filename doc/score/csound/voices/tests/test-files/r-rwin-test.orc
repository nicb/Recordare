;
;
	sr=44100
	kr=44100
	ksmps=1
	nchnls=2

	instr 1
	icthreshold=1000
	ilthreshold=30000

kamp	oscil1	0,1,p3,1
aref	soundin	p6,p7
aout	soundin	p4,p5

	aleft	= aout*kamp
	aright	= kamp*16000

;aout	balance	aout*kamp,aref
;aout	dam	aout,icthreshold,1,100,0.2,0.2
;aout	dam	aout,icthreshold,0.0001,1,2,2
;aout	limit	aout,-ilthreshold,ilthreshold

;aout	butlp	aout,5000
;	out	aout
	outs	aleft,aright

	endin
