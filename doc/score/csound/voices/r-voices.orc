;
; $Id: r-voices.orc,v 0.5 1999/09/06 07:51:16 nicb Exp nicb $
;
; 		Global organization:
;
;                  instr 1-10
;          (soundfile and envelope reader)
;			|	
;                   zaw 0-9
;		        |
;                 instr 30-39
;               (interpolators)
;			|
;         zkw 0-9(amp),10-19(x),20-29(y) 
;			|
;		+-------+-------+
;		|		|
;		|		|
;		v		v
;	   instr 90-99       instr100
;	     (room)	      (rev)
;
; 
;
	sr=44100
	kr=44100
	ksmps=1
	nchnls=2

;
; the zak a variables hold the variables for the audio outputs
; of instruments 1-10, like this:
;
; zar[0] = instr 1 audio output
; ...
; zar[9] = instr 10 audio output
;
; the zak k variables hold the position variables in the
; following way:
;
; zkr[0] = instr 30 (instr 1 dynamic and position controller) cur amp
; ...
; zkr[9] = instr 39 (instr 10 dynamic and position controller) cur amp
; zkr[10]= instr 30 (instr 1 dynamic and position controller) cur x position
; ...
; zkr[19]= instr 39 (instr 10 dynamic and position controller) cur x position
; zkr[20]= instr 30 (instr 1 dynamic and position controller) cur y position
; ...
; zkr[29]= instr 39 (instr 10 dynamic and position controller) cur y position
;
;
zakinit		20,60
garevl	init	0
garevr	init	0
givel	init	340

	;
	; zak system (movement) initialization system
	;
	instr 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
	ixstart=p4
	ixend=p5
	iystart=p6
	iyend=p7
	ixindex=p1+20
	iyindex=p1+40

kx	line	ixstart,p3,ixend
ky	line	iystart,p3,iyend

	zkw	kx,ixindex
	zkw	ky,iyindex

	endin

	instr 21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40
	izerodB=32767
	iamp=ampdb(0)
	isound=p4
	iskip=p5
	ifunc=p6
	index=p1-21
	ihithreshold=izerodB*ampdb(-12)
	ilothreshold=izerodB*ampdb(-18)
	iupratio=ampdb(-6)
	idownratio=ampdb(+6)
	irisetime=0.01
	idectime=0.01

print	ihithreshold,iupratio,ilothreshold,idownratio
aprior	zar	index
kamp	oscil1i	0,iamp,p3,ifunc
aout	soundin	isound,iskip	

;krms	rms	aout
;	printk	0,krms

aout	dam	aout,ihithreshold,iupratio,1,irisetime,idectime
aout	dam	aout,ilothreshold,1,idownratio,irisetime,idectime

	zaw	(aout*kamp)+aprior,index

	endin

;
; linear amplitude controller
;
	instr 41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60
	izerodB=ampdb(0)
	istart=izerodB*ampdb(p4)
	iend=izerodB*ampdb(p5)
	iampindex=p1-41

;
;	interpolate
;
kamp	line	istart,p3,iend
;
;	output
;
	zkw	kamp,iampindex
	endin

;-------------------------------------------
;	    ROOM INSTRUMENTS
;-------------------------------------------

	instr 91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110	; spatializer

	;
	; input indexings
	;
	iaindex=p1-91		; a-value
	iampindex=p1-91		; k-values
	ixindex=iampindex+20
	iyindex=iampindex+40
	;
	; p4	 p5
	;atten attnrev
	;
	idur  = p3
	iattadir = ampdb(p4)	; attenuazione di ampiezza [0]
 	iattarev = ampdb(p5)	; attenuazione riverbero [-30]
	iroom = 1		; table room def (set by engines)
	;
	; audio input
	;
asend	zar	iaindex		; signal
kamp	zkr	iampindex	; amplitude envelope

asig	=	asend*iattadir*kamp;
asend	=	0
	zaw	asend,iaindex	; clear input

kx 	zkr	ixindex		; coming from the movement engines
ky	zkr	iyindex		; coming from the movement engines

	                ;DEFINIZIONE INNER ROOM
	ixl   table 1,iroom     ;coordinate(in m) altoparlante
	iyl   table 2,iroom	;sinistro 
	ixr   table 3,iroom     ;coordinate(in m) altoparlante 
	iyr   table 4,iroom     ;destro       
	                ;DEFINIZIONE OUTER ROOM       
	ixmax table 5,iroom     ;larghezza max positiva (in m) 
	ixmin table 6,iroom     ;larghezza max negativa (in m) 
	iymax table 7,iroom     ;lunghezza max positiva (in m)
	iymin table 8,iroom     ;lunghezza max negativa (in m) 
	iy    table 10,iroom	; max coordinata y della sorgente
	print ixl,iyl,ixr,iyr,ixmax,ixmin,iymax,iymin
 
	ideltaf = sr/2 - 2000
	kfilt = sr/2 + ideltaf*ky/iy
	af0 tone asig, kfilt 
    
	asigf = (ky >= 0 ? asig : af0) 
    
	k1     = kx-ixl                 
	k2     = ky-iyl 
	k3     = kx-ixr
	k4     = ky-iyr 
	k1q    = k1*k1
	k2q    = k2*k2
	k3q    = k3*k3
	k4q    = k4*k4     
	kxmax  = 2*(ixmax-kx)
	kymax  = 2*(iymax-ky)
	kxmin  = 2*(kx-ixmin)
	kymin  = 2*(ky-iymin)
	                          ;CALCOLO SEGNALE DIRETTO 
	kdl = sqrt(k1q+k2q)       ;dist dir>L
	kdr = sqrt(k3q+k4q)       ;dist dir>R
	
;i muri sono numerati in senso orario a partire da quello di fronte 
;all'ascoltatore (m1)
	
	k5  = k2+kymax            ;CALCOLO RIFLESSIONI
	k1l = sqrt(k1q+(k5*k5))   ;dist rif m1>l
	                
	k6  = k4+kymax
	k1r = sqrt(k3q+(k6*k6))   ;dist rif m1>r  
	
	k7  = k1+kxmax          
	k2l = sqrt(k2q+(k7*k7))   ;dist rif m2>l
	
	k8  = k3+kxmax
	k2r = sqrt(k4q+(k8*k8))   ;dist rif m2>r 
	
	k9  = kymin-k2
	k3l = sqrt(k1q+(k9*k9))   ;dist rif m3>l
 
	k10 = kymin-k4
	k3r = sqrt(k3q+(k10*k10)) ;dist rif m3>r 
	
	k11 = kxmin-k1
	k4l = sqrt(k2q+(k11*k11)) ;dist rif m4>l
	
	k12 = kxmin-k3
	k4r = sqrt(k4q+(k12*k12)) ;dist rif m4>r
	
	kdel1  port  kdl/givel,0.1
	kdel2  port  kdr/givel,0.1
	kdel3  port  k1l/givel,0.1
	kdel4  port  k1r/givel,0.1 
	kdel5  port  k2l/givel,0.1
	kdel6  port  k2r/givel,0.1
	kdel7  port  k3l/givel,0.1
	kdel8  port  k3r/givel,0.1
	kdel9  port  k4l/givel,0.1
	kdel10 port  k4r/givel,0.1  
	
	adel   delayr   1          ;Path diretto
	a1     deltapi  kdel1      ;dir l
	a2     deltapi  kdel2      ;dir r
	       delayw   asigf       ;filtrato se passa dietro 

; enfasi toni medi per il passaggio laterale

	k0 = 0
	kk = 1
	ienfasi = 3  ; 0= enfasi nulla, 1 = regolare
	
	afilt butterbp asig, 3000, 4000
	afilte = afilt*ienfasi
	
	kabsy  = abs(ky)
	kafilt = (kabsy > 1 ? k0 : kk-kabsy)
	kbal   = (kx<0 ? k0 : kk)
	kampl = kafilt*(1-kbal)
	kampr = kafilt*kbal
	al = afilte*kampl     
	ar = afilte*kampr
	
	adel2  delayr   1          ;Path riflessi
	a3     deltapi  kdel3      ;rif m1>l  
	a4     deltapi  kdel4      ;rif m1>r
	a5     deltapi  kdel5      ;rif m2>l  
	a6     deltapi  kdel6      ;rif m2>r             
	a7     deltapi  kdel7      ;rif m3>l  
	a8     deltapi  kdel8      ;rif m3>r
	a9     deltapi  kdel9      ;rif m4>l  
	a10    deltapi  kdel10     ;rif m4>r          
	       delayw   asig                
	 
	alpf3l tone a7, 1400     ;filtro lpf su riflessioni muro 3
	alpf3r tone a8, 1400     ;20000/1+65%(ixmin) 
	 	 
	aleft  = (a1+al)/kdl+a3/k1l+a5/k2l+alpf3l/k3l+a9/k4l	 
	aright = (a2+ar)/kdr+a4/k1r+a6/k2r+alpf3r/k3r+a10/k4r
       
	outs aleft, aright
       
	garevl   = garevl+aleft*iattarev*(1/sqrt(kdl)) ;intensita' riverb l 
	garevr   = garevr+aright*iattarev*(1/sqrt(kdr));intensita' riverb R 

	endin

	instr 120  		; ***** riverberatore *****
	irevt = p4
 	idrevt = p5
 	iatl=0.05		; attenuazione f_acute left
 	iatr=0.02 		; attenuazione f_acute right
	
arsl	butterlp	garevl,4000 
arsr	butterlp	garevr,4000 
arevl	reverb2		arsl, irevt, iatl
arevr   reverb2		arsr, irevt+idrevt, iatr
	outs		arevl,arevr     
        
	garevl = 0 
	garevr = 0
     
	endin
;
; $Log: r-voices.orc,v $
; Revision 0.5  1999/09/06 07:51:16  nicb
; added interpolating position instruments
;
; Revision 0.4  1999/09/03 10:24:10  nicb
; added balance with reference (not used and commented out)
;
; Revision 0.3  1999/09/02 17:47:21  nicb
; corrected bug in instrument numbering
;
; Revision 0.2  1999/09/02 17:45:07  nicb
; added position instrument initialization
; bumped all instrument number by one
;
; Revision 0.1  1999/09/02 16:01:06  nicb
; added room and reverberation instruments
; modified instrument numbers to fit all instances
;
; Revision 0.0  1999/08/23 16:51:49  nicb
; Initial Revision
;
