; Produced automagically by:
;
; 	Recordare::Score2Csound $Revision: 0.5 $
;
; Room definition data
;                 xL   yL   xR   yR xmax xmin ymax  ymin   X   Y
f1   0 16 -2 0  -1.25  2  1.25  2   12  -12    20   -20    8   8 ; room
;
;  score.data,v 0.0 1999/09/02 16:04:47 nicb Exp
; 
;  strofa 1
; 
; Printing voice n. 1
i10    2.9200   0.3310  0   0.0000  11;     catbassa - Ci              (0)
i10    3.3310   0.2742  9   0.2610  12;      ninovel - ricordiamo      (0)
i10    3.5252   0.2964  3   2.2634  13;     clcbassa - ricordiamo      (1)
i10    3.7417   0.3085  5   0.8140  14;     nicbalta - ricordiamo      (2)
i10    3.9702   0.2742  9   0.8437  12;      ninovel - ricordiamo      (3)
i10    4.3244   0.2764  3   2.8170  15;     clcbassa - di              (0)
i10    4.6808   0.2106 11   1.3110  16;      titino1 - te,             (0)
i10    4.8113   0.3015  9   1.3525  17;      ninovel - te,             (1)
i10    5.9628   0.3563  5   1.9300  18;     nicbalta - maledetto       (0)
i10    6.2392   0.2528  9   1.9848  19;      ninovel - maledetto       (1)
i10    6.4119   0.2985 11   2.4670  20;      titino1 - maledetto       (2)
i10    6.6304   0.3310  0   3.1520  11;     catbassa - maledetto       (3)
i10    7.0414   0.2787  9   2.4390  21;      ninovel - generale        (0)
i10    7.2401   0.3310  0   3.5610  11;     catbassa - generale        (1)
i10    7.4911   0.2787  9   2.8363  21;      ninovel - generale        (2)
i10    7.8498   0.3310  0   4.0630  11;     catbassa - Mladic          (0)
i10    8.1008   0.2642  9   3.2192  22;      ninovel - Mladic          (1)
i10    8.2849   0.3310  0   4.5650  11;     catbassa - Mladic          (2)
i10    8.5359   0.2644  3   5.4613  23;     clcbassa - Mladic          (3)
i10    8.7204   0.3310  0   5.0670  11;     catbassa - Mladic          (4)
i10   10.0074   0.3773  5   4.9370  24;     nicbalta - vogliamo        (0)
i10   10.3047   0.2820 11   6.0200  25;      titino1 - vogliamo        (1)
i10   10.5067   0.3773  5   5.5317  24;     nicbalta - vogliamo        (2)
i10   10.9640   0.2077  9   5.3740  26;      ninovel - che             (0)
i10   11.4257   0.3310  0   7.0720  11;     catbassa - tu              (0)
i10   11.8367   0.2397  9   5.6930  27;      ninovel - non             (0)
i10   11.9964   0.2010 11   6.9230  28;      titino1 - non             (1)
i10   12.2774   0.3310  0   7.6320  11;     catbassa - sia             (0)
i10   12.5284   0.2352  9   6.1102  29;      ninovel - sia             (1)
i10   12.6837   0.2592  3   7.8423  30;     clcbassa - sia             (2)
i10   13.0228   0.3310  0   8.2190  11;     catbassa - dimenticato     (0)
i10   13.2738   0.2444  3   8.0674  31;     clcbassa - dimenticato     (1)
i10   13.4382   0.2411  9   6.6401  32;      ninovel - dimenticato     (2)
i10   13.5993   0.3181  5   8.2712  33;     nicbalta - dimenticato     (3)
i10   13.8374   0.2411  9   6.9623  32;      ninovel - dimenticato     (4)
i10   13.9985   0.2808 11  10.4122  34;      titino1 - dimenticato     (5)
i10   14.9713   0.2381  9   7.5960  35;      ninovel - agli            (0)
i10   15.1294   0.3533 11  12.0023  36;      titino1 - agli            (1)
i10   15.5627   0.3310  0  10.6080  11;     catbassa - uomini          (0)
i10   15.8137   0.2605  9   7.9535  37;      ninovel - uomini          (1)
i10   15.9942   0.3310  0  11.1100  11;     catbassa - uomini          (2)
i10   16.4052   0.2900  9   8.1490  38;      ninovel - fa              (0)
i10   16.7752   0.3305  3  10.4820  39;     clcbassa - bene            (0)
i10   17.0258   0.2580  9   8.5140  40;      ninovel - bene            (1)
i10   17.2038   0.3310  0  11.8230  11;     catbassa - bene            (2)
i10   17.8548   0.3781  5  11.6170  41;     nicbalta - ricordare       (0)
i10   18.1529   0.2686  9   8.9146  42;      ninovel - ricordare       (1)
i10   18.3415   0.3781  5  12.2133  41;     nicbalta - ricordare       (2)
i10   18.6396   0.2686  9   9.2917  42;      ninovel - ricordare       (3)
i10   18.9882   0.2429  3  12.0940  43;     clcbassa - i               (0)
i10   19.3111   0.2828  9   9.3940  44;      ninovel - peggiori        (0)
i10   19.5138   0.3709 11  17.3379  45;      titino1 - peggiori        (1)
i10   19.8047   0.3763  5  13.2897  46;     nicbalta - peggiori        (2)
i10   20.4341   0.3310  0  13.8580  11;     catbassa - non             (0)
i10   20.6851   0.3096  5  14.3196  47;     nicbalta - non             (1)
i10   21.0746   0.3310  0  14.1510  11;     catbassa - sei             (0)
i10   21.3256   0.2705  9  11.2485  48;      ninovel - sei             (1)
i10   21.6761   0.3279  3  14.1590  49;     clcbassa - il              (0)
i10   22.0840   0.3099 11  18.8410  50;      titino1 - solo            (0)
i10   22.3139   0.3310  0  14.9580  11;     catbassa - solo            (1)
i10   22.5649   0.2692  9  11.8345  51;      ninovel - solo            (2)
i10   23.4742   0.2205  3  15.5320  52;     clcbassa - ma              (0)
i10   23.7746   0.3761  5  16.3480  53;     nicbalta - la              (0)
i10   24.2307   0.3284 11  21.9350  54;      titino1 - storia          (0)
i10   24.4790   0.2904  9  12.6084  55;      ninovel - storia          (1)
i10   24.6895   0.3310  0  16.7340  11;     catbassa - storia          (2)
i10   25.2155   0.2395 11  23.9790  56;      titino1 - e`              (0)
i10   25.5350   0.2555  9  13.1630  57;      ninovel - sbadata         (0)
i10   25.7104   0.3666  5  18.2126  58;     nicbalta - sbadata         (1)
i10   25.9970   0.3310  0  17.7090  11;     catbassa - sbadata         (2)
i10   26.2480   0.2959 11  24.7487  59;      titino1 - sbadata         (3)
; Envelopes/Positions for voice 1
i30    2.9200  23.7839   0.00   0.00  -0.50   8.00
; Room Info for voice 1
i90    3.0000  31.7039   0.00 -24.00
; Printing voice n. 2
i11    2.9300   0.3350 10   0.0170  60;       silvia - Ci              (0)
i11    3.3450   0.3249  0   0.2450  61;     catbassa - ricordiamo      (0)
i11    3.5899   0.3350 10   0.4350  60;       silvia - ricordiamo      (1)
i11    3.8449   0.3249  0   0.7347  61;     catbassa - ricordiamo      (2)
i11    4.0897   0.2739  8   0.8076  62;     ninolent - ricordiamo      (3)
i11    4.4436   0.3350 10   1.1100  60;       silvia - di              (0)
i11    4.9866   0.4998  0   1.3310  63;     catbassa - te,             (0)
i11    6.4264   0.3350 10   2.4110  60;       silvia - maledetto       (0)
i11    6.6814   0.4031  0   2.7221  64;     catbassa - maledetto       (1)
i11    7.0045   0.3350 10   2.9210  60;       silvia - maledetto       (2)
i11    7.4195   0.3516  0   3.3100  65;     catbassa - generale        (0)
i11    7.6911   0.3350 10   3.3850  60;       silvia - generale        (1)
i11    7.9461   0.2997  8   3.0763  66;     ninolent - generale        (2)
i11    8.3257   0.3350 10   3.8370  60;       silvia - Mladic          (0)
i11    8.5807   0.4032  0   4.3862  67;     catbassa - Mladic          (1)
i11    8.9039   0.3350 10   4.3470  60;       silvia - Mladic          (2)
i11    9.1589   0.2956  8   3.8927  68;     ninolent - Mladic          (3)
i11   10.4905   0.3350 10   5.6080  60;       silvia - vogliamo        (0)
i11   10.7455   0.3155  8   5.3105  69;     ninolent - vogliamo        (1)
i11   10.9810   0.3571  0   6.5262  70;     catbassa - vogliamo        (2)
i11   11.4181   0.3350 10   6.3010  60;       silvia - che             (0)
i11   11.8331   0.2149  8   5.9490  71;     ninolent - tu              (0)
i11   12.1280   0.3350 10   6.7050  60;       silvia - non             (0)
i11   12.5430   0.3764  0   7.6320  72;     catbassa - sia             (0)
i11   12.8394   0.3350 10   7.1690  60;       silvia - sia             (1)
i11   13.8474   0.3193  8   8.0030  73;     ninolent - dimenticato     (0)
i11   14.0867   0.3350 10   8.2670  60;       silvia - dimenticato     (1)
i11   14.3417   0.3193  8   8.4815  73;     ninolent - dimenticato     (2)
i11   14.5810   0.3350 10   8.7770  60;       silvia - dimenticato     (3)
i11   14.8360   0.3193  8   8.9601  73;     ninolent - dimenticato     (4)
i11   15.9372   0.3350 10   9.9460  60;       silvia - agli            (0)
i11   16.3522   0.3494  0  10.6080  74;     catbassa - uomini          (0)
i11   16.6217   0.3350 10  10.4360  60;       silvia - uomini          (1)
i11   17.0367   0.2537  8  10.5960  75;     ninolent - fa              (0)
i11   17.3704   0.3350 10  10.8830  60;       silvia - bene            (0)
i11   17.6254   0.3511  8  11.0081  76;     ninolent - bene            (1)
i11   18.5185   0.3350 10  11.8200  60;       silvia - ricordare       (0)
i11   18.7735   0.3195  0  12.3505  77;     catbassa - ricordare       (1)
i11   19.0130   0.3350 10  12.3300  60;       silvia - ricordare       (2)
i11   19.2680   0.2934  8  11.9681  78;     ninolent - ricordare       (3)
i11   19.6413   0.4378  0  12.8990  79;     catbassa - i               (0)
i11   20.2071   0.3350 10  12.7740  60;       silvia - peggiori        (0)
i11   20.4621   0.3454  0  13.2584  80;     catbassa - peggiori        (1)
i11   20.7274   0.3350 10  13.2840  60;       silvia - peggiori        (2)
i11   21.8764   0.3598  0  13.8580  81;     catbassa - non             (0)
i11   22.1563   0.3350 10  14.4280  60;       silvia - non             (1)
i11   22.5713   0.2844  8  14.2830  82;     ninolent - sei             (0)
i11   22.7757   0.3350 10  14.6950  60;       silvia - sei             (1)
i11   23.1907   0.3730  0  14.5450  83;     catbassa - il              (0)
i11   23.6437   0.3350 10  14.9440  60;       silvia - solo            (0)
i11   23.8987   0.3826  8  15.0286  84;     ninolent - solo            (1)
i11   25.0193   0.3350 10  16.0890  60;       silvia - ma              (0)
i11   25.2743   0.2204  8  16.6324  85;     ninolent - ma              (1)
i11   25.5748   0.3350 10  16.3650  60;       silvia - la              (0)
i11   25.9898   0.3061  8  16.7390  86;     ninolent - storia          (0)
i11   26.2159   0.3350 10  16.7450  60;       silvia - storia          (1)
i11   26.4709   0.3061  8  17.1912  86;     ninolent - storia          (2)
i11   27.4430   0.4200  0  17.0150  87;     catbassa - e`              (0)
i11   27.9430   0.3350 10  17.9440  60;       silvia - sbadata         (0)
i11   28.1980   0.3790  0  17.5060  88;     catbassa - sbadata         (1)
i11   28.4970   0.3350 10  18.4540  60;       silvia - sbadata         (2)
i11   28.7520   0.3790  0  18.1041  88;     catbassa - sbadata         (3)
; Envelopes/Positions for voice 2
i31    2.9300  26.3611   0.00   0.00   0.00   8.00
; Room Info for voice 2
i91    3.0100  34.2811   0.00 -24.00
; Printing voice n. 3
i12    2.9400   0.5076  6   0.0890  89;    nicbbassa - Ci              (0)
i12    3.5276   0.3330  3   0.7280  90;      clcalta - ricordiamo      (0)
i12    3.7806   0.3568 10   0.4568  91;       silvia - ricordiamo      (1)
i12    4.0574   0.3330  3   1.2340  90;      clcalta - ricordiamo      (2)
i12    4.3104   0.4970 12   1.9560  92; titino2-soft - ricordiamo      (3)
i12    4.8874   0.3302  1   1.2550  93;   carlocarlo - di              (0)
i12    5.3786   0.3330  3   1.8400  90;      clcalta - te,             (0)
i12    6.2436   0.3447  6   2.4100  94;    nicbbassa - maledetto       (0)
i12    6.5083   0.3330  3   2.7740  90;      clcalta - maledetto       (1)
i12    6.7613   0.3992 12   5.8953  95; titino2-soft - maledetto       (2)
i12    7.2405   0.3482 10   3.1300  96;       silvia - generale        (0)
i12    7.5086   0.5022  1   3.7472  97;   carlocarlo - generale        (1)
i12    7.9308   0.3330  3   3.7840  90;      clcalta - generale        (2)
i12    8.3438   0.3501  6   4.0100  98;    nicbbassa - Mladic          (0)
i12    8.6139   0.3260 12   8.6960  99; titino2-soft - Mladic          (1)
i12    8.8599   0.3287 10   4.3345 100;       silvia - Mladic          (2)
i12    9.1086   0.3260 12   9.1879  99; titino2-soft - Mladic          (3)
i12   10.3146   0.4332  6   5.5160 101;    nicbbassa - vogliamo        (0)
i12   10.6678   0.3434  1   6.6864 102;   carlocarlo - vogliamo        (1)
i12   10.9312   0.4332  6   6.2225 101;    nicbbassa - vogliamo        (2)
i12   11.5164   0.4185 10   6.3010 103;       silvia - che             (0)
i12   12.1849   0.6221  1   7.6830 104;   carlocarlo - tu              (0)
i12   12.8870   0.3330  3   6.7810  90;      clcalta - non             (0)
i12   13.3000   0.3680  8   6.2680 105;     ninolent - sia             (0)
i12   13.5880   0.2893  6   7.4663 106;    nicbbassa - sia             (1)
i12   13.7973   0.3680  8   6.8439 105;     ninolent - sia             (2)
i12   14.5533   0.3252  6   7.6880 107;    nicbbassa - dimenticato     (0)
i12   14.7985   0.3330  3   8.1100  90;      clcalta - dimenticato     (1)
i12   15.0515   0.3252  6   8.1785 107;    nicbbassa - dimenticato     (2)
i12   15.2967   0.3225  8   8.7305 108;     ninolent - dimenticato     (3)
i12   15.5392   0.3252  6   8.6690 107;    nicbbassa - dimenticato     (4)
i12   16.9305   0.3330  3  10.0490  90;      clcalta - agli            (0)
i12   17.3435   0.3093  8  10.1890 109;     ninolent - uomini          (0)
i12   17.5728   0.3330  3  10.5510  90;      clcalta - uomini          (1)
i12   17.9858   0.4698  1  12.8470 110;   carlocarlo - fa              (0)
i12   18.5356   0.3752  6  10.9040 111;    nicbbassa - bene            (0)
i12   18.8308   0.2977 10  11.1007 112;       silvia - bene            (1)
i12   19.0485   0.4100  1  13.8290 113;   carlocarlo - bene            (2)
i12   19.8425   0.3330  3  11.8120  90;      clcalta - ricordare       (0)
i12   20.0955   0.2902  8  11.5382 114;     ninolent - ricordare       (1)
i12   20.3057   0.2920  6  11.9720 115;    nicbbassa - ricordare       (2)
i12   20.5176   0.3330  3  12.5710  90;      clcalta - ricordare       (3)
i12   20.9306   0.6366  8  12.0300 116;     ninolent - i               (0)
i12   21.7282   0.3125  6  12.4020 117;    nicbbassa - peggiori        (0)
i12   21.9608   0.3330  3  13.0460  90;      clcalta - peggiori        (1)
i12   22.2138   0.3902 12  27.6603 118; titino2-soft - peggiori        (2)
i12   23.4709   0.3416  6  13.8990 119;    nicbbassa - non             (0)
i12   23.7325   0.3330  3  14.5500  90;      clcalta - non             (1)
i12   24.1455   0.5116  1  17.3600 120;   carlocarlo - sei             (0)
i12   24.5772   0.3152  8  14.5182 121;     ninolent - sei             (1)
i12   24.9724   0.4379  6  14.5840 122;    nicbbassa - il              (0)
i12   25.5313   0.3330  3  15.0690  90;      clcalta - solo            (0)
i12   25.7843   0.4276 12  33.8636 123; titino2-soft - solo            (1)
i12   26.1319   0.3231  1  18.8523 124;   carlocarlo - solo            (2)
i12   27.1510   0.3330  3  16.3270  90;      clcalta - ma              (0)
i12   27.5640   0.4022  6  16.2110 125;    nicbbassa - la              (0)
i12   28.0462   0.3559  1  20.0480 126;   carlocarlo - storia          (0)
i12   28.3221   0.5607 12  38.1777 127; titino2-soft - storia          (1)
i12   28.8028   0.3275 10  16.9851 128;       silvia - storia          (2)
i12   29.5963   0.3077  8  18.2140 129;     ninolent - e`              (0)
i12   29.9840   0.4281  1  21.2520 130;   carlocarlo - sbadata         (0)
i12   30.3321   0.5132 12  40.9572 131; titino2-soft - sbadata         (1)
i12   30.7653   0.3330  3  18.3770  90;      clcalta - sbadata         (2)
i12   31.0183   0.3623  6  17.8648 132;    nicbbassa - sbadata         (3)
; Envelopes/Positions for voice 3
i32    2.9400  28.6006   0.00   0.00   0.50   8.00
; Room Info for voice 3
i92    3.0200  36.5206   0.00 -24.00
; Printing voice n. 4
i13    2.9700   0.3335  4   0.0000 133;      clcsuss - Ci              (0)
i13    3.3835   0.3817  7   0.2560 134;     nicbsuss - ricordiamo      (0)
i13    3.6851   0.3901  4   0.5171 135;      clcsuss - ricordiamo      (1)
i13    3.9952   0.3817  7   0.8594 134;     nicbsuss - ricordiamo      (2)
i13    4.4569   0.4593  4   1.1300 136;      clcsuss - di              (0)
i13    4.9962   0.3941  8   1.0640 137;     ninolent - te,             (0)
i13    6.2403   0.3872  4   2.5180 138;      clcsuss - maledetto       (0)
i13    6.5475   0.3576  7   2.6196 139;     nicbsuss - maledetto       (1)
i13    6.8251   0.3122  8   2.3963 140;     ninolent - maledetto       (2)
i13    7.0573   0.3576  7   3.1748 139;     nicbsuss - maledetto       (3)
i13    7.4949   0.3509  4   3.4510 141;      clcsuss - generale        (0)
i13    7.7658   0.3557  7   3.5037 142;     nicbsuss - generale        (1)
i13    8.0415   0.3226  8   3.1223 143;     ninolent - generale        (2)
i13    8.4441   0.3364  7   4.0180 144;     nicbsuss - Mladic          (0)
i13    8.7006   0.3283  4   4.3793 145;      clcsuss - Mladic          (1)
i13    8.9489   0.3364  7   4.5309 144;     nicbsuss - Mladic          (2)
i13    9.2053   0.3283  4   4.8759 145;      clcsuss - Mladic          (3)
i13   10.4896   0.3350  8   5.0750  60;     ninolent - vogliamo        (0)
i13   10.7446   0.3609  7   6.0859 146;     nicbsuss - vogliamo        (1)
i13   11.0255   0.3955  4   6.7451 147;      clcsuss - vogliamo        (2)
i13   11.5010   0.3748  7   6.5730 148;     nicbsuss - che             (0)
i13   12.1298   0.3054  4   7.3460 149;      clcsuss - tu              (0)
i13   12.5152   0.2536  8   6.0580 150;     ninolent - non             (0)
i13   12.6888   0.3552  4   7.7692 151;      clcsuss - non             (1)
i13   13.1240   0.3565  7   7.2880 152;     nicbsuss - sia             (0)
i13   13.4005   0.3831  8   6.5711 153;     ninolent - sia             (1)
i13   13.8635   0.3218  7   8.5240 154;     nicbsuss - dimenticato     (0)
i13   14.1054   0.3208  4   8.7458 155;      clcsuss - dimenticato     (1)
i13   14.3462   0.3218  7   9.0077 154;     nicbsuss - dimenticato     (2)
i13   14.5880   0.3118  8   8.6985 156;     ninolent - dimenticato     (3)
i13   14.8199   0.3218  7   9.4914 154;     nicbsuss - dimenticato     (4)
i13   15.8337   0.4665  4  10.8400 157;      clcsuss - agli            (0)
i13   16.3802   0.3920  7  11.1820 158;     nicbsuss - uomini          (0)
i13   16.6922   0.4460  4  11.5680 159;      clcsuss - uomini          (1)
i13   17.2182   0.3026  8  10.5960 160;     ninolent - fa              (0)
i13   17.6008   0.3495  7  12.1800 161;     nicbsuss - bene            (0)
i13   17.8703   0.2933  4  12.3333 162;      clcsuss - bene            (1)
i13   18.4836   0.4371  7  13.3720 163;     nicbsuss - ricordare       (0)
i13   18.8407   0.4470  4  13.3200 164;      clcsuss - ricordare       (1)
i13   19.2077   0.3473  8  11.8625 165;     ninolent - ricordare       (2)
i13   19.6349   0.4023  7  14.3100 166;     nicbsuss - i               (0)
i13   20.1173   0.4082  4  14.1420 167;      clcsuss - peggiori        (0)
i13   20.4454   0.4385  7  14.8435 168;     nicbsuss - peggiori        (1)
i13   20.8040   0.4082  4  14.7984 167;      clcsuss - peggiori        (2)
i13   21.4652   0.2960  8  14.0720 169;     ninolent - non             (0)
i13   21.8412   0.3610  4  16.3740 170;      clcsuss - sei             (0)
i13   22.1222   0.3237  7  16.6517 171;     nicbsuss - sei             (1)
i13   22.5258   0.7633  4  16.7430 172;      clcsuss - il              (0)
i13   23.3691   0.3757  7  16.9040 173;     nicbsuss - solo            (0)
i13   23.6649   0.3552  8  15.0012 151;     ninolent - solo            (1)
i13   23.9401   0.3757  7  17.4954 173;     nicbsuss - solo            (2)
i13   24.9558   0.3071  4  18.4250 174;      clcsuss - ma              (0)
i13   25.3429   0.5390  7  18.2760 175;     nicbsuss - la              (0)
i13   25.9619   0.3683  8  16.7390 176;     ninolent - storia          (0)
i13   26.2502   0.4276  4  19.0816 123;      clcsuss - storia          (1)
i13   26.5978   0.3248  7  18.9445 177;     nicbsuss - storia          (2)
i13   27.1175   0.2331  4  19.7950 178;      clcsuss - e`              (0)
i13   27.4307   0.3752  7  19.0850 111;     nicbsuss - sbadata         (0)
i13   27.7259   0.3477  4  20.1607 179;      clcsuss - sbadata         (1)
i13   27.9935   0.3752  7  19.6754 111;     nicbsuss - sbadata         (2)
i13   28.2887   0.3477  4  20.6960 179;      clcsuss - sbadata         (3)
; Envelopes/Positions for voice 4
i33    2.9700   1.0000 -90.00   0.00   0.00   0.50
i33    3.9700  21.8264   0.00   0.00   0.00   0.50
i33   25.7964   3.0000   0.00 -90.00   0.00   0.50
; Room Info for voice 4
i93    3.0500  33.7464   0.00 -24.00
; Printing voice n. 5
i14    4.5200   0.2764 12   0.1130  15; titino2-soft - Ci              (0)
i14    4.7164   0.2800  1   0.2000 180;   carlocarlo - Ci              (1)
i14    5.0764   0.2930  2   0.3520 181;      cecilia - ricordiamo      (0)
i14    5.2894   0.3656 12   0.9906 182; titino2-soft - ricordiamo      (1)
i14    5.5750   0.2930  2   0.7781 181;      cecilia - ricordiamo      (2)
i14    5.7881   0.3656 12   1.5619 182; titino2-soft - ricordiamo      (3)
i14    6.0737   0.2800  1   1.0740 180;   carlocarlo - ricordiamo      (4)
i14    6.4337   0.2275 10   1.1100 183;       silvia - di              (0)
i14    6.8262   0.3132  2   1.8980 184;      cecilia - te,             (0)
i14    7.0594   0.2800  1   1.7190 180;   carlocarlo - te,             (1)
; Envelopes/Positions for voice 5
i34    4.5200   2.9794  -6.00  -6.00  -6.00   4.00
; Room Info for voice 5
i94    4.6000  10.8994   0.00 -24.00
; Printing voice n. 6
i15   11.1400   0.3200  2   6.4260 185;      cecilia - vogliamo        (0)
i15   11.3800   0.3324  1   6.6754 186;   carlocarlo - vogliamo        (1)
i15   11.6324   0.2948 11   6.2477 187;      titino1 - vogliamo        (2)
i15   12.1582   0.2465  1   7.4140 188;   carlocarlo - che             (0)
i15   12.6957   0.3200  2   7.7000 185;      cecilia - tu              (0)
i15   13.4097   0.2117 11   6.8020 189;      titino1 - non             (0)
i15   13.5414   0.3892  1   8.2772 190;   carlocarlo - non             (1)
i15   14.0106   0.2640 11   6.9770 191;      titino1 - sia             (0)
i15   14.1946   0.2777 10   7.1117 192;       silvia - sia             (1)
i15   14.3923   0.2640 11   7.3450 191;      titino1 - sia             (2)
i15   15.2803   0.3200  2   9.6770 185;      cecilia - dimenticato     (0)
i15   15.5203   0.4109  1   9.7009 193;   carlocarlo - dimenticato     (1)
i15   15.8512   0.3600 10   8.5720 194;       silvia - dimenticato     (2)
i15   16.1312   0.3520 11  10.2241 195;      titino1 - dimenticato     (3)
i15   16.4033   0.3600 10   9.1320 194;       silvia - dimenticato     (4)
; Envelopes/Positions for voice 6
i35   11.1400   5.7833  -6.00  -6.00   6.00   4.00
; Room Info for voice 6
i95   11.2200  13.7033   0.00 -24.00
; Reverb instrument line
i100   3.0000  36.5406   0.8000   0.1500
; 
; Functions used:
; 
f28  0 8192 6 0 1630 0.5 1630 1 836 1 836 1 1630 0.5 1630 0
f93  0 8192 6 0 992 0.5 992 1 2112 1 2112 1 992 0.5 992 0
f64  0 8192 6 0 812 0.5 813 1 2471 1 2471 1 813 0.5 812 0
f32  0 8192 6 0 1359 0.5 1359 1 1378 1 1378 1 1359 0.5 1359 0
f178 0 8192 6 0 1405 0.5 1406 1 1285 1 1285 1 1406 0.5 1405 0
f67  0 8192 6 0 812 0.5 813 1 2471 1 2471 1 813 0.5 812 0
f86  0 8192 6 0 1070 0.5 1070 1 1956 1 1956 1 1070 0.5 1070 0
f39  0 8192 6 0 991 0.5 991 1 2114 1 2114 1 991 0.5 991 0
f108 0 8192 6 0 1016 0.5 1016 1 2064 1 2064 1 1016 0.5 1016 0
f143 0 8192 6 0 1015 0.5 1016 1 2065 1 2065 1 1016 0.5 1015 0
f132 0 8192 6 0 904 0.5 904 1 2288 1 2288 1 904 0.5 904 0
f137 0 8192 6 0 831 0.5 831 1 2434 1 2434 1 831 0.5 831 0
f123 0 8192 6 0 766 0.5 766 1 2564 1 2564 1 766 0.5 766 0
f45  0 8192 6 0 883 0.5 883 1 2330 1 2330 1 883 0.5 883 0
f41  0 8192 6 0 866 0.5 867 1 2363 1 2363 1 867 0.5 866 0
f136 0 8192 6 0 713 0.5 713 1 2670 1 2670 1 713 0.5 713 0
f62  0 8192 6 0 1196 0.5 1196 1 1704 1 1704 1 1196 0.5 1196 0
f106 0 8192 6 0 1132 0.5 1133 1 1831 1 1831 1 1133 0.5 1132 0
f112 0 8192 6 0 1100 0.5 1101 1 1895 1 1895 1 1101 0.5 1100 0
f63  0 8192 6 0 655 0.5 656 1 2785 1 2785 1 656 0.5 655 0
f87  0 8192 6 0 780 0.5 780 1 2536 1 2536 1 780 0.5 780 0
f11  0 8192 6 0 989 0.5 990 1 2117 1 2117 1 990 0.5 989 0
f124 0 8192 6 0 1014 0.5 1014 1 2068 1 2068 1 1014 0.5 1014 0
f16  0 8192 6 0 1555 0.5 1556 1 985 1 985 1 1556 0.5 1555 0
f174 0 8192 6 0 1067 0.5 1067 1 1962 1 1962 1 1067 0.5 1067 0
f121 0 8192 6 0 1039 0.5 1040 1 2017 1 2017 1 1040 0.5 1039 0
f104 0 8192 6 0 526 0.5 527 1 3043 1 3043 1 527 0.5 526 0
f38  0 8192 6 0 1129 0.5 1130 1 1837 1 1837 1 1130 0.5 1129 0
f25  0 8192 6 0 1161 0.5 1162 1 1773 1 1773 1 1162 0.5 1161 0
f114 0 8192 6 0 1129 0.5 1129 1 1838 1 1838 1 1129 0.5 1129 0
f69  0 8192 6 0 1038 0.5 1039 1 2019 1 2019 1 1039 0.5 1038 0
f171 0 8192 6 0 1012 0.5 1012 1 2072 1 2072 1 1012 0.5 1012 0
f151 0 8192 6 0 922 0.5 923 1 2251 1 2251 1 923 0.5 922 0
f130 0 8192 6 0 765 0.5 765 1 2566 1 2566 1 765 0.5 765 0
f55  0 8192 6 0 1128 0.5 1128 1 1840 1 1840 1 1128 0.5 1128 0
f12  0 8192 6 0 1195 0.5 1195 1 1706 1 1706 1 1195 0.5 1195 0
f40  0 8192 6 0 1270 0.5 1270 1 1556 1 1556 1 1270 0.5 1270 0
f165 0 8192 6 0 943 0.5 944 1 2209 1 2209 1 944 0.5 943 0
f129 0 8192 6 0 1064 0.5 1065 1 1967 1 1967 1 1065 0.5 1064 0
f43  0 8192 6 0 1349 0.5 1349 1 1398 1 1398 1 1349 0.5 1349 0
f88  0 8192 6 0 864 0.5 865 1 2367 1 2367 1 865 0.5 864 0
f142 0 8192 6 0 921 0.5 921 1 2254 1 2254 1 921 0.5 921 0
f138 0 8192 6 0 846 0.5 846 1 2404 1 2404 1 846 0.5 846 0
f44  0 8192 6 0 1158 0.5 1159 1 1779 1 1779 1 1159 0.5 1158 0
f179 0 8192 6 0 942 0.5 942 1 2212 1 2212 1 942 0.5 942 0
f147 0 8192 6 0 828 0.5 829 1 2439 1 2439 1 829 0.5 828 0
f126 0 8192 6 0 920 0.5 921 1 2255 1 2255 1 921 0.5 920 0
f20  0 8192 6 0 1097 0.5 1098 1 1901 1 1901 1 1098 0.5 1097 0
f97  0 8192 6 0 652 0.5 652 1 2792 1 2792 1 652 0.5 652 0
f189 0 8192 6 0 1547 0.5 1548 1 1001 1 1001 1 1548 0.5 1547 0
f186 0 8192 6 0 985 0.5 986 1 2125 1 2125 1 986 0.5 985 0
f29  0 8192 6 0 1393 0.5 1393 1 1310 1 1310 1 1393 0.5 1393 0
f163 0 8192 6 0 749 0.5 750 1 2597 1 2597 1 750 0.5 749 0
f183 0 8192 6 0 1440 0.5 1440 1 1216 1 1216 1 1440 0.5 1440 0
f14  0 8192 6 0 1062 0.5 1062 1 1972 1 1972 1 1062 0.5 1062 0
f177 0 8192 6 0 1008 0.5 1009 1 2079 1 2079 1 1009 0.5 1008 0
f96  0 8192 6 0 941 0.5 941 1 2214 1 2214 1 941 0.5 941 0
f18  0 8192 6 0 919 0.5 920 1 2257 1 2257 1 920 0.5 919 0
f30  0 8192 6 0 1264 0.5 1264 1 1568 1 1568 1 1264 0.5 1264 0
f61  0 8192 6 0 1008 0.5 1009 1 2079 1 2079 1 1009 0.5 1008 0
f152 0 8192 6 0 919 0.5 919 1 2258 1 2258 1 919 0.5 919 0
f91  0 8192 6 0 918 0.5 918 1 2260 1 2260 1 918 0.5 918 0
f79  0 8192 6 0 748 0.5 748 1 2600 1 2600 1 748 0.5 748 0
f122 0 8192 6 0 748 0.5 748 1 2600 1 2600 1 748 0.5 748 0
f110 0 8192 6 0 697 0.5 697 1 2702 1 2702 1 697 0.5 697 0
f66  0 8192 6 0 1093 0.5 1093 1 1910 1 1910 1 1093 0.5 1093 0
f85  0 8192 6 0 1486 0.5 1487 1 1123 1 1123 1 1487 0.5 1486 0
f90  0 8192 6 0 984 0.5 984 1 2128 1 2128 1 984 0.5 984 0
f17  0 8192 6 0 1086 0.5 1087 1 1923 1 1923 1 1087 0.5 1086 0
f52  0 8192 6 0 1486 0.5 1486 1 1124 1 1124 1 1486 0.5 1486 0
f107 0 8192 6 0 1007 0.5 1008 1 2081 1 2081 1 1008 0.5 1007 0
f120 0 8192 6 0 640 0.5 641 1 2815 1 2815 1 641 0.5 640 0
f115 0 8192 6 0 1122 0.5 1122 1 1852 1 1852 1 1122 0.5 1122 0
f83  0 8192 6 0 878 0.5 878 1 2340 1 2340 1 878 0.5 878 0
f37  0 8192 6 0 1257 0.5 1258 1 1581 1 1581 1 1258 0.5 1257 0
f159 0 8192 6 0 734 0.5 735 1 2627 1 2627 1 735 0.5 734 0
f133 0 8192 6 0 982 0.5 983 1 2131 1 2131 1 983 0.5 982 0
f119 0 8192 6 0 959 0.5 959 1 2178 1 2178 1 959 0.5 959 0
f31  0 8192 6 0 1340 0.5 1341 1 1415 1 1415 1 1341 0.5 1340 0
f109 0 8192 6 0 1059 0.5 1059 1 1978 1 1978 1 1059 0.5 1059 0
f70  0 8192 6 0 917 0.5 918 1 2261 1 2261 1 918 0.5 917 0
f19  0 8192 6 0 1296 0.5 1296 1 1504 1 1504 1 1296 0.5 1296 0
f82  0 8192 6 0 1152 0.5 1152 1 1792 1 1792 1 1152 0.5 1152 0
f47  0 8192 6 0 1058 0.5 1058 1 1980 1 1980 1 1058 0.5 1058 0
f134 0 8192 6 0 858 0.5 858 1 2380 1 2380 1 858 0.5 858 0
f15  0 8192 6 0 1185 0.5 1186 1 1725 1 1725 1 1186 0.5 1185 0
f182 0 8192 6 0 896 0.5 896 1 2304 1 2304 1 896 0.5 896 0
f168 0 8192 6 0 747 0.5 747 1 2602 1 2602 1 747 0.5 747 0
f74  0 8192 6 0 937 0.5 938 1 2221 1 2221 1 938 0.5 937 0
f161 0 8192 6 0 937 0.5 938 1 2221 1 2221 1 938 0.5 937 0
f139 0 8192 6 0 916 0.5 916 1 2264 1 2264 1 916 0.5 916 0
f50  0 8192 6 0 1057 0.5 1057 1 1982 1 1982 1 1057 0.5 1057 0
f190 0 8192 6 0 841 0.5 842 1 2413 1 2413 1 842 0.5 841 0
f42  0 8192 6 0 1219 0.5 1220 1 1657 1 1657 1 1220 0.5 1219 0
f98  0 8192 6 0 935 0.5 936 1 2225 1 2225 1 936 0.5 935 0
f160 0 8192 6 0 1082 0.5 1083 1 1931 1 1931 1 1083 0.5 1082 0
f99  0 8192 6 0 1005 0.5 1005 1 2086 1 2086 1 1005 0.5 1005 0
f33  0 8192 6 0 1030 0.5 1030 1 2036 1 2036 1 1030 0.5 1030 0
f135 0 8192 6 0 839 0.5 840 1 2417 1 2417 1 840 0.5 839 0
f181 0 8192 6 0 1118 0.5 1118 1 1860 1 1860 1 1118 0.5 1118 0
f118 0 8192 6 0 839 0.5 840 1 2417 1 2417 1 840 0.5 839 0
f164 0 8192 6 0 733 0.5 733 1 2630 1 2630 1 733 0.5 733 0
f150 0 8192 6 0 1292 0.5 1292 1 1512 1 1512 1 1292 0.5 1292 0
f162 0 8192 6 0 1117 0.5 1117 1 1862 1 1862 1 1117 0.5 1117 0
f141 0 8192 6 0 933 0.5 934 1 2229 1 2229 1 934 0.5 933 0
f75  0 8192 6 0 1291 0.5 1292 1 1513 1 1513 1 1292 0.5 1291 0
f78  0 8192 6 0 1116 0.5 1117 1 1863 1 1863 1 1117 0.5 1116 0
f127 0 8192 6 0 584 0.5 584 1 2928 1 2928 1 584 0.5 584 0
f51  0 8192 6 0 1217 0.5 1217 1 1662 1 1662 1 1217 0.5 1217 0
f84  0 8192 6 0 856 0.5 856 1 2384 1 2384 1 856 0.5 856 0
f58  0 8192 6 0 893 0.5 894 1 2309 1 2309 1 894 0.5 893 0
f148 0 8192 6 0 874 0.5 874 1 2348 1 2348 1 874 0.5 874 0
f192 0 8192 6 0 1179 0.5 1180 1 1737 1 1737 1 1180 0.5 1179 0
f76  0 8192 6 0 933 0.5 933 1 2230 1 2230 1 933 0.5 933 0
f60  0 8192 6 0 978 0.5 978 1 2140 1 2140 1 978 0.5 978 0
f131 0 8192 6 0 638 0.5 639 1 2819 1 2819 1 639 0.5 638 0
f156 0 8192 6 0 1050 0.5 1051 1 1995 1 1995 1 1051 0.5 1050 0
f167 0 8192 6 0 802 0.5 803 1 2491 1 2491 1 803 0.5 802 0
f35  0 8192 6 0 1376 0.5 1376 1 1344 1 1344 1 1376 0.5 1376 0
f48  0 8192 6 0 1211 0.5 1211 1 1674 1 1674 1 1211 0.5 1211 0
f102 0 8192 6 0 954 0.5 954 1 2188 1 2188 1 954 0.5 954 0
f153 0 8192 6 0 855 0.5 855 1 2386 1 2386 1 855 0.5 855 0
f65  0 8192 6 0 931 0.5 932 1 2233 1 2233 1 932 0.5 931 0
f71  0 8192 6 0 1524 0.5 1525 1 1047 1 1047 1 1525 0.5 1524 0
f73  0 8192 6 0 1026 0.5 1026 1 2044 1 2044 1 1026 0.5 1026 0
f188 0 8192 6 0 1329 0.5 1329 1 1438 1 1438 1 1329 0.5 1329 0
f111 0 8192 6 0 873 0.5 873 1 2350 1 2350 1 873 0.5 873 0
f128 0 8192 6 0 1000 0.5 1001 1 2095 1 2095 1 1001 0.5 1000 0
f77  0 8192 6 0 1025 0.5 1026 1 2045 1 2045 1 1026 0.5 1025 0
f172 0 8192 6 0 429 0.5 429 1 3238 1 3238 1 429 0.5 429 0
f49  0 8192 6 0 999 0.5 999 1 2098 1 2098 1 999 0.5 999 0
f173 0 8192 6 0 872 0.5 872 1 2352 1 2352 1 872 0.5 872 0
f187 0 8192 6 0 1111 0.5 1112 1 1873 1 1873 1 1112 0.5 1111 0
f95  0 8192 6 0 820 0.5 821 1 2455 1 2455 1 821 0.5 820 0
f21  0 8192 6 0 1175 0.5 1176 1 1745 1 1745 1 1176 0.5 1175 0
f81  0 8192 6 0 910 0.5 911 1 2275 1 2275 1 911 0.5 910 0
f185 0 8192 6 0 1024 0.5 1024 1 2048 1 2048 1 1024 0.5 1024 0
f140 0 8192 6 0 1049 0.5 1050 1 1997 1 1997 1 1050 0.5 1049 0
f194 0 8192 6 0 910 0.5 910 1 2276 1 2276 1 910 0.5 910 0
f195 0 8192 6 0 930 0.5 931 1 2235 1 2235 1 931 0.5 930 0
f117 0 8192 6 0 1048 0.5 1049 1 1999 1 1999 1 1049 0.5 1048 0
f101 0 8192 6 0 756 0.5 756 1 2584 1 2584 1 756 0.5 756 0
f155 0 8192 6 0 1021 0.5 1021 1 2054 1 2054 1 1021 0.5 1021 0
f158 0 8192 6 0 835 0.5 836 1 2425 1 2425 1 836 0.5 835 0
f145 0 8192 6 0 998 0.5 998 1 2100 1 2100 1 998 0.5 998 0
f144 0 8192 6 0 974 0.5 974 1 2148 1 2148 1 974 0.5 974 0
f26  0 8192 6 0 1577 0.5 1578 1 941 1 941 1 1578 0.5 1577 0
f57  0 8192 6 0 1282 0.5 1283 1 1531 1 1531 1 1283 0.5 1282 0
f54  0 8192 6 0 997 0.5 998 1 2101 1 2101 1 998 0.5 997 0
f53  0 8192 6 0 871 0.5 871 1 2354 1 2354 1 871 0.5 871 0
f105 0 8192 6 0 890 0.5 890 1 2316 1 2316 1 890 0.5 890 0
f146 0 8192 6 0 907 0.5 908 1 2281 1 2281 1 908 0.5 907 0
f94  0 8192 6 0 950 0.5 951 1 2195 1 2195 1 951 0.5 950 0
f56  0 8192 6 0 1368 0.5 1368 1 1360 1 1360 1 1368 0.5 1368 0
f46  0 8192 6 0 870 0.5 871 1 2355 1 2355 1 871 0.5 870 0
f176 0 8192 6 0 889 0.5 890 1 2317 1 2317 1 890 0.5 889 0
f72  0 8192 6 0 870 0.5 871 1 2355 1 2355 1 871 0.5 870 0
f92  0 8192 6 0 659 0.5 659 1 2778 1 2778 1 659 0.5 659 0
f100 0 8192 6 0 996 0.5 997 1 2103 1 2103 1 997 0.5 996 0
f27  0 8192 6 0 1367 0.5 1367 1 1362 1 1362 1 1367 0.5 1367 0
f68  0 8192 6 0 1108 0.5 1109 1 1879 1 1879 1 1109 0.5 1108 0
f59  0 8192 6 0 1107 0.5 1107 1 1882 1 1882 1 1107 0.5 1107 0
f113 0 8192 6 0 799 0.5 799 1 2498 1 2498 1 799 0.5 799 0
f125 0 8192 6 0 814 0.5 815 1 2467 1 2467 1 815 0.5 814 0
f184 0 8192 6 0 1046 0.5 1046 1 2004 1 2004 1 1046 0.5 1046 0
f166 0 8192 6 0 814 0.5 815 1 2467 1 2467 1 815 0.5 814 0
f180 0 8192 6 0 1170 0.5 1170 1 1756 1 1756 1 1170 0.5 1170 0
f170 0 8192 6 0 907 0.5 908 1 2281 1 2281 1 908 0.5 907 0
f191 0 8192 6 0 1241 0.5 1241 1 1614 1 1614 1 1241 0.5 1241 0
f149 0 8192 6 0 1072 0.5 1073 1 1951 1 1951 1 1073 0.5 1072 0
f193 0 8192 6 0 797 0.5 797 1 2502 1 2502 1 797 0.5 797 0
f154 0 8192 6 0 1018 0.5 1018 1 2060 1 2060 1 1018 0.5 1018 0
f22  0 8192 6 0 1240 0.5 1240 1 1616 1 1616 1 1240 0.5 1240 0
f36  0 8192 6 0 927 0.5 927 1 2242 1 2242 1 927 0.5 927 0
f23  0 8192 6 0 1239 0.5 1239 1 1618 1 1618 1 1239 0.5 1239 0
f80  0 8192 6 0 948 0.5 949 1 2199 1 2199 1 949 0.5 948 0
f175 0 8192 6 0 607 0.5 608 1 2881 1 2881 1 608 0.5 607 0
f169 0 8192 6 0 1107 0.5 1107 1 1882 1 1882 1 1107 0.5 1107 0
f34  0 8192 6 0 1166 0.5 1167 1 1763 1 1763 1 1167 0.5 1166 0
f89  0 8192 6 0 645 0.5 646 1 2805 1 2805 1 646 0.5 645 0
f103 0 8192 6 0 782 0.5 783 1 2531 1 2531 1 783 0.5 782 0
f24  0 8192 6 0 868 0.5 868 1 2360 1 2360 1 868 0.5 868 0
f157 0 8192 6 0 702 0.5 702 1 2692 1 2692 1 702 0.5 702 0
f13  0 8192 6 0 1105 0.5 1106 1 1885 1 1885 1 1106 0.5 1105 0
f116 0 8192 6 0 514 0.5 515 1 3067 1 3067 1 515 0.5 514 0
