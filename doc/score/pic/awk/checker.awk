#
# $Id: checker.awk 38 2014-02-12 14:44:53Z nicb $
#
function print_output(tag, start, dur,
	end)
{
	end = start + dur;

	printf("draw(%d,%.4f,%.4f)\n", tag+1, start, end);
}
BEGIN {
	_start_	= start != "" ? start : 2.7;
	_end_	= end != "" ? end : 5;

	print ".PS\ncopy \"check-header.pic\"";
	printf("globals(%.4f,%.4f)\n", _start_, _end_);
}
END {
	print ".PE";
}
/^i1/ && $2 >= _start_ && $2 < _end_ {
	print_output($4, $2, $3);
}
#
# $Log: checker.awk,v $
# Revision 0.0  1999/08/22 15:44:10  nicb
# Initial Revision
#
