require 'spec_helper'

describe Rdisplay::CsoundLine do

  it 'parses correctly a csound line' do
		csound_line = 'i1     2.9200 0.3310  0   0.0000  11;     catbassa - Ci              (0)'
		cl = Rdisplay::CsoundLine.new(csound_line)
		expect(cl.instr).to eq(1)
		expect(cl.at).to eq(2.92)
		expect(cl.dur).to eq(0.331)
		expect(cl.soundin).to eq(0)
		expect(cl.skip).to eq(0.0)
		expect(cl.speaker).to eq('catbassa')
		expect(cl.word).to eq('Ci')
		expect(cl.fragno).to eq(0)
  end

end
