module Rdisplay

	class CsoundLine

		attr_reader :line, :instr, :at, :dur, :soundin, :skip, :speaker, :word, :fragno

		def initialize(line)
			@line = line
			parse
		end

		def render
		end

	private

		def parse
			(@instr, @at, @dur, @soundin, @skip, notused, @speaker, dummy, @word, @fragno) = self.line.split(/\s+/)
			# some need some cleanup
			self.instr.sub!(/^\s*i\s*/, '')
			self.fragno.sub!(/^\(([0-9]+)\)/, '\1')
		end

end
