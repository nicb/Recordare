module Rdisplay

	class CsoundPicRenderer

		DEFAULT_FILENAME = File.expand_path(File.join(['..'] * 4, 'csound', 'voices', 'r-modified.sco'), __FILE__)

		attr_reader :filename, :lines

		def initialize(fn = DEFAULT_FILENAME)
			@filename = fn
			@lines = []
		end

		def render
			parse if self.lines.empty?
			self.lines.each do
				|l|
				l.render
			end
		end

	private

		def parse
			File.open(self.filename, 'r') do
				|fh|

				while((line = fh.gets) && !fh.eof?)
					line.chomp
					next if line ~ /^\s*$/ || line ~ /^\s*;/
					self.lines << CsoundLine.new(line) if line ~ /^\s*i\s*[0-9]*/
				end

			end
		end

end
