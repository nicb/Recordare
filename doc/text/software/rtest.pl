#!/usr/bin/perl
#
# test for the Recordare random drawing machine
#

use Recordare::RandomCell;
use Recordare::RandomDraw;

use strict;

my @cells =
(
	new Recordare::RandomCell('sixty', 0.6),
	new Recordare::RandomCell('thirty', 0.3),
	new Recordare::RandomCell('ten', 0.1),
);

my $rd = new Recordare::RandomDraw(\@cells);

my $i = 0;

for ($i = 0; $i < 100000; ++$i)
{
	print $rd->draw(), "\n";
}
