#!/usr/bin/perl
# vi:set nowrap ts=4:
#
# $Id: mix.pl,v 0.1 1999/06/26 17:03:52 nicb Exp nicb $
#
# 
#

use Recordare::Speakers;
use Recordare::Index;
use Recordare::CsoundLine;
use Recordare::Paths;

use strict;

#
# start of main
#

my @speakers =
(
	'catbassa',
	'carlocarlo',
	'clcsuss',
	'nicbalta',
);

$main::ref_idx = 0;
$main::current_at = 0;
$main::secs = 0.23;

my $paths = new Recordare::Paths();
my $Speakers = new Recordare::Speakers(\@speakers);
my $num_words = $Speakers->size($main::ref_idx);
my $i = 0;

$main::current_speaker = new Recordare::Index($Speakers->num(),
	$Speakers->num());

print_header();

for ($i = 0; $i < $num_words; ++$i)
{
	process_word($i, $Speakers, $main::secs);
}

#
# end of main
#

sub process_word($$$)
{
	my ($idx, $speakers, $secs) = @_;

	my $ref_speaker = $speakers->speaker($main::ref_idx);
	my $ref_word = $ref_speaker->word($idx);
	my $ref_dur  = $ref_word->dur();
	my $segments = $ref_dur / $secs;
	my $i = 0;

	for ($i = 0; $i < $segments; ++$i)
	{
		my $speaker_idx = $main::current_speaker->next();
		$main::current_at = produce_fragment($i, $main::current_at, $secs,
			$speakers->speaker($speaker_idx), $ref_word, $ref_speaker);
	}
}

sub produce_fragment($$$$)
{
	my ($idx, $current_at, $secs, $speaker, $ref_word, $ref_speaker) = @_;

	my $word = $speaker->word($ref_word->idx());
	my $ratio = $word->dur() / $ref_word->dur();
	my $word_secs = ($secs * $ratio);
	my $relative_offset = $idx * $word_secs;
	my $reference_offset = $idx * $secs;
	my $real_dur = $word_secs;
	my $next_at = $current_at + $real_dur;
	my $real_offset = $word->action_time() + $relative_offset;
	my $real_reference_offset = $ref_word->action_time() + $reference_offset;
	my $sco_line = new Recordare::CsoundLine($current_at, $real_dur,
		$speaker, $real_offset, $ref_word, $ref_speaker,
		$real_reference_offset, $word_secs);
	
	$sco_line->pre_dovetail(0.6);
	$sco_line->post_dovetail(0.6);

	$sco_line->print();

	return $next_at;
}

sub print_header()
{
	printf("; vi:set nowrap:\n; Produced by %s %s\n;\n", qw($RCSfile: mix.pl,v $),
			qw($Revision: 0.1 $));
	print ";f1 0 4096 20 7 1 3\t; Kaiser window\n";
	print ";f1 0 4096 6 0 40 5 40 15 80 85 40 95 40 100 3616 100 40 95 40 85 80 15 40 5 40 0; cubic window\n";
	print ";f1 0 4096 20 1		; Hamming window\n";
	print "f1 0 4096 20 2		; Hanning window <<---- \n";
	print ";f1 0 4096 20 3		; Bartlett (triangle) window\n";
	print ";f1 0 4096 20 4		; Blackman (3 terms) window\n";
	print ";f1 0 4096 20 5		; Blackman (4 terms) window\n";
	print ";f1 0 4096 20 6		; Gaussian window\n";
	print ";f1 0 4096 20 9		; sync window\n";
}
#
# $Log: mix.pl,v $
# Revision 0.1  1999/06/26 17:03:52  nicb
# added reference voice to balance
#
# Revision 0.0  1999/06/25 14:49:15  nicb
# Initial Revision
#
