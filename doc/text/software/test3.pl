#!/usr/bin/perl
#
# test for the Recordare objects
#

use Recordare::Text;

use strict;

my $tstring = "catbassa";

my $text = new Recordare::Text($tstring);

print $text->version(), "\n";
print $text->sound_path(), "\n";
print $text->data_path(), "\n";
print $text->speaker(), "\n";
print $text->size(), "\n";
print "index before: ", $text->index(), "\n";
my $returned = $text->index(4);
print "index after setting 4: ", $text->index(), "\n";
print "index() returned: ", $returned, "\n";

my $i = 0;
my $num_words = $text->size();

for ($i = 0; $i < $num_words; ++$i)
{
	print $i, "\t", $text->action_time($i), "\t", $text->dur($i), "\t",
		$text->word($i), "\n";
}
