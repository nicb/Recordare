## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Score2Csound.pm - score to csound converter class for Recordare
##
##
## Score2Csound.pm,v 0.17 2000/06/26 22:05:18 nicb Exp
##

use	Recordare::ScoreInterpreter;
use	Recordare::Globals;
use Recordare::Tools;

package Score2Csound;

use strict;

@Score2Csound::ISA   = qw( ScoreInterpreter );

=pod

=head1 NAME

C<Recordare::Score2Csound> - score to csound converter class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::Score2Csound;

 my $lh = new Score2Csound($file);

=cut

sub new($$)
{
	my ($class, $file) = @_;

	my $pkg = $class->SUPER::new($file);
	$pkg->version('0.17');
	$pkg->{'_score_end_'} = 0;
	$pkg->{'_score_start_'} = 1000000;		# start from a high number
	$pkg->{'_table_numbers_'} = {};
	$pkg->{'_position_initialization_'} = {};
	$pkg->clear_voices();
	$pkg->clear_mixers();

	my $g = new Globals();
	$pkg->{'_last_table_number_'} = $g->require('first_table');
	$pkg->{'_insno_offset_'} = $g->require('first_instrument');

	return $pkg;
}
=pod

=head1 DESCRIPTION

C<Recordare::Score2Csound> is the score 2 csound
converter class for I<Recordare> scores.

=head1 METHODS

=over 4

=item C<print_comment($fh,$line)>

This is one of the mandatory functions to
be implemented by C<Recordare::ScoreInterpreter>

=cut

sub print_comment($$$)
{
	my ($this, $fh, $line) = @_;

	printf $fh (";\n;%s\n;\n", $line);
}

=pod

=item C<print_section($fh, $section)>

this is the mandatory section function

=cut

sub print_section($$$)
{
	my ($this, $fh, $section) = @_;
	
	$this->clear_voices();
	$this->clear_mixers();
	$this->set_end($section->dur());

	$this->print_comment($fh, ' ' . $section->notes());
	$this->do_print_section($fh, $section);

	$this->SUPER::print_section($fh, $section);
	
	$this->print_reverb($fh);
	$this->print_functions($fh);		# print all table functions
	$this->print_end_of_section($fh);	# clear functions and put an 's'
}

sub do_print_section($$$)
{
	my ($this, $fh, $section) = @_;
	#
	# nothing being done here (but can be used by sub-classes)
	#
}

=pod

=item C<print_voice($fh, $voice)>

...and this is another one.

=cut

sub print_voice($$$)
{
	my ($this, $fh, $voice) = @_;
	my $last_end = 0;
	my $rat = $voice->real_at();

	$this->set_voices();

	$this->set_start($rat);
	$this->print_voice_start($fh, $voice);

	for(my $cur_word = $voice->first_word(); $cur_word <= $voice->last_word();
		++$cur_word)
	{
		$last_end = $this->print_word($fh, $cur_word, $voice, $last_end);
	}

	$this->set_end($last_end);
	$this->print_voice_end($fh, $voice, $last_end);

	$this->print_mixer($fh, $voice, $this->{'_printed_voice_'});
	$this->print_envelopes($fh, $voice, $this->{'_printed_voice_'});
	$this->print_position($fh, $voice, $this->{'_printed_voice_'});
	$this->increase_voices();
}

sub print_voice_start($$$)
{
	my ($this, $fh, $voice) = @_;

	$this->print_comment($fh, "Printing voice n. " .
		++$this->{'_printed_voice_'});
	$this->print_comment($fh,
		sprintf("%1s %8s %8s %2s %8s %3s", 'i', '  at  ',
			'  dur  ', 'fh', ' offset ', 'fun'));
}

sub print_voice_end($$$$)
{
	my ($this, $fh, $voice, $last_end) = @_;
	#
	# not used here (but maybe used in sub-classes)
	#
}

$Score2Csound::NUM_OVERLAPS = 3;

sub clear_voices($)
{
	my $this = shift;

	$this->{'_allocated_voices_'} = $this->{'_cur_voice_'} = 0;
}

sub increase_voices($)
{
	my $this = shift;

	$this->{'_allocated_voices_'} += $Score2Csound::NUM_OVERLAPS;
}

sub set_voices($)
{
	my $this = shift;

	$this->{'_cur_voice_'} = 0;
}

sub get_initial_voice($)
{
	my $this = shift;

	return $this->{'_allocated_voices_'};
}

sub get_voice($)
{
	my $this = shift;
	my $result = $this->{'_cur_voice_'} + $this->{'_allocated_voices_'};

	++$this->{'_cur_voice_'};
	$this->{'_cur_voice_'} %= $Score2Csound::NUM_OVERLAPS;

	return $result;
}

sub clear_mixers($)
{
	my $this = shift;

	$this->{'_cur_mixer_'} = 0;
}

sub get_mixer($)
{
	my $this = shift;
	my $result = $this->{'_cur_mixer_'};

	++$this->{'_cur_mixer_'};

	return $result;
}

sub print_mixer($$$$)
{
	my ($this, $fh, $voice, $vnum) = @_;
	my $g = new Globals();
	my $reader = $g->require('first_instrument');
	my $instoff = $g->require('compressor_offset');
	my $instrno = $reader+$instoff;
	my $ivoice = $this->get_initial_voice();
	my $mixer = $this->get_mixer();
	my $at = $voice->real_at();
	my $end = $this->score_end();
	my $dur = $end-$at;

	$this->print_comment($fh, 'compression instrument');
	printf $fh ("i%3d %8.4f %8.4f %2d %2d %2d %2d\n", $instrno, $at, $dur,
		$mixer, $ivoice, $ivoice+1, $ivoice+2);
}

sub set_end($$)
{
	my ($this, $end) = @_;
	
	$this->score_end($end) if ($end > $this->score_end());
}

sub set_start($$)
{
	my ($this, $start) = @_;
	
	$this->score_start($start) if ($start < $this->score_start());
}

sub print_word($$$$$)
{
	my ($this, $fh, $idx, $voice, $last_end) = @_;
	my $word = $voice->reference()->word($idx);
	my $nsegs = Tools::round($word->dur()/$voice->segment_dur());
	my $nat_offset = $voice->at()+($word->action_time()-
		$voice->reference()->word($voice->first_word())->action_time());
	my $offset = $this->calculate_offset($voice, $word, $last_end, $nat_offset);
	my $g = new Globals();
	my $dovetail = $g->require('dovetail');

	for (my $i = 0; $i < $nsegs; ++$i)
	{
		$offset = $this->print_segment($fh, $i, $voice, $word, $offset,
			$nsegs, $dovetail);
	}

	$voice->instrument()->end_time($offset+($dovetail*2));

	return $voice->instrument()->end_time();
}

sub print_segment($$$$$$$$)
{
	my ($this, $fh, $idx, $voice, $word, $at, $nsegs, $dt) = @_;
	my $speaker = $voice->draw();
	my $widx = $word->idx();
	#
	# $seg_dur and $dur will be different at the end of a word, because
	# $dur will include the ending of the word, while $seg_dur will just
	# be the result of a division into a number of segments
	#
	my ($seg_dur, $dur) = $this->calculate_segment_dur($speaker, $idx,
		$voice, $word, $nsegs);
	my $file_offset = $speaker->word($widx)->action_time()+($seg_dur*$idx);
	my $hop = $dur + $dt; # segment + plus dove-tailing
	my $result = $at+$hop;

	my $edge = $dt;
	my $rat = $at > 0 ? $at-$edge : $at;
	$rat = $rat < 0 ? 0 : $rat;
	my $rdur = $dur+($edge*2);
	my $func = $this->select_function(sprintf("%.4f", $rdur));

	$this->do_print_segment($fh, $rat, $rdur, $speaker,
		$file_offset, $func, $word, $idx);

	return $result;
}

sub do_print_segment($$$$$$$$$)
{
	my ($this, $fh, $rat, $rdur, $speaker, $file_offset, $func,
		$word, $idx) = @_;
	#my $instrno = $voice->instrument()->instrument();
	my $g = new Globals();
	my $instrno = $g->require('first_instrument');

	printf $fh ("i%-3d %8.4f %8.4f %2d %8.4f %3d %2d; %14s - %-12s (%d)\n",
										$instrno,
										$rat,
										$rdur,
										$speaker->index(),
										$file_offset,
										$func,
										$this->get_voice(),
										$speaker->name(),
										$word->word(),
										$idx);
}

sub calculate_segment_dur($$$$$$)
{
	my ($this, $speaker, $idx, $voice, $word, $nsegs) = @_;
	my $dur = 0;
	my $lastsegnum = $nsegs-1;
	my $widx = $word->idx();
	my $word_dur = $speaker->word($widx)->dur();
	my $seg_dur = $word_dur/$nsegs;

	if ($idx < $lastsegnum)
	{
		$dur = $seg_dur;
	}
	else
	{
		my $rest = $seg_dur*($nsegs-1);
		$dur = $word_dur-$rest;
	}

	return ($seg_dur, $dur);
}

sub calculate_offset($$$$$)
{
	my ($this, $voice, $word, $last_end, $nat_offset) = @_;
	my $delta = 0;
	my $idx = $word->idx();

	if (defined($voice->reference->word($idx-1)))
	{
		my $previous_end = $voice->reference->word($idx-1)->action_time()+
			$voice->reference->word($idx-1)->dur();
		my $cur_start = $voice->reference->word($idx)->action_time();
		$delta = $cur_start-$previous_end;
		$delta = $delta < 0 ? 0 : $delta;
	}

	my $nat_start = $last_end + $delta;
	my $result = $nat_offset > $nat_start ? $nat_offset : $nat_start;

	return $result;
}

sub select_function($$)
{
	my ($this, $dur) = @_;
	my $result = -1;

	if (!defined($this->{'_table_numbers_'}->{$dur}))
	{
		$this->{'_table_numbers_'}->{$dur} = ++$this->{'_last_table_number_'};
	}

	$result = $this->{'_table_numbers_'}->{$dur};

	return $result;
}

sub print_functions($$)
{
	my ($this, $fh) = @_;
	my $k = '';

	$this->print_comment($fh, 'Functions used:');

	foreach $k (keys %{$this->{'_table_numbers_'}})
	{
		$this->print_function($fh, $k, $this->{'_table_numbers_'}->{$k});
	}
}

sub print_function($$$$)
{
	my ($this, $fh, $dur, $id) = @_;
	$dur = $dur + 0.0;					# coerce into float number
	my $g = new Globals();
	my $dt = $g->require('dovetail');
	my $ep = $dt/$dur;					# edge percentage
	my $size = $g->require('function_size');
	my $es = int($size*$ep);			# edge samples
	my $ss = $size-($es*2);				# sustain samples

	my @halfes = ();					# edge sizes
	$halfes[0] = int($es/2);
	$halfes[1] = $es-$halfes[0];

	my @halfss = ();					# sustain sizes
	$halfss[0] = int($ss/2);
	$halfss[1] = $ss-$halfss[0];

	printf $fh ("f%-3d 0 %-4d 6 0 %3d 0.5 %3d 1 %3d 1 %3d 1 %3d 0.5 %3d 0\n",
		$id, $size, $halfes[0], $halfes[1], $halfss[0], $halfss[1],
		$halfes[1], $halfes[0]);
}

sub print_end_of_section($$)
{
	my ($this, $fh) = @_;
	my $cur_sec = $this->linecontent($this->current_section());

	$this->clear_functions($fh);

	printf $fh (";\ns ; END OF SECTION %s\n;\n", $cur_sec->notes());

	$this->reset_everything_else();
}

sub clear_functions($$)
{
	my ($this, $fh) = @_;
	my $k = "";
	my $end = $this->score_end();

	foreach $k (keys %{$this->{'_table_numbers_'}})
	{
		my $idx = $this->{'_table_numbers_'}{$k} * -1;

		printf $fh ("f%-4d %8.4f\n", $idx, $end); 
		delete($this->{'_table_numbers_'}->{$k});
	}
}

sub reset_everything_else($)
{
	my $this = shift;

	$this->{'_score_end_'} = 0;
	$this->{'_score_start_'} = 1000000;		# start from a high number

	my $k = "";
	foreach $k (%{$this->{'_position_initialization_'}})
	{
		delete($this->{'_position_initialization_'}->{$k});
	}

	my $g = new Globals();
	$this->{'_last_table_number_'} = $g->require('first_table');
	$this->{'_insno_offset_'} = $g->require('first_instrument');
}

=pod

=item C<print_envelopes($fh, $voice, $voicenum)>

this method prints all envelope/positions indications
for a given C<$voice> into the C<$fh> filehandle.

=cut

sub print_envelopes($$$$)
{
	my ($this, $fh, $voice, $vnum) = @_;

	$this->print_comment($fh, " Envelopes/Positions for voice $vnum");

	for (my $i = 0; $i < scalar(@{$voice->envelopes()}); ++$i)
	{
		$this->print_envelope($fh, $voice->envelope($i), $voice);
	}
}

sub print_envelope($$$$)
{
	my ($this, $fh, $env, $voice) = @_;
	my $end = $this->score_end();

	$env->last_time($end);
	printf $fh ("i%-3d %8.4f %8.4f %6.2f %6.2f\n",
								$voice->instrument()->envelope_instrument(),
								$env->start_time(),
								$env->end_time()-$env->start_time(),
								$env->start(),
								$env->end());
}

=pod

=item C<print_position($fh, $voice, $voicenum)>

this method prints all room position information
for a given C<$voice> into the C<$fh> filehandle.

=cut

sub print_position($$$$)
{
	my ($this, $fh, $voice, $vnum) = @_;
	my $g = new Globals();
	my $added_dur = $g->require('added_dur');# add for all decays to complete
	my $end = $voice->instrument()->end_time();
	my $at = $voice->real_at();
	my $full_dur = ($end-$at)+$added_dur;
	my $off = $g->require('envelope_n_offset')+$g->require('first_instrument')-1;
	my $posinsno = $voice->instrument()->envelope_instrument()-$off;

	$this->print_comment($fh, " Movement information for voice $vnum");

	printf $fh ("i%-3d %8.4f %8.4f %8.4f %8.4f %8.4f %8.4f\n",
								$posinsno,
								$at,
								$full_dur,
								$voice->position()->startx(),
								$voice->position()->endx(),
								$voice->position()->starty(),
								$voice->position()->endy());

	$this->print_comment($fh, " Room Info for voice $vnum");

	printf $fh ("i%-3d %8.4f %8.4f %6.2f %6.2f\n",
								$voice->instrument()->position_instrument(),
								$at,
								$full_dur,
								$g->require('direct_attenuation'),
								$g->require('reverb_attenuation'));
}

=pod

=item C<print_reverb($fh)>

this method prints the reverb line needed
for the entire score into the C<$fh> filehandle.

=cut

sub print_reverb($$)
{
	my ($this, $fh) = @_;
	my $g = new Globals();
	my $added_dur = $g->require('added_dur');# add for all decays to complete
	my $start  = $this->score_start();
	my $end = $this->score_end();
	my $rev_decay = $g->require('reverb_decay');
	my $rev_rdelta = $g->require('reverb_right_delta');
	my $rev_instrument = $g->require('reverb_instrument');

	$this->print_comment($fh, "Reverb instrument line");

	printf $fh ("i%-3d %8.4f %8.4f %8.4f %8.4f\n",
								$rev_instrument,
								$start,
								($end-$start)+$added_dur,
								$rev_decay,
								$rev_decay + $rev_rdelta);
}

=pod

=item C<print_header($fh)>

prints a header containing version information,
tables etc.

=cut

sub print_header($$)
{
	my $this = shift;
	my $fh = shift || \*STDOUT;
	
	print $fh ("; vi:set nowrap:\n;\n");
	print $fh ("; Produced automagically by:\n;\n");
	printf $fh ($this->all_versions('; '));

	printf $fh (";\n; Room definition data\n;              xL   yL   xR  yR  xmax   xmin   ymax    ymin   X    Y\n");
	printf $fh ("f1   0 16 -2 -1.25  2  1.25  2  7.195 -7.195    9.06   -2.8 0     3.13; room\n;\n");
}

=pod

=item C<score_end(...)>

sets/gets the end of the score

=cut

sub score_end
{
	my ($this, $end) = @_;
	
	$this->{'_score_end_'} = defined($end) ? $end : $this->{'_score_end_'};

	return $this->{'_score_end_'};
}

=pod

=item C<score_start(...)>

sets/gets the start of the score

=cut

sub score_start
{
	my ($this, $start) = @_;
	
	$this->{'_score_start_'} = defined($start) ? $start : $this->{'_score_start_'};

	return $this->{'_score_start_'};
}

=pod

=back

=head1 VERSION

 Score2Csound.pm,v 0.17 2000/06/26 22:05:18 nicb Exp
 

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# Score2Csound.pm,v
# Revision 0.17  2000/06/26 22:05:18  nicb
# still better encapsulation of time calculation support functions
#
# Revision 0.16  2000/06/26 16:16:43  nicb
# adjusted action times and durations of instruments
#
# Revision 0.15  2000/06/26 15:49:27  nicb
# re-organized sources for better encapsulation of computations
#
# Revision 0.14  2000/06/25 06:47:52  nicb
# added mixing functions
#
# Revision 0.13  2000/06/23 13:55:43  nicb
# added voice compression instrument
#
# Revision 0.12  2000/06/11 08:48:16  nicb
# modified room dimension data
#
# Revision 0.11  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.10  1999/09/11 17:23:16  nicb
# added end-of-word segment handling
#
# Revision 0.9  1999/09/04 17:00:31  nicb
# added position movement
#
# Revision 0.8  1999/09/04 15:37:04  nicb
# added section handling
#
# Revision 0.7  1999/09/03 10:27:20  nicb
# added reference speaker handling in voice lines (not used, though)
#
# Revision 0.6  1999/09/02 17:57:56  nicb
# added position instrumentation
#
# Revision 0.5  1999/09/02 13:51:44  nicb
# added global table function offset handling
#
# Revision 0.4  1999/08/23 17:20:09  nicb
# better rounding on function envelopes
#
# Revision 0.3  1999/08/22 15:57:16  nicb
# added function printing
#
# Revision 0.2  1999/08/19 17:37:23  nicb
# added print envelope methods
#
# Revision 0.1  1999/08/18 08:19:49  nicb
# added dynamic instrument allocation
#
# Revision 0.0  1999/08/18 06:59:34  nicb
# Initial Revision
#
# Revision 1.1  1999/08/18 06:58:16  nicb
# Initial revision
#
