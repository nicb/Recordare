## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Word.pm - word class for Recordare Objects
##
##
## $Id: Word.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package Word;

use strict;

@Word::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Word> - word class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Word;

 my $lh = new Recordare::Word($line);

=cut

sub new($$)
{
	my ($class, $line) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.2 $));

	$pkg->parse($line);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::Word> is the class for I<Recordare>
word objects. It contains the description of a word
taken from the data collection. It is used by
the C<Recordare::Text> object.

=head1 METHODS

=over 4

=item C<idx()>

returns the index of the word in the data text

=cut

sub idx($)
{
	my ($self) = @_;

	return $self->{_idx_};
}

=pod

=item C<action_time()>

returns the action time of the word in the data text

=cut

sub action_time($)
{
	my ($self) = @_;

	return $self->{_at_};
}

=pod

=item C<dur()>

returns the duration time of the word in the data text

=cut

sub dur($)
{
	my ($self) = @_;

	return $self->{_dur_};
}

=pod

=item C<word()>

returns the actual word from the data text

=cut

sub word($)
{
	my ($self) = @_;

	return $self->{_word_};
}

=pod

=item C<notes()>

returns the actual notes from the data text.
Notes are optional, and they are used to insert
remarks about the specific word (such as
C<MISSING> or C<INCOMPLETE>, etc.)

=cut

sub notes($)
{
	my ($self) = @_;

	return $self->{_notes_};
}

=pod

=item C<parse($line)>

This is an internal method of the object to
parse a line which should be presented in the
C<data> format, that is:

<idx>|<at>|<dur>|<word>[|<notes>]

as the square brackets indicate, the C<notes>
field is optional

=cut

sub parse($$)
{
	my ($self,$line) = @_;

	chop $line;
	my ($idx, $at, $dur, $word, $annotations, @not_used) = split('\|', $line);
	$self->{_idx_} = $idx;
	$self->{_at_} = $at;
	$self->{_dur_} = $dur;
	$self->{_word_} = $word;
	$self->{_notes_} = $annotations;
}

=pod

=back

=head1 VERSION

 $Id: Word.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Word.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/07/29 13:35:04  nicb
# added annotations field
#
# Revision 0.0  1999/06/24 16:09:16  nicb
# Initial Revision
#
