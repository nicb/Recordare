## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreEnvelope.pm - score speaker class for Recordare
##
##
## $Id: ScoreEnvelope.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package ScoreEnvelope;

use strict;

@ScoreEnvelope::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::ScoreEnvelope> - score speaker class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreEnvelope;

 my $lh = new Recordare::ScoreEnvelope($at, $start, $endtime, $end, $time_offset);

=cut

sub new($$$$$$)
{
	my ($class, $at, $start, $endtime, $end, $time_offset) = @_;

	my $pkg = $class->SUPER::new('$Revision: 0.2 $');

	$pkg->{'_offset_'} = $time_offset;
	$pkg->{'_start_time_'} = $at;
	$pkg->{'_start_amp_'} = $start;
	$pkg->{'_end_time_'} = $endtime;
	$pkg->{'_end_amp_'} = $end;

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::ScoreEnvelope> is the class for I<Recordare>
score envelopes, essentially a couples of time, amplitude
values where amplitudes is an attenuation value in dB
(0=unity gain) and times is an relative time in seconds
where this amplitude node should be occur.

=head1 METHODS

=over 4

=item C<start()>

gets the start amplitude value.

=cut

sub start($)
{
	my $this = shift;

	return $this->{'_start_amp_'};
}

=pod

=item C<start_time()>

gets the absolute time of the starting node, in seconds.

=cut

sub start_time($)
{
	my $this = shift;
	my $result = $this->{'_start_time_'} < 0 ?
		$this->last_time()+$this->{'_start_time_'} :
		$this->offset()+$this->{'_start_time_'};

	return $result;
}

=pod

=item C<end()>

gets the end amplitude value.

=cut

sub end($)
{
	my $this = shift;

	return $this->{'_end_amp_'};
}

=pod

=item C<end_time()>

gets the absolute time of the end node, in seconds.

=cut

sub end_time($)
{
	my $this = shift;
	my $result = 0;
	
	if ($this->{'_end_time_'} eq 'END')
	{
		$result = $this->last_time();
	}
	else
	{
		$result = $this->{'_end_time_'} < 0 ?
			$this->last_time()+$this->{'_end_time_'} :
			$this->offset()+$this->{'_end_time_'};
	}

	return $result;
}

=pod

=item C<offset()>

returns the offset time for this envelope description

=cut

sub offset($)
{
	my $this = shift;

	return $this->{'_offset_'};
}

=pod

=item C<last_time(...)>

set/gets the total end time for
the voice this envelope belongs.
Since this is not known at compile time,
it may not be defined at query time.

=cut

sub last_time
{
	my ($this, $time) = @_;

	$this->{'_last_time_'} = $time if defined($time);

	die("ScoreEnvelope::last_time() not defined at this time") unless
		defined($this->{'_last_time_'});

	return $this->{'_last_time_'};
}

=pod

=back

=head1 VERSION

 $Id: ScoreEnvelope.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: ScoreEnvelope.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/08/19 17:32:54  nicb
# added last_time method
#
# Revision 0.0  1999/08/16 17:11:20  nicb
# Initial Revision
#
