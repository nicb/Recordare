## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreInstrument.pm - voice interpreter class for Recordare
##
##
## $Id: ScoreInstrument.pm,v 0.5 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;
use Recordare::Globals;

package ScoreInstrument;

use strict;

@ScoreInstrument::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::ScoreInstrument> - instrument class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreInstrument;

 my $lh = new ScoreInstrument($voice_at, ...);

=cut

sub new
{
	my ($class, $at, $end_time) = @_;

	my $pkg = $class->SUPER::new('$Revision: 0.5 $');
	$pkg->{'_end_time_'} = $end_time if defined($end_time);
	
	$pkg->allocate_voice($at);

	return ($class, $pkg);
}

#
# allocation is based on a static array of voices;
# before allocating a new one, each
# voice is checked for definition and end time.
# The first free (undefined) or outdated one
# is used.
#

@main::__voice_pool__ = () unless (scalar(@main::__voice_pool__) > 0);

sub allocate_voice($$)
{
	my ($this, $at) = @_;
	my $voice_allocated = 0;
	my $result = 0;
	my $g = new Globals();
	my $start_insno = $g->require('first_instrument');

	for (my $i = $start_insno; $i < $this->num_voices(); ++$i)
	{
		if (defined($main::__voice_pool__[$i]->end_time()) &&
			$at > $main::__voice_pool__[$i]->end_time())
		{
			$result = $this->set_voice($i);
			$voice_allocated = 1;
			last;
		}
	}

	if (!$voice_allocated)
	{
		my $new_idx = $this->num_voices() ? $this->num_voices() : $start_insno;
		$result = $this->set_voice($new_idx);
		$voice_allocated = 1;
	}
}

sub set_voice($$)
{
	my ($this, $idx) = @_;
	$idx = $idx ? $idx : 1;	# coerce into an integer
							# and start from instrument 1

	@main::__voice_pool__[$idx] = $this;
	$this->{'_ins_no_'} = $idx;
}

=pod

=head1 DESCRIPTION

C<Recordare::ScoreInstrument> is the class for I<Recordare>
score instruments. It maintains a static pool of instrument
numbers which grows dynamically as need be.

=head1 METHODS

=over 4

=item C<end_time(...)>

sets/gets the time until which the voice is in
use and cannot be reused. This may well be
undefined at any given time, so it must
be always checked against definition.

=cut

sub end_time
{
	my ($this, $time) = @_;
	$this->{'_end_time_'} = defined($time) ? $time : $this->{'_end_time_'};

	return $this->{'_end_time_'};
}

=pod

=item C<instrument()>

returns the instrument number

=cut

sub instrument($)
{
	my $this = shift;

	return $this->{'_ins_no_'};
}

=pod

=item C<envelope_instrument()>

returns the envelope instrument number
(which is connected to the instrument number:
it is simply the instrument number plus
a constant - 10, in our case)

=cut

sub envelope_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->{'_ins_no_'}+$g->require('envelope_n_offset');
}

=pod

=item C<position_instrument()>

returns the position instrument number
(which is connected to the instrument number:
it is simply the instrument number plus
a constant - 20, in our case)

=cut

sub position_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->{'_ins_no_'}+$g->require('position_n_offset');
}

=pod

=item C<num_voices()>

returns the current number of
allocated voices

=cut

sub num_voices($)
{
	my $this = shift;

	return scalar(@main::__voice_pool__);
}

=pod

=item C<ScoreInstrument::clear_voice_pool()>

this is a static function (not a method)
which allows to clear the voice pool completely
(useful, for example, at section boundaries)

=cut

sub clear_voice_pool()
{
	#
	# not sure this releases the memory, but it should...
	#
	@main::__voice_pool__ = ();
}

=pod

=back

=head1 VERSION

 $Id: ScoreInstrument.pm,v 0.5 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: ScoreInstrument.pm,v $
# Revision 0.5  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.4  1999/09/04 15:35:55  nicb
# added section handling
#
# Revision 0.3  1999/09/02 17:53:12  nicb
# added position instrument numbering
#
# Revision 0.2  1999/08/22 15:56:18  nicb
# added envelope numbering offset
#
# Revision 0.1  1999/08/19 17:35:37  nicb
# corrected bug in end_time() method
#
# Revision 0.0  1999/08/18 08:17:39  nicb
# Initial Revision
#
