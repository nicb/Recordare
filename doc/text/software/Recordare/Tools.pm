## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Tools.pm - generic tools for Recordare Objects
##
##
## $Id: Tools.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use Exporter;

use strict;

package Tools;

@Tools::ISA   = qw(Exporter);
@Tools::EXPORT = qw ( random round );

=pod

=head1 NAME

C<Recordare::Tools> - generic tools for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Tools;
 my $num = random($lo, $hi);
 my $int = round($float);

=head1 DESCRIPTION

C<Recordare::Tools> is a library of tools
to be used in I<Recordare> objects

=head1 FUNCTIONS

=over 4

=item C<round($num)>

rounds a float into an integer

=cut

sub round($)
{
	my $num = shift;

	return int(($num < 0) ? $num-0.5 : $num+0.5);
}

=pod

=item C<random($min, $max)>

returns a random number in the
B<$min>-B<$max> number

=cut

sub random($$)
{
	my ($min, $max) = @_;
	my $range = $max-$min;
	my $value = rand($range) + $min;

	return $value;
}

=back

=head1 VERSION

 $Id: Tools.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Tools.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  2000/05/14 18:10:58  nicb
# reduced the package root naming
# added the strict tag
#
# Revision 0.0  1999/09/15 11:53:55  nicb
# Initial Revision
#
