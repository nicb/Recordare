## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Score2VoiceForm.pm - score to csound converter class for Recordare
##
##
## Score2VoiceForm.pm,v 0.3 2000/06/26 15:49:27 nicb Exp
##

use Recordare::ScoreInterpreter;
use	Recordare::Score2Csound;
use	Recordare::Globals;
use Recordare::Tools;

package Score2VoiceForm;

use strict;

@Score2VoiceForm::ISA   = qw( Score2Csound );

=pod

=head1 NAME

C<Recordare::Score2VoiceForm> - score to csound converter class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::Score2VoiceForm;

 my $lh = new Score2VoiceForm($file);
 $lh->print_head();
 $lh->print();
 $lh->print_tail();

=cut

sub new($$)
{
	my ($class, $file) = @_;

	my $pkg = $class->SUPER::new($file);
	$pkg->version('0.3');

	return $pkg;
}
=pod

=head1 DESCRIPTION

C<Recordare::Score2VoiceForm> is
the score to pic voice form
converter class for I<Recordare> scores.
This class prints the formal arrangement
for all voices in I<Recordare>.

=head1 METHODS

=over 4

=item C<print_comment($fh,$line)>

This is one of the mandatory functions to
be implemented by C<Recordare::ScoreInterpreter>

=cut

sub print_comment($$$)
{
	my ($this, $fh, $line) = @_;

	printf $fh ("#\n#%s\n#\n", $line);
}

=pod

=item C<do_print_section($fh, $section)>

this is the mandatory printing section function...

=cut

$main::_sectno_ = 0;

sub do_print_section($$$)
{
	my ($this, $fh, $section) = @_;
	$main::_voiceno_ = 0;
	$this->current_tag(sprintf("VFSection_%02d", ++$main::_sectno_));
	
	printf $fh ("vf_section(%s,%.4f,\"%s\");\n", $this->current_tag(),
		$this->current_section_dur($section->dur()), $section->notes());
}

sub print_reverb($$)
{
	my ($this, $fh) = @_;
	#
	# unused here
	#
}

sub print_functions($$)
{
	my ($this, $fh) = @_;
	#
	# unused here
	#
}

sub print_end_of_section($$)
{
	my ($this, $fh) = @_;
	#
	# unused here
	#
}

=pod

=item C<print_voice_end($fh, $voice)>

...and this is the mandatory voice one.

=cut

$main::_voiceno_ = 0;

sub print_voice_end($$$$)
{
	my ($this, $fh, $voice, $last_end) = @_;
	my $voice_number = $main::_voiceno_++;
	my $ar_speakers = $voice->speakers();

	$this->current_voice(sprintf("VFVoice_%02d", $voice_number));

	printf $fh ("vf_voice(%s,%s,%d,%.4f,%.4f,%.4f);\n",
		$this->current_voice(), $this->current_tag(),
		$voice_number,$this->current_section_dur(),
		$voice->at(), $last_end);

	#
	# here all the speakers have to be remapped and visualized
	#
	for (my $i = 0; $i < scalar(@$ar_speakers); ++$i)
	{
		printf $fh ("vf_speaker(%s,%s,%d,%.4f);\n", $this->current_voice(),
			$this->current_tag(), $ar_speakers->[$i]->index(),
			$ar_speakers->[$i]->probability());
	}
}

sub print_mixer($$$$$)
{
	my ($this, $voice, $tag, $last_end) = @_;
	#
	# unused here
	#
}

sub print_envelopes($$$$)
{
	my ($this, $voice, $tag) = @_;
	#
	# unused here
	#
}

sub print_position($$$$)
{
	my ($this, $voice, $tag) = @_;
	#
	# unused here
	#
}

=pod

=item C<do_print_segment($fh,$rat,$rdur,$speaker,$file_offset,$func,$word,$idx)>

...and this is another no-op here.

=cut

sub do_print_segment($$$$$$$$$)
{
	my ($this, $fh, $rat, $rdur, $speaker, $file_offset, $func,
		$word, $idx) = @_;
	#
	# unused here
	#
}

=pod

=item C<current_tag([$string])>

sets/gets the current tag

=cut

sub current_tag
{
	my ($this, $string) = @_;
	$this->{'_current_tag_'} = defined($string) ? $string :
		$this->{'_current_tag_'};

	return $this->{'_current_tag_'};
}

=pod

=item C<current_voice([$string])>

sets/gets the current voice tag

=cut

sub current_voice
{
	my ($this, $string) = @_;
	$this->{'_current_voice_'} = defined($string) ? $string :
		$this->{'_current_voice_'};

	return $this->{'_current_voice_'};
}

=pod

=item C<current_section_dur([$dur])>

sets/gets the current section duration

=cut

sub current_section_dur
{
	my ($this, $dur) = @_;
	$this->{'_current_section_dur_'} = defined($dur) ? $dur :
		$this->{'_current_section_dur_'};

	return $this->{'_current_section_dur_'};
}

=pod

=item C<print_head([$fh])>

prints the header.
C<$fh> is a file handle which
defaults to C<STDOUT> if not present.

=cut

sub print_head
{
	my $this = shift;
	my $fh = shift || \*STDOUT;
	my $total_duration = $this->calculate_total_duration();

	print $fh <<EOF
.\\" Pic format file produced automagically by the
.\\" \$RCSFile\$ \$Revision\$ object
.\\" DO NOT EDIT! (your changes will be wiped away)
.\\"
.sp 1c
.PF "\\'\\'\\'\\s6\$Revision\$ \$Date\$\\s0\\'"
.PS
total_duration=$total_duration
copy "voiceform.pic.include"

vf_legenda("[00]=catbassa",0);
vf_legenda("[01]=carlocarlo",1);
vf_legenda("[02]=cecilia",2);
vf_legenda("[03]=clcalta",3);
vf_legenda("[04]=clcbassa",4);
vf_legenda("[05]=clcsuss",5);
vf_legenda("[06]=nicbalta",6);
vf_legenda("[07]=nicbbassa",7);
vf_legenda("[08]=nicbsuss",8);
vf_legenda("[09]=ninolent",9);
vf_legenda("[10]=ninovel",10);
vf_legenda("[11]=silvia",11);
vf_legenda("[12]=titino1",12);
vf_legenda("[13]=titino2-soft",13);
vf_legenda("[14]=giuliano",14);
vf_legenda("[15]=carlo-padre",15);
EOF
}

sub calculate_total_duration($)
{
	my $this = shift;
	my $nsects = $this->num_lines();
	my $result = 0;

	for (my $i = 0; $i < $nsects; ++$i)
	{
		my $type = $this->linetype($i);
		
		if ($type eq 'section')
		{
			my $section = $this->linecontent($i);
			$result += $section->dur();
		}
	}

	return $result;
}

=pod

=item C<print_tail([$fh])>

prints the trailer.
C<$fh> is a file handle which
defaults to C<STDOUT> if not present.

=cut

sub print_tail
{
	my $this = shift;
	my $fh = shift || \*STDOUT;

	print $fh ".PE\n";
}

=pod

=back

=head1 VERSION

 Score2VoiceForm.pm,v 0.3 2000/06/26 15:49:27 nicb Exp
 

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# Score2VoiceForm.pm,v
# Revision 0.3  2000/06/26 15:49:27  nicb
# re-organized sources for better encapsulation of computations
#
# Revision 0.2  2000/06/21 17:23:34  nicb
# corrected bug in voice calculation
#
# Revision 0.1  2000/06/11 15:47:48  nicb
# added sidebox legenda
#
# Revision 0.0  2000/06/11 08:51:37  nicb
# Initial Revision
#
