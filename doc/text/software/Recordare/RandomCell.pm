## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## RandomCell.pm - random cell class for Recordare Objects
##
##
## $Id: RandomCell.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package RandomCell;

use strict;

@RandomCell::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::RandomCell> - random cell class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::RandomCell;

 my $cookie = 'foo';
 my $probability = 5;
 my $lh = new RandomCell($cookie, $probability);

=cut

sub new($$$)
{
	my ($class, $cookie, $probability) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.2 $));

	$pkg->{'_tag_'} = $cookie;
	$pkg->{'_probability_'} = $probability;
	($pkg->{'_start_range_'}, $pkg->{'_end_range_'}) = -1;

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::RandomCell> is the a random cell
that holds a single random unit
to be used during random draws.
The probability applied to it is summed up to
all the other and then normalized by the
C<Recordare::RandomDraw> object.

=head1 METHODS

=over 4

=item C<tag()>

returns the tag applied to the cell

=cut

sub tag($)
{
	my ($self) = @_;

	return $self->{'_tag_'};
}

=pod

=item C<probability()>

returns the probability applied to the cell.

=cut

sub probability($)
{
	my ($self) = @_;

	return $self->{'_probability_'};
}

=pod

=item C<start(...)>

sets/gets the range start
(used by the C<Recordare::RandomDraw> object)

=cut

sub start
{
	my ($self, $s) = @_;

	$self->{'_start_range_'} = defined($s) ? $s: $self->{'_start_range_'};

	return $self->{'_start_range_'};
}

=pod

=item C<end(...)>

sets/gets the range end
(used by the C<Recordare::RandomDraw> object)

=cut

sub end
{
	my ($self, $e) = @_;

	$self->{'_end_range_'} = defined($e) ? $e: $self->{'_end_range_'};

	return $self->{'_end_range_'};
}

=pod

=back

=head1 SEE ALSO

C<Recordare::RandomDraw>

=head1 VERSION

 $Id: RandomCell.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: RandomCell.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/08/18 06:05:21  nicb
# corrected bug in Base class creation
#
# Revision 0.0  1999/07/30 10:30:37  nicb
# Initial Revision
#
