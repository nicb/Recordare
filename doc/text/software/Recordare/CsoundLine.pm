## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## CsoundLine.pm - csound line class for Recordare Objects
##
##
## $Id: CsoundLine.pm,v 0.5 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::OutputBase;

package CsoundLine;

use strict;

@CsoundLine::ISA   = qw(OutputBase);

=pod

=head1 NAME

C<Recordare::CsoundLine> - csound score line class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::CsoundLine;

 my $lh = new Recordare::CsoundLine($at,$dur,$speaker,$off,$word,
                             $ref_speaker,$ref_offset,$cross_time);

The arguments to the object creation are:

=over 4

=item C<$at>

action time of the fragment

=item C<$dur>

duration of the fragment

=item C<$speaker>

speaker of the fragment

=item C<$off>

actual offset of the fragment

=item C<$word>

word of the fragment

=item C<$ref_speaker>

reference speaker

=item C<$ref_offset>

reference offset

=item C<$cross_time>

crossing time

=back

=cut

sub new($$$$$$)
{
	my ($class, $at, $dur, $speaker, $off, $word, $ref_speaker,
		$ref_offset,$cross_time) = @_;

	my $pkg = $class->SUPER::new($at, $dur, $speaker, $word);
	
	$pkg->version(qw($Revision: 0.5 $));	# base class method

	$pkg->{'_offset_'} = $off;
	$pkg->{'_ref_speaker_'} = $ref_speaker;
	$pkg->{'_ref_offset_'} = $ref_offset;
	$pkg->{'_cross_time_'} = $cross_time;
	$pkg->{'_pre_'} = 0.35;					# pre crossing percentage
	$pkg->{'_post_'} = 0.35;				# post crossing percentage
	$pkg->{'_instr_'} = 1;

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::CsoundLine> is the class for I<Recordare>
csound score lines. It contains the description of
a single csound score line,
along with its printing method.

=head1 METHODS

=over 4

=item C<print(...)>

prints out the csound line contained in
the object, in csound syntax

=cut

sub print
{
	my ($self, $out_fh) = @_;
	my $next_at = $self->action_time() + $self->cross_dur();

	$out_fh = defined($out_fh) ? $out_fh : \*STDOUT;

	printf $out_fh ("i%-2d %09.4f %06.4f %1d %09.4f %1d %09.4f ; %-12s - %-20s\n",
		$self->instrument(), $self->action_time(), $self->dur(),
		$self->speaker()->index(), $self->offset(),
		$self->ref_speaker()->index(), $self->ref_offset(),
		$self->speaker()->name(), $self->word()->word());

	return $next_at;
}

=pod

=item C<cross_dur()>

returns the duration time of this word to get
the action times crossed 

=cut

sub cross_dur($)
{
	my ($self) = @_;

	my $pre = $self->cross_time() * $self->{'_pre_'};
	my $result = $self->{'_dur_'} + $pre;

	return $result;
}

=pod

=item C<offset()>

returns the real offset time of the speaker's word

=cut

sub offset($)
{
	my ($self) = @_;

	my $pre = $self->cross_time() * $self->{'_pre_'};
	my $result = $self->{'_offset_'} - $pre;
	$result = $result < 0 ? 0 : $result;

	return $result;
}

=pod

=item C<instrument()>

returns the instrument

=cut

sub instrument($)
{
	my ($self) = @_;

	return $self->{'_instr_'};
}

=pod

=item C<ref_speaker()>

returns the reference word object

=cut

sub ref_speaker($)
{
	my ($self) = @_;

	return $self->{'_ref_speaker_'};
}

=pod

=item C<ref_offset()>

returns the reference offset

=cut

sub ref_offset($)
{
	my ($self) = @_;

	return $self->{'_ref_offset_'};
}

=pod

=item C<cross_time()>

returns the crossing time

=cut

sub cross_time($)
{
	my ($self) = @_;

	return $self->{'_cross_time_'};
}

=pod

=item C<pre_dovetail([$norm_value])>

set/returns the crossing value as a fraction
of the cross_time

=cut

sub pre_dovetail
{
	my ($self, $norm_value) = @_;
	my $result = $norm_value ? ($self->{'_pre_'} = $norm_value) :
		$self->{'_pre_'};

	return $result;
}

=pod

=item C<post_dovetail([$norm_value])>

set/returns the crossing value as a fraction
of the cross_time

=cut

sub post_dovetail
{
	my ($self, $norm_value) = @_;
	my $result = $norm_value ? ($self->{'_post_'} = $norm_value) :
		$self->{'_post_'};

	return $result;
}

=pod

=back

=head1 VERSION

 $Id: CsoundLine.pm,v 0.5 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: CsoundLine.pm,v $
# Revision 0.5  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.4  1999/07/29 13:39:05  nicb
# fitted to the OutputBase base class
#
# Revision 0.3  1999/07/29 09:26:14  nicb
# added pre-post dovetailing info
#
# Revision 0.2  1999/06/26 17:09:13  nicb
# added reference speaker (to calculate comparisons etc.)
#
# Revision 0.1  1999/06/26 15:33:05  nicb
# adjusted pre-post timing
#
# Revision 0.0  1999/06/25 14:43:16  nicb
# Initial Revision
#
# Revision 0.0  1999/06/24 16:09:16  nicb
# Initial Revision
#
