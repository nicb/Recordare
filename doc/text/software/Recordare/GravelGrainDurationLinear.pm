## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelGrainDurationLinear.pm - gravel grain duration class for Recordare
##
##
## GravelGrainDurationLinear.pm,v 0.1 2000/06/27 18:10:44 nicb Exp
##

use	Recordare::GravelGrainDuration;

package GravelGrainDurationLinear;

use strict;

@GravelGrainDurationLinear::ISA
	= qw(GravelGrainDuration);

=pod

=head1 NAME

C<Recordare::GravelGrainDurationLinear> - gravel grain duration class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::GravelGrainDurationLinear;>

C<my $lh = new GravelGrainDurationLinear($gdur, $at);>

=cut

sub new($$)
{
	my ($class, $graindurline, $at) = @_;

	my $pkg = $class->SUPER::new($graindurline, $at);
	$pkg->version('0.1');

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<GravelGrainDurationLinear> is the class for I<Recordare>
gravel grain duration with linear trend.

=head1 METHODS

=over 4

=item C<factors()>

gets the 2 factors of the function
that regulates the grain duration.

=cut

sub factors($$)
{
	my ($this, $start_gdur) = @_;
	my $end = $this->at()+$this->dur();
	#
	# a = (y1-y0)/(x1-x0)
	#
	my $a = ($this->final_grain_duration() - $start_gdur) / ($end-$this->at());
	#
	# b = 
	#
	my $b = $this->final_grain_duration() - ($a * $end);

	return ($a, $b);
}

=pod

=item C<calculate($a, $b, $time)>

returns the abscissa value for a
given B<$time> time ordinate given
the B<$a> and B<$b> factors along the
following formula:

$y = $aX+$b

where B<$y> is the returned value

=cut

sub calculate($$$$)
{
	my ($this, $a, $b, $time) = @_;

	return ($a*$time)+$b;
}

=pod

=back

=head1 VERSION

 GravelGrainDurationLinear.pm,v 0.1 2000/06/27 18:10:44 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# GravelGrainDurationLinear.pm,v
# Revision 0.1  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.0  2000/05/14 18:21:41  nicb
# Initial Revision
#
