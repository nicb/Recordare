## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelInterpreter.pm - gravel interpreter class for Recordare
##
##
## GravelInterpreter.pm,v 0.1 2000/06/27 18:10:44 nicb Exp
##

use	FileHandle;
use	Recordare::Base;
use	Recordare::GravelVoice;

package GravelInterpreter;

use strict;

@GravelInterpreter::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::GravelInterpreter> - score interpreter class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::GravelInterpreter;>

C<my $lh = new GravelInterpreter($file);>

=cut

sub new($$)
{
	my ($class, $file) = @_;

	my $pkg = $class->SUPER::new('0.1');

	$pkg->{'_lines_'} = [];
	$pkg->{'_last_end_'} = -1;

	$pkg->parse($file);

	return $pkg;
}

=pod

=pod

=head1 DESCRIPTION

C<GravelInterpreter> is the interpreter
class for I<Recordare> scores. It parses score definitions
which have the following format:

- any line beginning with a '#' (hash character) is a comment
 
- any line beginning with a '##' (double hash character) is a comment
  which will be carried into the produced score
 
- other than that, any line is passed to appropriate objects
  for parsing

The C<GravelInterpreter> class functions as
a base class for different output classes, such as
the C<Gravel2Csound> and the <Gravel2Pic>
classes. As such it possesses an 'unimplemented'
C<print()> method which is supposed to be implemented
in lower classes.

=head1 METHODS

=over 4

=item C<print(...)>

This method calls print functions for the following
kind of lines (these may be more in the future):

- voice lines (function print_voice)
- comments    (function print_comment)

If these printing functions are not implemented in
the derived classes, these calls will stop the interpreter

=cut

sub print
{
	my $this = shift;
	my $fh	 = shift || \*STDOUT;
	my $nlines = $this->num_lines();

	for (my $i = 0; $i < $nlines; ++$i)
	{
		my $type = $this->linetype($i);
		my $line = $this->linecontent($i);

		if ($type eq 'comment')
		{
			$this->print_comment($fh, $line);
		}
		elsif ($type eq 'voice')
		{
			$this->print_voice($fh, $line);
		}
	}
}

sub print_comment($$$)
{
	my ($this, $fh, $line) = @_;
	my $class = ref($this);

	die("${class}::print_comment() unimplemented: could not print $line");
}

sub print_voice($$$)
{
	my ($this, $fh, $voice) = @_;
	my $class = ref($this);
	my $voiceclass = ref($voice);

	die("${class}::print_voice() unimplemented: could not print $voiceclass");
}

=pod

=item C<linetype($n)>

returns the type of line at number C<$n>

=cut

sub linetype($$)
{
	my ($this, $idx) = @_;

	return $this->{'_lines_'}->[$idx]->{'_type_'};
}

=pod

=item C<linecontent($n)>

returns the content of line at number C<$n>

=cut

sub linecontent($$)
{
	my ($this, $idx) = @_;

	return $this->{'_lines_'}->[$idx]->{'_core_'};
}

=pod

=item C<last_end()>

returns the last end time for this score

=cut

sub last_end($)
{
	my $this = shift;

	return $this->{'_last_end_'};
}

=pod

=back

=head1 PROTECTED METHODS

These are methods to be used by subclasses
to access the parsed lines.

=over 4

=item C<num_lines()>

returns the number of lines parsed.

=cut

sub num_lines($)
{
	my $this = shift;

	return scalar(@{$this->{'_lines_'}});
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($voice)>

=cut

sub parse($$)
{
	my ($this, $file) = @_;

	my $fh = new FileHandle;
	
	if ($fh->open("$file", "r"))
	{
		my $line = "";
		while($line = <$fh>)
		{
			chop $line;
			if ($line =~ /^##/)
			{
				my $comment = $line;
				$comment =~ s/^##//;
				$this->add_comment($comment);
			}
			elsif ($line =~ /^voice/)
			{
				my $voice = new GravelVoice($line);
				$this->add_voice($voice);
			}
		}
	}
	else
	{
		die("could not open $file");
	}

}

sub _add_anything($$$)
{
	my ($this, $type, $voice) = @_;
	my $nentries = $this->num_lines();

	$this->{'_lines_'}->[$nentries] =
	{
		'_type_'	=>	$type,
		'_core_'	=>	$voice,
	};
}

sub add_voice($$)
{
	my ($this, $voice) = @_;
	
	$this->_add_anything('voice', $voice);
	$this->set_end($voice);
}

sub add_comment($$)
{
	my ($this, $comment) = @_;
	
	$this->_add_anything('comment', $comment);
}

sub set_end($$)
{
	my ($this, $voice) = @_;

	$this->{'_last_end_'} = $voice->end()
		if ($voice->end() > $this->{'_last_end_'});
}

=pod

=back

=head1 VERSION

 GravelInterpreter.pm,v 0.1 2000/06/27 18:10:44 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# GravelInterpreter.pm,v
# Revision 0.1  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.0  1999/09/15 11:53:55  nicb
# Initial Revision
#
