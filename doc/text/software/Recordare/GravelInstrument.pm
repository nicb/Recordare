## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelInstrument.pm - gravel instrument class for Recordare
##
##
## GravelInstrument.pm,v 0.1 2000/06/27 18:10:44 nicb Exp
##

use	Recordare::Base;
use Recordare::Globals;

package GravelInstrument;

use strict;

@GravelInstrument::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::GravelInstrument> - gravel instrument class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::GravelInstrument;>

C<my $lh = new GravelInstrument($voice_at, ...);>

=cut

sub new
{
	my ($class, $at, $end_time) = @_;

	my $pkg = $class->SUPER::new('0.1');
	$pkg->{'_end_time_'} = $end_time if defined($end_time);
	
	$pkg->allocate_voice($at);

	return ($class, $pkg);
}

#
# allocation is based on a static array of voices;
# before allocating a new one, each
# voice is checked for definition and end time.
# The first free (undefined) or outdated one
# is used.
#

@main::__ginst_pool__ = () unless (scalar(@main::__ginst_pool__) > 0);

sub allocate_voice($$)
{
	my ($this, $at) = @_;
	my $voice_allocated = 0;
	my $result = 0;
	my $g = new Globals();
	my $start_insno = $g->require('grain_amp_instrument');

	for (my $i = $start_insno; $i < $this->num_voices(); ++$i)
	{
		if (defined($main::__ginst_pool__[$i]->end_time()) &&
			$at > $main::__ginst_pool__[$i]->end_time())
		{
			$result = $this->set_voice($i);
			$voice_allocated = 1;
			last;
		}
	}

	if (!$voice_allocated)
	{
		my $new_idx = $this->num_voices() ? $this->num_voices() : $start_insno;
		$result = $this->set_voice($new_idx);
		$voice_allocated = 1;
	}
}

sub set_voice($$)
{
	my ($this, $idx) = @_;
	$idx = $idx ? $idx : 1;	# coerce into an integer
							# and start from instrument 1

	@main::__ginst_pool__[$idx] = $this;
	$this->{'_ins_no_'} = $idx;
}

=pod

=head1 DESCRIPTION

C<GravelInstrument> is the class for I<Recordare>
gravel instruments. It maintains a static pool of instrument
numbers which grows dynamically as need be.

=head1 METHODS

=over 4

=item C<end_time(...)>

sets/gets the time until which the voice is in
use and cannot be reused. This may well be
undefined at any given time, so it must
be always checked against definition.

=cut

sub end_time
{
	my ($this, $time) = @_;
	$this->{'_end_time_'} = $time if defined($time);

	return $this->{'_end_time_'};
}

=pod

=item C<instrument()>

returns the instrument number

=cut

sub instrument($)
{
	my $this = shift;

	return $this->{'_ins_no_'};
}

=pod

=item C<num_voices()>

returns the current number of
allocated voices

=cut

sub num_voices($)
{
	my $this = shift;

	return scalar(@main::__ginst_pool__);
}

=pod

=item C<GravelInstrument::clear_voice_pool()>

this is a static function (not a method)
which allows to clear the voice pool completely
(useful, for example, at section boundaries)

=cut

sub clear_voice_pool()
{
	#
	# not sure this releases the memory, but it should...
	#
	@main::__ginst_pool__ = ();
}

=pod

=back

=head1 VERSION

 GravelInstrument.pm,v 0.1 2000/06/27 18:10:44 nicb Exp
 

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# GravelInstrument.pm,v
# Revision 0.1  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.0  1999/09/16 07:43:49  nicb
# Initial Revision
#
