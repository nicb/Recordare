## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreSpeaker.pm - score speaker class for Recordare
##
##
## $Id: ScoreSpeaker.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::RandomCell;
use Recordare::Text;
use Recordare::Globals;

package ScoreSpeaker;

use strict;

@ScoreSpeaker::ISA = qw(Text RandomCell);

=pod

=head1 NAME

C<Recordare::ScoreSpeaker> - score speaker class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreSpeaker;

 my $lh = new Recordare::ScoreSpeaker($speakerline,$first,$last);

where C<$first> is the first word to be treated
and C<$last> is the last one.

=cut

sub new($$$$)
{
	my ($class, $speakerline, $first, $last) = @_;
	my ($speaker, $probability) = split(':', $speakerline);

	my $pkg = $class->Text::new($speaker, $first, $last);
	$pkg = $pkg->RandomCell::new($speaker, $probability);
	$pkg->version('$Revision: 0.2 $');
	$pkg->set_speaker_index();

	return $pkg;
}

sub set_speaker_index($)
{
	my $this = shift;
	my $globals = new Globals();
	my $result = $globals->global($this->name());

	die("Undefined speaker index for \"" . $this->name() . "\"")
		unless defined($result);

	$this->index($result);
}

=pod

=head1 DESCRIPTION

C<Recordare::ScoreSpeaker> is the class for I<Recordare>
score speakers, essentially a speaker name with a given
probability. It is passed a line in the following format:

<speaker>:<probability>

which gets parsed and saved in a data structure.
C<Recordare::ScoreSpeaker> is derived from the
C<Recordare::RandomCell> object, so an array of
such objects can be passed to the C<Recordare::RandomDraw>
object for random drawing.

=head1 METHODS

=over 4

=item C<name()>

gets the speaker name.

=cut

sub name($$)
{
	my $this = shift;

	return $this->tag();
}

=pod

=back

=head1 VERSION

 $Id: ScoreSpeaker.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: ScoreSpeaker.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/08/18 06:17:01  nicb
# added speaker index handling through globals
#
# Revision 0.0  1999/08/16 17:11:20  nicb
# Initial Revision
#
