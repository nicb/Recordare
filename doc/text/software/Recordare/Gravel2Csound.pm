## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Gravel2Csound.pm - gravel interpreter class for Recordare
##
##
## $Id: Gravel2Csound.pm,v 0.4 2000/07/02 16:55:46 nicb Exp nicb $
##

use	FileHandle;
use	Recordare::GravelInterpreter;
use	Recordare::GravelVoice;
use Recordare::Tools;

package Gravel2Csound;

use strict;

@Gravel2Csound::ISA   = qw( GravelInterpreter );

=pod

=head1 NAME

C<Recordare::Gravel2Csound> - score to csound converter class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::Gravel2Csound;>

C<my $lh = new Gravel2Csound($file);>

=cut

sub new($$)
{
	my ($class, $file) = @_;

	my $pkg = $class->SUPER::new($file);
	$pkg->version('$Revision: 0.4 $');
	$pkg->{'_printed_voice_'} = 0;

	return $pkg;
}

=pod

=pod

=head1 DESCRIPTION

C<Gravel2Csound> is the csound
converter which is derived from the
C<GravelInterpreter> class
which is needed to print the csound
output of the I<Recordare> gravel scores.

=head1 METHODS

=over 4

=item C<print_comment($fh,$line)>

This is one of the mandatory functions to
implement in derived classes

=cut

sub print_comment($$$)
{
	my ($this, $fh, $line) = @_;

	printf $fh (";\n;%s\n;\n", $line);
}

=pod

=item C<print_voice($fh, $voice)>

...and this is another one.

=cut


sub print_voice($$$)
{
	my ($this, $fh, $voice) = @_;

	$this->current_voice($voice);
	$this->print_comment($fh, "Printing voice n. " . $this->current_voice());

	$this->print_room_line($fh, $voice);
	$this->print_voicer_line($fh, $voice);
	$this->print_amplitude_line($fh, $voice);
	$this->print_display_line($fh, $voice) if ($voice->display());
	$this->print_grain_duration_lines($fh, $voice);

	$this->print_comment($fh, sprintf("%2s %8s %6s %5s %4s %2s %6s %6s %4s %4s %4s %4s %2s",
		'in', '   at', '  dur', '  dB', 'pch', 'fn', 'gap_hi', 'gap_lo',
		' p1', ' p2', ' p3', ' p4', 'st'));

	my $final_time = $voice->end();
	my $cur_time = $voice->at();

	while($cur_time < $final_time)
	{
		$cur_time = $this->print_current_voice($fh, $voice, $cur_time);
	}
}

sub print_current_voice($$$$)
{
	my ($this, $fh, $voice, $cur_time) = @_;
	my $g = new Globals();
	my $rate_leeway = $voice->rate() * $g->require('gravel_rate_random');
	my $rate = $voice->rate() + Tools::random(-$rate_leeway,$rate_leeway);
	my $dur_leeway = $g->require('gravel_dur') * $g->require('gravel_dur_random');
	my $dur = $g->require('gravel_dur') + Tools::random(-$dur_leeway,$dur_leeway);
	my $amp = $g->require('gravel_voice_amplitude');

	printf $fh ("i%-3d %8.4f %6.4f %5.2f %4s %2d %6.4f %6.4f %4.2f %4.2f %4.2f %4.2f %1d\n",
											$this->granule_instrument(),
											$cur_time,
											$dur,
											$amp,
											$voice->note(),
											$voice->singer(),
											$voice->gap_hi(),
											$voice->gap_lo(),
											$voice->pitch_shift($cur_time),
											$voice->pitch_shift($cur_time),
											$voice->pitch_shift($cur_time),
											$voice->pitch_shift($cur_time),
											$voice->skip_time($cur_time));

	return $cur_time + $rate;
}

sub print_grain_duration_lines($$$)
{
	my ($this, $fh, $voice) = @_;
	my $gdurlines = $voice->grain_durations();

	for (my $i = 0; $i < scalar(@$gdurlines); ++$i)
	{
		$this->print_grain_duration_line($fh, $gdurlines->[$i], $voice);
	}
}

sub print_grain_duration_line($$$$)
{
	my ($this, $fh, $gline, $voice) = @_;
	my $trend = $voice->gdur_trend() eq 'e' ? 1 : 0;

	printf $fh ("i%-3d %8.4f %8.4f %8.4f %1d\n",
								$this->grain_duration_instrument(),
								$gline->at(),
								$gline->dur(),
								$gline->final_grain_duration(),
								$trend);
}

sub print_room_line($$$)
{
	my ($this, $fh, $voice) = @_;
	my $g = new Globals();
	
	printf $fh ("i%-3d %8.4f %8.4f 0 %5.2f %5.2f %5.2f %5.2f\n\n",
										$this->room_instrument(),
										$voice->at(),
										$voice->dur()+$g->require('added_dur'),
										$g->require('gravel_reverb_attenuation'),
										$g->require('gravel_room_attenuation'),
										$voice->x(),
										$voice->y());
}

sub print_voicer_line($$$)
{
	my ($this, $fh, $voice) = @_;
	my $g = new Globals();
	my $vowel = Tools::round(rand()*4); # a=0,e=1,i=2,o=3,u=4
	
	printf $fh ("i%-3d %8.4f %8.4f %d ; a=0, e=1, i=2, o=3, u=4\n",
										$this->voicer_instrument(),
										$voice->at(),
										$voice->dur()+$g->require('added_dur'),
										$vowel);
}

sub print_display_line($$$)
{
	my ($this, $fh, $voice) = @_;
	my $g = new Globals();
	
	printf $fh ("i%-3d %8.4f %8.4f %d\t\t\t;display instrument\n",
										$this->display_instrument(),
										$voice->at(),
										$voice->dur()+$g->require('added_dur'),
										$this->voice_number());
}

sub print_amplitude_line($$$)
{
	my ($this, $fh, $voice) = @_;
	my $g = new Globals();
	
	printf $fh ("i%-3d %8.4f 0.001 0\t\t\t; amplitude instrument\n",
										$this->grain_amplitude_instrument(),
										$voice->at());
}

=pod

=item C<print_header()>

prints all the score header information needed

=cut

sub print_header($)
{
	my $this = shift;
	my $g = new Globals();
	my $reverb_dur = $this->last_end() + $g->require('added_dur');

	print <<EOF
; vi:set nowrap:
;
; r-gravel score
;
; the following instruments are defined:
;
;  1-20  - amplitude instruments:
;	p4 = end amplitude attenuation (dB, 0=unity gain)
;
; 41-60  - grain gap instruments:
;	p4 = grain duration end
;
; 81-100 - granule instruments:
;	p4 = absolute amplitude (dB)
;	p5 = sound ftable number (10 = bf4 female, 11 = e4 male)
;	p6 = pitch ratio (1 = original pitch)
;	p7 = gap duration hi
;	p8 = gap duration lo
;	p9 = pitch shift control pitch 1
;       p10= pitch shift control pitch 2
;       p11= pitch shift control pitch 3
;       p12= pitch shift control pitch 4
;            (use ratios 0.75-7 for gravel, 0.975-1.035 ratios for singing)
;	p13= skip time (0 for gravel, 1 for voices)
;
;101-120 - output instruments:
;	p4  = direct attenuation (usually 0)
;	p5  = reverb attenuation (usually -30/-40)
;	p6  = room attenuation (usually -6/-12)
;       p7  = fixed x position
;       p8  = fixed y position
;
;300     - reverb instrument:
;
;
; f1  = room definition function
;
; Room definition data
;              xL   yL   xR  yR  xmax   xmin   ymax    ymin   X    Y
f1   0 16 -2 -1.25  2  1.25  2  7.195 -7.195   9.06   -2.8    0   3.13; room
;
; vocali femminili
;            |------------------------------a-----------------------------|            |-------------------------------e------------------------------|           |-------------------------------i------------------------------|           |-------------------------------o-------------------------------|           |-------------------------------u------------------------------|
f12 0 256 -2 650 1 69 1100 0.38 95 2960 0.23 95 3600 0.26 102 4900 0.11 120 0 0 0  0 0 500 1  75 1750 0.36 104 2550 0.3 123 3650 0.19 140 5400 0.07 165 0 0 0 0 0 330 1 89 2000 0.19 114 2900 0.28 132 3950 0.32 145 5400 0.11 162 0 0 0 0 0 400 1 86 840 0.24 109 2900 0.052 130 3550 0.06 138 4900 0.029 157 0 0 0 0 0 280 1 70 650 0.12 132 2300 0.004 156 3750 .003 224 4900 .002 272
;
; f30 = gravel density function
; f10 = female voice function
; f11 = male voice function
f30  0 8193 5 0.1 8192 1		; reverse exponential function
;f30  0 8193 7  0     8182 1 10  0   	; reverse linear      function
;f08 0 32768   11 20 1 1		; pulse function
f07 0 4097  5 1 4096 0.0001             ; grain envelope function
f08 0 262145 9 32.24 1 0 56.63 1 0 72.38 0.8 0 84.43 0.2 0 

i300	0 	$reverb_dur
EOF
}

=pod

=item C<current_voice(...)>

sets/gets the current voice

=cut

sub current_voice
{
	my ($this, $voice) = @_;
	$this->{'_printed_voice_'} = $voice->voice_number() if defined($voice);

	return $this->{'_printed_voice_'};
}

=pod

=item C<voice_number()>

returns the voice number

=cut

sub voice_number($)
{
	my $this = shift;

	return $this->current_voice()-1;
}

=pod

=item C<grain_duration_instrument()>

returns the grain duration instrument

=cut

sub grain_duration_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->voice_number()+$g->require('grain_dur_instrument');
}

=pod

=item C<grain_amplitude_instrument()>

returns the grain amplitude instrument

=cut

sub grain_amplitude_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->voice_number()+$g->require('grain_amp_instrument');
}

=pod

=item C<granule_instrument()>

returns the granule instrument

=cut

sub granule_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->voice_number()+$g->require('granule_instrument');
}

=pod

=item C<room_instrument()>

returns the room instrument

=cut

sub room_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->voice_number()+$g->require('gravel_room_instrument');
}

=pod

=item C<voicer_instrument()>

returns the voicer instrument

=cut

sub voicer_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $this->voice_number()+$g->require('gravel_voicer_instrument');
}

=pod

=item C<display_instrument()>

returns the display instrument

=cut

sub display_instrument($)
{
	my $this = shift;
	my $g = new Globals();

	return $g->require('gravel_display_instrument');
}

=pod

=back

=head1 VERSION

 $Id: Gravel2Csound.pm,v 0.4 2000/07/02 16:55:46 nicb Exp nicb $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Gravel2Csound.pm,v $
# Revision 0.4  2000/07/02 16:55:46  nicb
# added voicing lines
#
# Revision 0.3  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.2  2000/05/14 18:16:48  nicb
# added fof tables
#
# Revision 0.1  1999/09/16 07:44:41  nicb
# removed a number of bugs - functional version
#
# Revision 0.0  1999/09/15 11:53:55  nicb
# Initial Revision
#
