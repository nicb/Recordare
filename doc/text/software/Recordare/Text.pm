## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Text.pm - text class for Recordare Objects
##
##
## $Id: Text.pm,v 0.6 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;
use Recordare::Word;
use Recordare::Paths;
use FileHandle;

package Text;

use strict;

@Text::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Text> - text class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Text;

 my $lh = new Recordare::Text($speaker, ...);

where C<$speaker> is the speaker of the text,
and other arguments can be: C<$first> and
C<$last>, where C<$first> is the index of the
first word in the range, and C<$last> is
the last word of the range. If these are
not defined, the first and last word of the
text are picked up.

=cut

sub new
{
	my ($class, $speaker, $first, $last) = @_;
	$first = defined($first) ? $first : 0;
	$last  = defined($last ) ? $last  : '_LAST_';

	my $pkg = $class->SUPER::new(qw($Revision: 0.6 $));

	$pkg->{'_name_'} = $speaker;
	$pkg->{'_words_'} = {}; 		# an empty hash of word objects
	$pkg->{'_index_'} = 0;			# numeric index for this speaker
	$pkg->read($speaker, $first, $last);

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::Text> is the class for I<Recordare>
text objects. It contains the full text of the I<Recordare>
taken from the data collection, all split up in
single C<Recordare::Word> objects to be used later on.

=head1 METHODS

=over 4

=item C<name()>

returns the speaker name

=cut

sub name($)
{
	my ($self) = @_;

	return $self->{'_name_'};
}

=pod

=item C<text()>

returns the array of word objects

=cut

sub text($)
{
	my ($self) = @_;

	return $self->{'_words_'};
}


=pod

=item C<size()>

returns the size of the array of
word objects

=cut

sub size($)
{
	my ($self) = @_;

	return scalar(keys(%{$self->text()}));
}

=pod

=item C<action_time($index)>

returns the action time of the word
at the given C<$index>

=cut

sub action_time($$)
{
	my ($self,$idx) = @_;

	return $self->text()->{$idx}->action_time();
}

=pod

=item C<dur($index)>

returns the duration time of the word
at the given C<$index>

=cut

sub dur($$)
{
	my ($self, $idx) = @_;

	return $self->text()->{$idx}->dur();
}

=pod


=item C<word($index)>

returns the actual word object
at the given C<$index>

=cut

sub word($$)
{
	my ($self, $index) = @_;
	my $i = $index + 0;					# force it to be a number

	return $self->text()->{$i};
}


=pod


=item C<index([$index])>

sets and/or returns the numeric index
for the current speaker of this text

=cut

sub index
{
	my ($self, $index) = @_;
	my $result = defined($index) ? $self->{'_index_'} = $index :
		$self->{'_index_'};

	return $result;
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<read($speaker, $first, $last)>

This is an internal method of the object to
read an entire text out of a data file into
a hash of C<Recordare::Word> objects ordered
by index.

=cut

sub read($$$$)
{
	my ($self,$speaker,$first,$last) = @_;

	my $paths = new Paths();
	my $path = $paths->data_path();
	my $file = "$path/${speaker}.dat";
	my $fh = new FileHandle;
	
	if ($fh->open("$file", "r"))
	{
		my $line = "";
		while($line = <$fh>)
		{
			if ($line !~ /^#/)
			{
				my $word = new Word($line);
				my $i = $word->idx() + 0;				# force into integer
				$self->insert($word) unless (($i < $first) ||
					($last ne '_LAST_' && $i > $last));
				last if ($last ne '_LAST_' && $i > $last);
			}
		}
	}
	else
	{
		die("could not open $file");
	}
}


=pod

=item C<insert($word)>

This is an internal method of the object to
position a word into the text array.

=cut

sub insert($$)
{
	my ($self,$word) = @_;
	my $i = $word->idx() + 0;					# force into integer

	$self->{'_words_'}->{$i} = $word;
}

=pod

=back

=head1 VERSION

 $Id: Text.pm,v 0.6 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Text.pm,v $
# Revision 0.6  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.5  1999/08/18 06:15:52  nicb
# corrected bug in index fetching for each word
#
# Revision 0.4  1999/08/16 17:07:21  nicb
# added word range selection
# transformed array of words in hash of words to accelerate selection
#
# Revision 0.3  1999/08/16 11:10:48  nicb
# corrected minor bug in documentation
#
# Revision 0.2  1999/07/29 13:33:47  nicb
# added paths with the Paths object
#
# Revision 0.1  1999/06/25 08:44:59  nicb
# added index() method for indexing speakers numerically
#
# Revision 0.0  1999/06/24 16:09:16  nicb
# Initial Revision
#
