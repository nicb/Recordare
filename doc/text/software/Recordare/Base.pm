## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Base.pm - base class for Recordare Objects
##
##
## $Id: Base.pm,v 0.6 2000/05/20 08:10:52 nicb Exp $
##

package Base;

use strict;

@Base::ISA   = qw(Exporter);

=pod

=head1 NAME

C<Recordare::Base> - base class for all I<Recordare> objects

=head1 SYNOPSIS

 my $lh = new Base($version);

=cut

sub new($$)
{
	my ($class, $version) = @_;
	my $result = '';

	if (ref($class))					# multiple inheritance trick
	{
		my $object = ref($class);
		$class->{'_version_'}->{$object} = $version;
		$result = bless($class, $object);
	}
	else
	{
		$result = bless ({ '_version_' => {}, }, $class);
		$result->{'_version_'}->{$class} = $version;
	}

	return $result;
}

=pod

=head1 DESCRIPTION

C<Recordare::Base> is the base class for all I<Recordare>
objects. It should never be called directely,
but rather from derived class objects.

=head1 METHODS

=over 4

=item C<version(...)>

sets/gets the version of the object.

=cut

sub version
{
	my ($self, $version) = @_;
	my $class = ref($self);
	$self->{'_version_'}->{$class} = defined($version) ? $version :
		$self->{'_version_'}->{$class};

	return $self->{'_version_'}->{$class};
}

=pod

=item C<all_versions($prefix)>

returns a string containing the
versions for all classes used,
one per line, in the following format:

C<$prefixE<lt>tabE<gt>E<lt>class nameE<gt>E<lt>tabE<gt>E<lt>versionE<gt>>

where C<$prefix> is an optional prefix
used to make the string work as a
comment.

=cut

sub all_versions($$)
{
	my ($this, $prefix) = @_;
	my $result = '';
	my $k = "";

	foreach $k (keys %{$this->{'_version_'}})
	{
		$result .= sprintf("%s\t%-20s %s\n", $prefix, $k,
			$this->{'_version_'}->{$k});
	}

	return $result;
}

=pod

=back

=head1 VERSION

 $Id: Base.pm,v 0.6 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Base.pm,v $
# Revision 0.6  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.5  1999/08/18 08:20:39  nicb
# added support for multiple object versioning
# (does not work at all, though)
#
# Revision 0.4  1999/08/18 06:07:12  nicb
# added code to implement real multiple inheritance
#
# Revision 0.3  1999/08/16 16:40:50  nicb
# added reference selection to make multiple inheritance
#
# Revision 0.2  1999/08/15 16:40:49  nicb
# version() method is now set/get
#
# Revision 0.1  1999/07/29 13:32:06  nicb
# removed paths from the base class
#
# Revision 0.0  1999/06/24 16:09:16  nicb
# Initial Revision
#
