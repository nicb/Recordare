## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Paths.pm - path class for Recordare Objects
##
##
## $Id: Paths.pm,v 0.3 2000/05/20 08:10:52 nicb Exp $
##

use Recordare::Base;

package Paths;

use strict;

@Paths::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Paths> - path class for all I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Paths;
 my $lh = new Paths();

=cut

sub new($)
{
	my ($class) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.3 $));

	$main::_data_path_ = '../segmentation/real/data'
		unless defined($main::_data_path_);
	$main::_sound_path_ = '/cdrom/mladic'
		unless defined($main::_sound_path_);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::Paths> is the class that handles
all I<Recordare> paths. It is called by all
objects to derive paths.

=head1 METHODS

=over 4

=item C<sound_path(...)>

sets/gets the path where the audio files lie.

=cut

sub sound_path($$)
{
	my ($self, $path) = @_;

	$main::_sound_path_ = defined($path) ? $path : $main::_sound_path_;

	return $main::_sound_path_;
}


=pod

=over 4

=item C<data_path(...)>

sets/gets the path where the data files lie.

=cut

sub data_path($$)
{
	my ($self, $path) = @_;

	$main::_data_path_ = defined($path) ? $path : $main::_data_path_;

	return $main::_data_path_;
}

=pod

=back

=head1 VERSION

 $Id: Paths.pm,v 0.3 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Paths.pm,v $
# Revision 0.3  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.2  1999/08/18 06:14:22  nicb
# corrected bug in double setting of variables
# (probably this class should now be subclassed from Globals
#
# Revision 0.1  1999/07/29 13:31:16  nicb
# changed path setting to make global settings instead
#
# Revision 0.0  1999/07/29 10:42:39  nicb
# Initial Revision
#
#
