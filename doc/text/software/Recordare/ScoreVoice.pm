## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreVoice.pm - voice interpreter class for Recordare
##
##
## ScoreVoice.pm,v 0.11 2000/06/26 22:04:17 nicb Exp
##

use	Recordare::Base;
use Recordare::ScoreSpeaker;
use Recordare::ScoreEnvelope;
use Recordare::ScoreInstrument;
use Recordare::ScorePosition;
use Recordare::RandomUniqueDraw;
use Recordare::Globals;

package ScoreVoice;

use strict;

@ScoreVoice::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::ScoreVoice> - voice score class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreVoice;

 my $lh = new ScoreVoice($voice);

=cut

sub new($$)
{
	my ($class, $voice) = @_;

	my $pkg = $class->SUPER::new('0.11');

	$pkg->parse($voice);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::ScoreVoice> is the class for I<Recordare>
score voices. It parses a score line which has the following
format:

 - other lines are pipe-separated records with the
   following data:
   voice|<at>|<word-range>|<ref. speaker>|<speaker list>|<envelope list>|<pos.>|<sdur>
  
   where:
  
 - voice        = voice type descriptor
 - at           = action time for this voice
                  (in seconds)
 - word range   = the range of words to be produced
                  (in the format nn-nn) (words are a
                  0-offset array)
 - ref. speaker = the reference speaker to use as
                  timing template
 - speaker list = a list of speakers to be used, in the
                  following format:
                  <speaker name>:<probability percentage>,...
 - env. list    = a list of envelope points in the following
                  format:
                  <dB>:<time>,<dB>:<time>,...
                  where <dB> is an attenuation factor in
                  dB (0=unity gain) and time is in seconds
                  and is relative to the beginning
                  of the excerpt; when time is negative it
                  is taken to be from the end of the excerpt;
                  'END' as time tag means the end of the
                  excerpt
 - pos          = is an x,y position for this fragment in
                  the following format:
                  <start x>,<start y>:<end x>,<end y>
                  where the virtual listener is a 0,0,
                  front left is -1,1, front right is 1,1,
                  rear left is -1,-1, and rear right is 1,-1
                  (distances are in meters)
 - sdur         = segment duration (duration of each voice
                  segment)

=head1 METHODS

=over 4

=item C<at()>

returns the action time offset for this voice

=cut

sub at($)
{
	my $this = shift;

	return $this->{'_at_'};
}

=pod

=item C<real_at()>

returns the real action time offset for this voice

=cut

sub real_at($)
{
	my $this = shift;
	my $g = new Globals();
	my $result = $this->{'_at_'}-$g->require('dovetail');
	$result = $result < 0 ? 0 : $result;

	return $result;
}

=pod

=item C<first_word()>

returns the index to the first word to be uttered

=cut

sub first_word($)
{
	my $this = shift;

	return $this->{'_first_word_'};
}

=pod

=item C<last_word()>

returns the index to the last word to be uttered

=cut

sub last_word($)
{
	my $this = shift;

	return $this->{'_last_word_'};
}

=pod

=item C<reference()>

returns the reference speaker name

=cut

sub reference($)
{
	my $this = shift;

	return $this->{'_reference_speaker_'};
}

=pod

=item C<speaker($idx)>

returns the C<Recordare::ScoreSpeaker> structure
number C<$idx>

=cut

sub speaker($$)
{
	my ($this, $idx) = @_;

	return $this->{'_speaker_drawer_'}->cells()->[$idx];
}


=pod

=item C<speakers()>

returns the array of C<Recordare::ScoreSpeaker> structures

=cut

sub speakers($)
{
	my ($this) = @_;

	return $this->{'_speaker_drawer_'}->cells();
}

=pod

=item C<envelope($idx)>

returns the C<Recordare::ScoreEnvelope> structure
number C<$idx>

=cut

sub envelope($$)
{
	my ($this, $idx) = @_;

	return $this->{'_envelopes_'}->[$idx];
}

=pod

=item C<envelopes()>

returns the array of C<Recordare::ScoreEnvelope>
structures

=cut

sub envelopes($)
{
	my $this = shift;

	return $this->{'_envelopes_'};
}

=pod

=item C<positon()>

returns the position for this voice

=cut

sub position($)
{
	my $this = shift;

	return $this->{'_position_'};
}

=pod

=item C<segment_dur()>

returns the duration for each segment
of the word

=cut

sub segment_dur($)
{
	my $this = shift;

	return $this->{'_seg_dur_'};
}

=pod

=item C<instrument()>

returns the C<Recordare::ScoreInstrument>
object for this voice

=cut

sub instrument($)
{
	my $this = shift;

	return $this->{'_instrument_'};
}

=pod

=item C<draw()>

this simply calls the C<draw()> method
of the C<Recordare::RandomUniqueDraw>
object contained inside. It returns
a selected speaker.

=cut

sub draw($)
{
	my $this = shift;

	return $this->{'_speaker_drawer_'}->draw();
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($voice)>

=cut

sub parse($$)
{
	my ($this, $voice) = @_;

	my ($type, $at, $wordrange, $spk_ref, $speakers, $envelopes, $pos, $sdur) =
		split('\|', $voice);

	$this->{'_at_'} = $at;
	($this->{'_first_word_'}, $this->{'_last_word_'}) = split('-', $wordrange);
	$this->{'_seg_dur_'} = $sdur;

	my @speakers = split(',', $speakers);
	my @scorespeakers = ();
	for (my $i = 0; $i < scalar(@speakers); ++$i)
	{
		$scorespeakers[$i] =
			new ScoreSpeaker($speakers[$i], $this->first_word(),
				$this->last_word());
		$this->{'_reference_speaker_'} = $scorespeakers[$i]
			if ($scorespeakers[$i]->name() eq $spk_ref);
	}

	$this->{'_reference_speaker_'} = 
		new ScoreSpeaker($spk_ref, $this->first_word(),
		$this->last_word()) unless defined($this->{'_reference_speaker_'});
	#
	# ScoreSpeakers are derived from RandomCell objects
	# so a RandomDraw object can be defined with them
	#
	$this->{'_speaker_drawer_'} =
		new RandomUniqueDraw(\@scorespeakers);

	$this->{'_position_'} = new ScorePosition($pos);

	$this->{'_instrument_'} = new ScoreInstrument($this->at());

	$this->add_envelopes($envelopes);
}

sub add_envelopes($$)
{
	my ($this, $envelopes) = @_;
	my @envs = split(',', $envelopes);
	my $ar_envs = $this->complete_envelopes(\@envs);
	my $rat = $this->real_at();

	for (my $i = 0; $i < scalar(@$ar_envs); ++$i)
	{
		my $hr_e = $ar_envs->[$i];
		$this->{'_envelopes_'}->[$i] =
			new ScoreEnvelope($hr_e->{'start_time'},
			$hr_e->{'start_amp'}, $hr_e->{'end_time'},
			$hr_e->{'end_amp'}, $rat);
	}
}

sub complete_envelopes($$)
{
	my ($this, $ar_envs) = @_;
	my %envs = ();
	my @returned_envs = ();
	my $last_amp = 0;

	for (my $i = 0; $i < scalar(@$ar_envs); ++$i)
	{
		my ($amp, $time) = split(':', $ar_envs->[$i]);

		$envs{$time} = $last_amp = $amp;
	}

	$envs{0} = 0 unless defined($envs{0});
	
	$envs{END} = defined($envs{END}) ? $envs{END} : $last_amp;

	my $idx = 0;
	my $k = '';
	my ($last_time, $last_amp) = (0, $envs{0});

	foreach $k (sort sort_times (keys %envs))
	{
		next if ($k eq '0');

		$returned_envs[$idx++] =
		{
			'start_time'	=> $last_time,
			'start_amp'		=> $last_amp,
			'end_time'		=> $k,
			'end_amp'		=> $envs{$k},
		};
		($last_time, $last_amp) = ($k, $envs{$k});
	}

	return \@returned_envs;
}

sub sort_times
{
	#
	# this is an ugly hack, indeed :)
	#
	my $_a_ = $a;
	my $_b_ = $b;

	$_a_ = $_a_ eq 'END' ? 1100000 : $_a_ < 0 ? 1000000-$_a_ : $_a_;
	$_b_ = $_b_ eq 'END' ? 1100000 : $_b_ < 0 ? 1000000-$_b_ : $_b_;

	return $_a_ <=> $_b_;
}

=pod

=back

=head1 VERSION

 ScoreVoice.pm,v 0.11 2000/06/26 22:04:17 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# ScoreVoice.pm,v
# Revision 0.11  2000/06/26 22:04:17  nicb
# substituted real_at() function for inline writing
#
# Revision 0.10  2000/06/26 16:34:07  nicb
# added real_at() function
#
# Revision 0.9  2000/06/11 08:49:32  nicb
# added speakers() method (needed by Score2VoiceForm object)
#
# Revision 0.8  2000/05/20 17:10:21  nicb
# corrected documentation formatting
#
# Revision 0.7  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.6  1999/09/04 17:00:31  nicb
# added position movement
#
# Revision 0.5  1999/09/04 15:37:04  nicb
# added section handling
#
# Revision 0.4  1999/08/22 15:55:16  nicb
# added dovetailing to envelopes too
#
# Revision 0.3  1999/08/19 17:34:58  nicb
# added envelope handling
#
# Revision 0.2  1999/08/18 08:18:48  nicb
# added Score Instrument object
#
# Revision 0.1  1999/08/18 06:18:42  nicb
# added segment duration data
# fitted RandomUniqueDraw object in place of RandomDraw
#
# Revision 0.0  1999/08/16 17:11:20  nicb
# Initial Revision
#
