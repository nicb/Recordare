## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Recordare::OutputBase.pm - base class for Recordare output Objects
##
## Base class for all (csound, pic, etc.) Recordare output objects
##
##
## $Id: OutputBase.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package OutputBase;

use strict;

@OutputBase::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::OutputBase> - base score line class for I<Recordare> output objects

=head1 SYNOPSIS

This class is not supposed to be used directely.
Rather, sub-object for every output language are
supposed to be designed to use the features
of this class and adapt them to the output
of each specific language.

The creation arguments are:

 use Recordare::OutputBase;

 my $lh = new OutputBase($at,$dur,$speaker,$word);

The detailed list of arguments to the object creation is:

=over 4

=item C<$at>

action time of the fragment

=item C<$dur>

duration of the fragment

=item C<$speaker>

speaker object reference of the fragment

=item C<$word>

word object of the fragment

=back

=cut

sub new($$$$$)
{
	my ($class, $at, $dur, $speaker, $word) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.2 $));

	$pkg->{'_at_'} = $at;
	$pkg->{'_dur_'} = $dur;
	$pkg->{'_speaker_'} = $speaker;
	$pkg->{'_word_'} = $word;

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::OutputBase> is the base class for all I<Recordare>
output lines. It contains the description of
a single output event,
along with a default printing method
which is supposed to be overridden
by a specific method in the subclass.

=head1 METHODS

=over 4

=item C<print(...)>

default printing method,
to be overridden by the subclass.
An optional argument is the output filehandle pointer.

=cut

sub print
{
	my ($self, $out_fh) = @_;
	my $next_at = $self->action_time() + $self->dur();

	$out_fh = defined($out_fh) ? $out_fh : \*STDOUT;

	printf $out_fh ("%20s at %8.3f for %8.3f; %-12s\n",
		$self->word()->word(), $self->action_time(), $self->dur(),
		$self->speaker()->name());

	return $next_at;
}

=pod

=item C<action_time()>

returns the real action time of this word

=cut

sub action_time($)
{
	my ($self) = @_;

	return $self->{'_at_'};
}

=pod

=item C<dur()>

returns the real duration time of this word

=cut

sub dur($)
{
	my ($self) = @_;

	return $self->{'_dur_'};
}


=item C<speaker()>

returns a handle to the speaker object

=cut

sub speaker($)
{
	my ($self) = @_;

	return $self->{'_speaker_'};
}

=pod

=item C<word()>

returns the word object

=cut

sub word($)
{
	my ($self) = @_;

	return $self->{'_word_'};
}

=pod

=back

=head1 VERSION

 $Id: OutputBase.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: OutputBase.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/07/29 13:38:12  nicb
# corrected documentation mistake
#
# Revision 0.0  1999/07/29 10:43:55  nicb
# Initial Revision
#
