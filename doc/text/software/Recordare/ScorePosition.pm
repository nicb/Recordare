## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScorePosition.pm - score position class for Recordare
##
##
## $Id: ScorePosition.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package ScorePosition;

use strict;

@ScorePosition::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::ScorePosition> - score position class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScorePosition;

 my $lh = new Recordare::ScorePosition($positionline);

=cut

sub new($$)
{
	my ($class, $positionline) = @_;

	my $pkg = $class->SUPER::new('$Revision: 0.1 $');

	$pkg->{'_dur_'} = 0;
	$pkg->parse($positionline);

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::ScorePosition> is the class for I<Recordare>
position locations, essentially a double couple of x, y
values in meters where 0,0 is the speaker position.
The couple is supposed to be start and end position.

=head1 METHODS

=over 4

=item C<startx()>

gets the start x position value.

=cut

sub startx($)
{
	my $this = shift;

	return $this->{'_start_x_'};
}

=pod

=item C<endx()>

gets the end x position value.

=cut

sub endx($)
{
	my $this = shift;

	return $this->{'_end_x_'};
}

=pod

=item C<starty()>

gets the start y position value.

=cut

sub starty($)
{
	my $this = shift;

	return $this->{'_start_y_'};
}

=pod

=item C<endy()>

gets the end y position value.

=cut

sub endy($)
{
	my $this = shift;

	return $this->{'_end_y_'};
}

=pod

=item C<offset()>

returns the offset time for this space description

=cut

sub offset($)
{
	my $this = shift;

	return $this->{'_offset_'};
}

=pod

=item C<dur(...)>

sets/gets the duration time for this space movement

=cut

sub dur
{
	my ($this, $dur) = @_;
	$this->{'_dur_'} = defined($dur) ? $dur : $this->{'_dur_'};

	return $this->{'_dur_'};
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($line)>

parses the line

=cut

sub parse($$)
{
	my ($this, $line) = @_;
	my ($startpos, $endpos) = split(':', $line);
	
	($this->{'_start_x_'}, $this->{'_start_y_'}) = split(',', $startpos);
	($this->{'_end_x_'}, $this->{'_end_y_'}) = split(',', $endpos);
}

=pod

=back

=head1 VERSION

 $Id: ScorePosition.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: ScorePosition.pm,v $
# Revision 0.1  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.0  1999/09/04 16:54:02  nicb
# Initial Revision
#
