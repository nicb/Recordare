## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Speakers.pm - text class for Recordare Objects
##
##
## $Id: Speakers.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
##

use Recordare::Base;
use Recordare::Text;

package Speakers;

use strict;

@Speakers::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Speakers> - speaker collection class
for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Speakers;

 my $lh = new Speakers(\@speaker_list);

=cut

sub new($$)
{
	my ($class, $speaker_list) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.1 $));

	$pkg->{'_num_speakers_'} = scalar(@$speaker_list);
	$pkg->{'_speakers_'} = []; 		# an empty array of speakers objects
	$pkg->read($speaker_list);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::Speakers> is the class for I<Recordare>
text objects. It contains the full text of the I<Recordare>
taken from the data collection, all split up in
single C<Recordare::Word> objects to be used later on.

=head1 METHODS

=over 4

=item C<num($)>

returns the number of speakers
present in the object

=cut

sub num($)
{
	my ($self) = @_;

	return $self->{'_num_speakers_'};
}

=pod

=item C<speaker($index)>

returns the speaker object at the given C<$index>

=cut

sub speaker($$)
{
	my ($self, $index) = @_;

	return $self->speakers()->[$index];
}

=pod

=item C<size($index)>

returns the size of the array of word objects
at the given C<$index>

=cut

sub size($$)
{
	my ($self, $index) = @_;

	return $self->speakers()->[$index]->size();
}

=pod

=item C<action_time($speaker_index, $word_index)>

returns the action time of the word
at the given C<$word_index>
for the given C<$speaker_index>

=cut

sub action_time($$$)
{
	my ($self, $sidx, $widx) = @_;

	return $self->speakers()->[$sidx]->action_time($widx);
}

=pod

=item C<dur($speaker_index, $word_index)>

returns the duration time of the word
at the given C<$word_index>
for the given C<$speaker_index>

=cut

sub dur($$$)
{
	my ($self, $sidx, $widx) = @_;

	return $self->speakers()->[$sidx]->dur($widx);
}

=pod


=item C<word($speaker_index, $word_index)>

returns the actual word object
at the given C<$word_index>
for the given C<$speaker_index>

=cut

sub word($$$)
{
	my ($self, $sidx, $widx) = @_;

	return $self->speakers()->[$sidx]->word($widx);
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<read($speaker)>

This is an internal method of the object to
read an entire text out of a data file into
an array C<Recordare::Word> objects ordered
by index.

=cut

sub read($$)
{
	my ($self,$ra_speakers) = @_;
	my $i = 0;

	for ($i = 0; $i < $self->num(); ++$i)
	{
		$self->{'_speakers_'}->[$i] = new Text($ra_speakers->[$i]);
		$self->speaker($i)->index($i);		# set the index for this speaker
	}
}

=pod


=item C<speakers()>

This is an internal method of the object
that returns the full array of speakers.

=cut

sub speakers($)
{
	my ($self) = @_;

	return $self->{'_speakers_'};
}

=pod

=back

=head1 VERSION

 $Id: Speakers.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
 

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Speakers.pm,v $
# Revision 0.1  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.0  1999/06/25 14:43:16  nicb
# Initial Revision
#
