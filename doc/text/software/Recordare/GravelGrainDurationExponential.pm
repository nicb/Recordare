## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelGrainDurationExponential.pm - gravel grain duration class for Recordare
##
##
## GravelGrainDurationExponential.pm,v 0.1 2000/06/27 18:10:44 nicb Exp
##

use	Recordare::GravelGrainDuration;

package GravelGrainDurationExponential;

use strict;

@GravelGrainDurationExponential::ISA =
	qw(GravelGrainDuration);

=pod

=head1 NAME

C<GravelGrainDurationExponential> - gravel grain duration class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::GravelGrainDurationExponential;>

C<my $lh = new GravelGrainDurationExponential($gdur, $at);>

=cut

sub new($$)
{
	my ($class, $graindurline, $at) = @_;

	my $pkg = $class->SUPER::new($graindurline, $at);
	$pkg->version('0.1');

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<GravelGrainDurationExponential> is the class for I<Recordare>
gravel grain duration with exponential trend.

=head1 METHODS

=over 4

=item C<factors()>

gets the 2 factors of the function
that regulates the grain duration.

=cut

sub factors($$)
{
	my ($this, $start_gdur) = @_;
	my $end = $this->at()+$this->dur();
	#
	# a = (log(y1)-log(y0))/(x1-x0)
	#
	my $a = (log($this->final_grain_duration())-log($start_gdur))/
		    ($end-$this->at());
	#
	# b = log(y1)-(a*x1)
	#
	my $b = log($this->final_grain_duration())-($a*$end);

	return ($a, $b);
}

=pod

=item C<calculate($a, $b, $time)>

returns the abscissa value for a
given B<$time> time ordinate given
the B<$a> and B<$b> factors along the
following formula:

       ($aX+$b)
$y = e

where B<$y> is the returned value

=cut

sub calculate($$$$)
{
	my ($this, $a, $b, $time) = @_;

	return exp(($a*$time)+$b);
}

=pod

=back

=head1 VERSION

 GravelGrainDurationExponential.pm,v 0.1 2000/06/27 18:10:44 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# GravelGrainDurationExponential.pm,v
# Revision 0.1  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.0  2000/05/14 18:19:32  nicb
# Initial Revision
#
