## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreInterpreter.pm - score interpreter class for Recordare
##
##
## ScoreInterpreter.pm,v 0.2 2000/05/20 08:10:52 nicb Exp
##

use FileHandle;
use	Recordare::Base;
use Recordare::ScoreVoice;
use Recordare::ScoreSection;

package ScoreInterpreter;

use strict;

@ScoreInterpreter::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::ScoreInterpreter> - score interpreter class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreInterpreter;

 my $lh = new ScoreInterpreter($file);

=cut

sub new($$)
{
	my ($class, $file) = @_;

	my $pkg = $class->SUPER::new('0.2');

	$pkg->{'_lines_'} = [];
	$pkg->{'_current_section_'} = -1;
	$pkg->{'_print_funcs_'} =
	{
		'comment'	=>	'print_comment',
		'voice'		=>	'print_voice',
		'section'	=>	'print_section',
	};

	$pkg->parse($file);

	return $pkg;
}

=pod

=pod

=head1 DESCRIPTION

C<Recordare::ScoreInterpreter> is the interpreter
class for I<Recordare> scores. It parses score definitions
which have the following format:

- any line beginning with a '#' (hash character) is a comment
 
- any line beginning with a '##' (double hash character) is a comment
  which will be carried into the produced csound score
 
- other than that, any line is passed to appropriate objects
  for parsing

The C<Recordare::ScoreInterpreter> class functions as
a base class for different output classes, such as
the C<Recordare::Score2Csound> and the <Recordare::Score2Pic>
classes. As such it possesses an 'unimplemented'
C<print()> method which is supposed to be implemented
in lower classes.

=head1 METHODS

=over 4

=item C<print(...)>

This method calls print functions for the following
kind of lines (these may be more in the future):

- voice lines (function print_voice)
- section lines (function print_section)
- comments    (function print_comment)

If these printing functions are not implemented in
the derived classes, these calls will stop the interpreter

=cut

sub print
{
	my $this = shift;
	my $fh	 = shift || \*STDOUT;
	my $nsects = $this->num_lines();

	for (my $i = 0; $i < $nsects; ++$i)
	{
		my $type = $this->linetype($i);
		my $line = $this->linecontent($i);

		if ($type eq 'comment')
		{
			$this->print_comment($fh, $line);
		}
		elsif ($type eq 'section')
		{
			$this->current_section($i);
			$this->print_section($fh, $line);
		}
	}
}

sub print_section($$$)
{
	my ($this, $fh, $sect)	= @_;
	my $nlines	= $sect->num_lines();

	for (my $i=0; $i < $nlines; ++$i)
	{
		$this->print_voice($fh, $sect->line($i));
	}
}

sub print_comment($$$)
{
	my ($this, $fh, $line) = @_;
	my $class = ref($this);

	die("${class}::print_comment() unimplemented: could not print $line");
}

sub print_voice($$$)
{
	my ($this, $fh, $voice) = @_;
	my $class = ref($this);
	my $voiceclass = ref($voice);

	die("${class}::print_voice() unimplemented: could not print $voiceclass");
}

=pod

=item C<linetype($n)>

returns the type of line at number C<$n>

=cut

sub linetype($$)
{
	my ($this, $idx) = @_;

	return $this->{'_lines_'}->[$idx]->{'_type_'};
}

=pod

=item C<linecontent($n)>

returns the content of line at number C<$n>

=cut

sub linecontent($$)
{
	my ($this, $idx) = @_;

	return $this->{'_lines_'}->[$idx]->{'_core_'};
}

=pod

=back

=head1 PROTECTED METHODS

These are methods to be used by subclasses
to access the parsed lines.

=over 4

=item C<num_lines()>

returns the number of lines parsed.

=cut

sub num_lines($)
{
	my $this = shift;

	return scalar(@{$this->{'_lines_'}});
}

=pod

=item C<add_printing_function($tag, $funcref)>

will add a printing function to the hash table
of functions that may do some printing output.
The prototype for such functions should be:

C<func($this, $filehandle, $object)>

where:

- $this: is the interpreter object instance itself
- $filehandle: a filehandle where to do the printing
- $object: the object to print

=cut

sub add_printing_function($$$)
{
	my ($this, $tag, $function) = @_;

	$this->{'_print_funcs_'}->{$tag} = $function;
}

=pod

=item C<current_section(...)>

sets/returns the current section

=cut

sub current_section
{
	my ($this, $snum) = @_;

	if (defined($snum))
	{
		if ($this->linetype($snum) eq 'section')
		{
			$this->{'_current_section_'} = $snum;
		}
		else
		{
			die(sprintf("setting current section number %d to non-section",
				$snum));
		}
	}

	return $this->{'_current_section_'};
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($voice)>

=cut

sub parse($$)
{
	my ($this, $file) = @_;

	my $fh = new FileHandle;
	
	if ($fh->open("$file", "r"))
	{
		my $line = "";
		while($line = <$fh>)
		{
			chop $line;
			if ($line =~ /^##/)
			{
				my $comment = $line;
				$comment =~ s/^##//;
				$this->add_comment($comment);
			}
			elsif ($line =~ /^voice/)
			{
				my $voice = new ScoreVoice($line);
				$this->add_voice($voice);
			}
			elsif ($line =~ /^section/)
			{
				my $section = new ScoreSection($line);
				$this->add_section($section);
				ScoreInstrument::clear_voice_pool();
			}
		}
	}
	else
	{
		die("could not open $file");
	}

}

sub add_voice($$)
{
	my ($this, $hr_entry) = @_;
	my $cs = $this->current_section();

	die("No section defined yet") if ($cs < 0);
	die("line $cs is not a section") if ($this->linetype($cs) ne 'section');

	$this->linecontent($cs)->add_line($hr_entry);
}

sub add_section($$)
{
	my ($this, $section) = @_;
	my $nentries = $this->num_lines();

	$this->{'_lines_'}->[$nentries] =
	{
		'_type_'	=>	'section',
		'_core_'	=>	$section,
	};
	$this->current_section($nentries);
}

sub add_comment($$)
{
	my ($this, $comment) = @_;
	my $nentries = $this->num_lines();

	$this->{'_lines_'}->[$nentries] =
	{
		'_type_'	=>	'comment',
		'_core_'	=>	$comment,
	};
}

=pod

=back

=head1 VERSION

 ScoreInterpreter.pm,v 0.2 2000/05/20 08:10:52 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# ScoreInterpreter.pm,v
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/09/04 15:35:55  nicb
# added section handling
#
# Revision 0.0  1999/08/16 17:11:20  nicb
# Initial Revision
#
