## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelVoice.pm - voice interpreter class for Recordare
##
##
## $Id: GravelVoice.pm,v 0.4 2000/07/02 16:55:46 nicb Exp nicb $
##

use	Recordare::Base;
use Recordare::Globals;
use Recordare::GravelGrainDurationLinear;
use Recordare::GravelGrainDurationExponential;
use Recordare::GravelInstrument;
use Recordare::Tools;

package GravelVoice;

use strict;

@GravelVoice::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::GravelVoice> - gravel voice class for I<Recordare>

=head1 SYNOPSIS

C<use Recordare::GravelVoice;>

C<my $lh = new GravelVoice($voice);>

=cut

sub new($$)
{
	my ($class, $voice) = @_;

	my $pkg = $class->SUPER::new('$Revision: 0.4 $');

	$pkg->{'_grain_durations_'} = [];
	$pkg->parse($voice);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<GravelVoice> is the class for I<Recordare>
gravel voices. It parses a gravel score line which has the following
format:

  voice|at|end|rate|note|xpos|ypos|<grain dur list>|gap hi|gap lo

  where:

  voice = constant tag
  at = action time offset (may be 0, but the system will always add
       a 'gravel_init_offset' seconds offset for system initialization)
  end = action time end (the system will always add a 'gravel_init_offset'
        seconds offset)
  rate = repetition rate (+/- 'gravel_rate' randomness)
  note = note in english notation (bf3 for B flat over middle C, etc.) 
         (central notes are bf3 for female and e3 for male)
  xpos = x position (may go from -34 for 34 meters to the left, to
         34 for 34 meters to the right)
  ypos = y position (may go from 40 for 40 meters in front, to -40
         for 40 meters in the back)
  <grain dur list> is a timed list for grain duration, which may be
  in the following format:
  <item>,<item>,...,<item>
  where <item> is in the following format:
  <at>:<dur>:<grain duration>
  where at = action time + 'gravel_init_offset', dur = duration of
  the transition, grain duration = final grain duration
  gap hi = granule high gap duration
  gap lo = granule low gap duration

=head1 METHODS

=over 4

=item C<at(...)>

sets/gets the action time value

=cut

sub at
{
	my ($this, $at) = @_;

	if (defined($at))
	{
		my $g = new Globals();

		$this->{'_at_'} = $at + $g->require('gravel_init_offset');
	}

	return $this->{'_at_'};
}

=pod

=item C<end(...)>

sets/gets the end for this voice

=cut

sub end
{
	my ($this, $end) = @_;

	if (defined($end))
	{
		my $g = new Globals();

		$this->{'_end_'} = $end + $g->require('gravel_init_offset');
	}

	return $this->{'_end_'};
}

=pod

=item C<rate(...)>

sets/gets the rate for this voice

=cut

sub rate
{
	my ($this, $rate) = @_;
	$this->{'_rate_'} =  $rate if defined($rate);

	return $this->{'_rate_'};
}

=pod

=item C<dur()>

returns the duration for this voice

=cut

sub dur($)
{
	my $this = shift;

	return $this->end()-$this->at();
}

=pod

=item C<note(...)>

sets/gets the note. In setting, it takes a
note in standard english notation, like
c3 for middle c, etc.
In getting, it returns the pitch in
pitch class notation

=cut

sub note
{
	my ($this, $note) = @_;
	
	$this->set_note($note) if (defined($note));

	return $this->{'_note_pch_'};
}

sub set_note($$)
{
	my ($this, $note) = @_;
	$note = lc($note);
	my $result = 0;

	if ($note =~ /([a-g])([#f])*([0-9]+)/)
	{
		my ($note, $acc, $octave) = ( $1, $2, $3 );

		$result = convert_to_pch($note, $acc, $octave);
	}

	$this->{'_note_pch_'} = $result;
}

sub convert_to_pch($$$)
{
	my ($note, $acc, $octave) = @_;
	my %pitches =
	(
		'c'	=>	0,
		'd'	=>	2,
		'e'	=>	4,
		'f'	=>	5,
		'g'	=>	7,
		'a'	=>	9,
		'b'	=>	11,
	);
	my %accs = ( '#' => 	+1,	'f'	=>	-1,	);
	$octave += 5;						# pitch classes are 5-offset
	my $result = sprintf("%d.%02d", $octave, $pitches{$note}+$accs{$acc});

	return $result;
}

=pod

=item C<singer()>

returns the singer function for this voice

=cut

sub singer($)
{
	my $this = shift;
	my $g = new Globals();
	my $ff = $g->require('gravel_female_function');
	my $mf = $g->require('gravel_male_function');
	
	return ($this->note() >= $g->require('gravel_mf_crossover')) ? $ff : $mf;
}

=pod

=item C<gap_hi(...)>

sets/gets the high gap level

=cut

sub gap_hi
{
	my ($this, $gap_hi) = @_;
	$this->{'_gap_hi_'} = defined($gap_hi) ? $gap_hi : $this->{'_gap_hi_'};

	return $this->{'_gap_hi_'};
}

=pod

=item C<gap_lo(...)>

sets/gets the low gap level

=cut

sub gap_lo
{
	my ($this, $gap_lo) = @_;
	$this->{'_gap_lo_'} = defined($gap_lo) ? $gap_lo : $this->{'_gap_lo_'};

	return $this->{'_gap_lo_'};
}

=pod

=item C<x(...)>

sets/gets the x position for this voice

=cut

sub x
{
	my ($this, $x) = @_;
	$this->{'_x_'} = defined($x) ? $x : $this->{'_x_'};

	return $this->{'_x_'};
}

=pod

=item C<y(...)>

sets/gets the y position for this voice

=cut

sub y
{
	my ($this, $y) = @_;
	$this->{'_y_'} = defined($y) ? $y : $this->{'_y_'};

	return $this->{'_y_'};
}

=pod

=item C<display(...)>

sets/gets the display flag

=cut

sub display
{
	my ($this, $flag) = @_;
	$this->{'_display_flag_'} = $flag if defined($flag);

	return $this->{'_display_flag_'};
}

=pod

=item C<gdur_trend(...)>

sets/gets the type of grain duration trend
which will be 'l' for linear and 'e' for
exponential

=cut

sub gdur_trend
{
	my ($this, $flag) = @_;
	$this->{'_gdur_trend_'} = $flag if defined($flag);

	return $this->{'_gdur_trend_'};
}

=pod

=item C<pitch_shift($$)>

returns a random value for pitch shifting
according to whether the voice is singing
or not.

=cut

sub pitch_shift($$)
{
	my ($this, $cur_time) = @_;
	my ($lo, $hi) = ( 0, 0 );
	my $g = new Globals();

	if ($this->is_singing($cur_time))
	{
		$lo = $g->require('gravel_singing_lo');
		$hi = $g->require('gravel_singing_hi');
	}
	else
	{
		$lo = $g->require('gravel_gravel_lo');
		$hi = $g->require('gravel_gravel_hi');
	}

	return Tools::random($lo, $hi);
}

=pod

=item C<skip_time($time)>

returns the value for sample skip time according
to whether the voice is singing or not

=cut

sub skip_time($$)
{
	my ($this, $time) = @_;

	return $this->is_singing($time) ? 1 : 0;
}

=pod

=item C<is_singing($time)>

returns 1 when the voice is actually singing and
0 when it is gravel at the time C<$time>

=cut

sub is_singing($$)
{
	my ($this, $time) = @_;
	my $g = new Globals();
	my $level = $this->calculate_level($time);

	return $level > $g->require('gravel_crossover') ? 1 : 0;
}

sub calculate_level($$)
{
	my ($this, $time) = @_;
	my ($previous_gdur, $dur) = $this->find_dur($time);
	my ($a, $b) = $dur->factors($previous_gdur);

	return $dur->calculate($a, $b, $time);
}

sub find_dur($$)
{
	my ($this, $time) = @_;
	my $gdurlist = $this->grain_durations();
	my $previous = $gdurlist->[0];
	my $previous_end = 0;
	my $i = 0;
	my $last = scalar(@$gdurlist);

	for ($i = 0; $i < $last; ++$i)
	{
		my $this_end = $gdurlist->[$i]->at() + $gdurlist->[$i]->dur();
		last if $time >= $previous_end && $time < $this_end;
		$previous = $gdurlist->[$i];
		$previous_end = $this_end;
	}

	$i = ($i == $last) ? $last-1 : $i;

	return ($previous->final_grain_duration(), $gdurlist->[$i]); 
}

=pod

=item C<grain_durations()>

returns the array of grain durations

=cut

sub grain_durations($)
{
	my $this = shift;

	return $this->{'_grain_durations_'};
}

=pod

=item C<voice_number()>

returns the basic voice number

=cut

sub voice_number($)
{
	my $this = shift;

	return $this->{'_instrument_'}->instrument();
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($voice)>

=cut

sub parse($$)
{
	my ($this, $voice) = @_;

	my ($type, $at, $end, $rate, $note, $xpos, $ypos, $gdurlist, $gap_hi,
		$gap_lo, $display_flag) = split('\|', $voice);

	$this->at($at);
	$this->end($end);
	$this->rate($rate);
	$this->note($note);
	$this->x($xpos);
	$this->y($ypos);
	$this->create_grain_duration_list($gdurlist, $at);
	$this->gap_hi($gap_hi);
	$this->gap_lo($gap_lo);
	$this->display($display_flag);
	$this->{'_instrument_'} = new GravelInstrument($at, $end);
}

sub create_grain_duration_list($$$)
{
	my ($this, $gdurlist, $at) = @_;
	my ($type, $gdl) = split('\%', $gdurlist);
	my @list = split(',', $gdl);

	$this->gdur_trend($type);

	for (my $i = 0; $i < scalar(@list); ++$i)
	{
		my $gdur = undef;

		$gdur = new GravelGrainDurationLinear($list[$i], $at)
			if ($this->gdur_trend() eq 'l');
		$gdur = new GravelGrainDurationExponential($list[$i], $at)
			if ($this->gdur_trend() eq 'e');
		die("unknown grain duration trend type \'$type\'")
			unless defined($gdur);

		$this->{'_grain_durations_'}->[$i] = $gdur;
	}
}

=pod

=back

=head1 VERSION

 $Id: GravelVoice.pm,v 0.4 2000/07/02 16:55:46 nicb Exp nicb $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: GravelVoice.pm,v $
# Revision 0.4  2000/07/02 16:55:46  nicb
# added voicing lines
#
# Revision 0.3  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.2  2000/05/14 18:13:56  nicb
# added multiple calculations
#
# Revision 0.1  1999/09/16 07:44:41  nicb
# removed a number of bugs - functional version
#
# Revision 0.0  1999/09/15 11:53:55  nicb
# Initial Revision
#
