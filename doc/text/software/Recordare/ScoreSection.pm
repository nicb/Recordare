## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## ScoreSection.pm - section interpreter class for Recordare
##
##
## $Id: ScoreSection.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package ScoreSection;

use strict;

@ScoreSection::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::ScoreSection> - section score class for I<Recordare>

=head1 SYNOPSIS

 use Recordare::ScoreSection;

 my $lh = new ScoreSection($section);

=cut

sub new($$)
{
	my ($class, $section) = @_;

	my $pkg = $class->SUPER::new('$Revision: 0.1 $');

	$pkg->{'_lines_'} = [];
	$pkg->parse($section);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::ScoreSection> is the class for I<Recordare>
score sections. It parses a score line which has the following
format:

  C<section|E<lt>durE<gt>|E<lt>annotationsE<gt>
 
  where:
 
- section		= section type constant
- dur 			= duration for this section
				  (in seconds)
- annotations	= annotations

=head1 METHODS

=over 4

=item C<dur()>

returns the duration for this section

=cut

sub dur($)
{
	my $this = shift;

	return $this->{'_dur_'};
}

=pod

=item C<notes()>

returns the annotations for this section

=cut

sub notes($)
{
	my $this = shift;

	return $this->{'_notes_'};
}

=pod

=item C<num_lines()>

returns the number of lines for this section

=cut

sub num_lines($)
{
	my $this = shift;

	return scalar(@{$this->{'_lines_'}});
}

=pod

=item C<line($idx, ...)>

sets/gets the line number B<$idx> of this section

=cut

sub line
{
	my ($this, $idx, $line) = @_;
	
	$this->{'_lines_'}->[$idx] = defined($line) ? $line :
		$this->{'_lines_'}->[$idx];

	return $this->{'_lines_'}->[$idx];
}

=item C<add_line($line)>

sets/gets the line number B<$idx> of this section

=cut

sub add_line($$)
{
	my ($this, $line) = @_;
	my $nlines = $this->num_lines();
	
	return $this->{'_lines_'}->[$nlines] = $line;
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($section)>

=cut

sub parse($$)
{
	my ($this, $section) = @_;

	my ($type, $dur, $notes) = split('\|', $section);

	$this->{'_dur_'} = $dur;
	$this->{'_notes_'} = $notes;
}

=pod

=back

=head1 VERSION

 $Id: ScoreSection.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: ScoreSection.pm,v $
# Revision 0.1  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.0  1999/09/04 15:37:24  nicb
# Initial Revision
#
