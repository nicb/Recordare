## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Globals.pm - global variables class for Recordare Objects
##
##
## $Id: Globals.pm,v 0.3 2000/05/20 08:10:52 nicb Exp $
##

use Recordare::Base;
use FileHandle;

package Globals;

use strict;

@Globals::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Globals> - global variables class for I<Recordare> objects

=head1 SYNOPSIS

There are two possible synopsis:

 use Recordare::Globals;

 my $globals_file = 'global.data';
 my $globals = new Globals($globals_file);

 $globals->global('xxx', 0.5);
 $xxx = $globals->global('xxx');

or

 use Recordare::Globals;

 my $globals = new Globals();

 $globals->global('xxx', 0.5);
 $xxx = $globals->global('xxx');

=cut

sub new($$)
{
	my ($class, $filename) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.3 $));
	%main::__globals__ = () if scalar(keys(%main::__globals__)) == 0;

	$pkg->parse($filename) if defined($filename);

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::Globals> is the class that handles
global variables for I<Recordare> objects.

=head1 METHODS

=over 4

=item C<global(...)>

sets/gets the a global variable:
when it is called with two or three arguments,
as in:

 $g->global('xxx', 0.5, 'xxx settings');

it sets the variable (C<xxx> in this
case). When it is called with one
argument, as in:

 $xxx = $g-E<gt>global('xxx');

it returns the value of the variable (C<'xxx'>,
in this case). The variable may well
be undefined.

=cut

sub global
{
	my ($self, $tag, $value, $notes) = @_;

	if (defined($value))
	{
		$main::__globals__{$tag} = {} unless
			defined($main::__globals__{$tag});
		$main::__globals__{$tag}->{'_value_'} = $value;
		$main::__globals__{$tag}->{'_notes_'} = $notes;
	}

	return $main::__globals__{$tag}->{'_value_'};
}

=pod

=item C<require($tag)>

tries to return the value of the corresponding C<$tag>,
and C<die>s if the value is not set.

=cut

sub require($$)
{
	my ($self, $tag) = @_;

	die("Requested tag \"$tag\" not previously set")
		unless defined($main::__globals__{$tag}->{'_value_'});

	return $self->global($tag);
}

=pod

=item C<notes($tag)>

returns the notes of the corresponding C<$tag>.

=cut

sub notes($$)
{
	my ($self, $tag) = @_;
	my $result = $main::__globals__{$tag}->{'_notes_'}
		if defined($main::__globals__{$tag});

	return $result;
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($filename)>

parses a file with the following format:

- any line beginning by '#' is a comment

- any other line is a pipe separated record in the follwing format:

  <tag>|<value>[|<annotation>]

and sets the global variables according to each tag.

=cut

sub parse($$)
{
	my ($this, $filename) = @_;
	my $fh = new FileHandle();

	if ($fh->open($filename, "r"))
	{
		my $line = "";

		while ($line = <$fh>)
		{
			$this->add_global($line) unless $line =~ /^#/;
		}
	}
	else
	{
		my $ref = ref($this);
		die("${ref}::parse(): cannot open file \"$filename\"");
	}
}

sub add_global($$)
{
	my ($this, $line) = @_;
	my ($tag, $value, $notes) = split('\|', $line);

	$this->global($tag, $value, $notes);
}

=pod

=back

=head1 VERSION

 $Id: Globals.pm,v 0.3 2000/05/20 08:10:52 nicb Exp $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Globals.pm,v $
# Revision 0.3  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.2  1999/09/02 17:51:00  nicb
# added global file reading and handling
#
# Revision 0.1  1999/09/02 13:50:40  nicb
# added require method
#
# Revision 0.0  1999/08/18 06:08:26  nicb
# Initial Revision
#
