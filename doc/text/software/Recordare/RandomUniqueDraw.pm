## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## RandomUniqueDraw.pm - random cell class for Recordare Objects
##                                  with unique drawings
##
##
## $Id: RandomUniqueDraw.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
##

use Recordare::RandomDraw;

package RandomUniqueDraw;

use strict;

@RandomUniqueDraw::ISA   = qw(RandomDraw);

=pod

=head1 NAME

C<Recordare::RandomUniqueDraw> - random unique drawing class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::RandomUniqueDraw;

 my @cells = ( RandomUniqueDraw('1', 0.6),
 				RandomUniqueDraw('2', 0.4) );
 my $lh = new RandomUniqueDraw(\@cells);

=cut

sub new($$)
{
	my ($class, $ar_cells) = @_;

	my $pkg = $class->SUPER::new($ar_cells);
	$pkg->version('$Revision: 0.1 $');

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::RandomUniqueDraw> is a class
derived from the C<Recordare::RandomDraw>.
It does the same things but never C<draw()>s
the same tag twice.

=head1 METHODS

=over 4

=item C<draw()>

returns a unique random tag with the given probability

=cut

sub draw($)
{
	my $this = shift;
	my $result = '';

	while(($result = $this->SUPER::draw()) eq $this->last_drawn())
	{
		# empty on purpose
	}

	$this->last_drawn($result);

	return $result;
}

=pod

=item C<last_drawn(...)>

sets/gets the last drawn tag

=cut

sub last_drawn
{
	my ($this, $drawn) = @_;
	$this->{'_last_drawn_'} = defined($drawn) ? $drawn :
		$this->{'_last_drawn_'};

	return $this->{'_last_drawn_'};
}

=pod

=back

=head1 SEE ALSO

C<Recordare::RandomCell>

=head1 VERSION

 $Id: RandomUniqueDraw.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
 
=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: RandomUniqueDraw.pm,v $
# Revision 0.1  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.0  1999/08/18 06:05:55  nicb
# Initial Revision
#
# Revision 0.0  1999/07/30 10:30:37  nicb
# Initial Revision
#
