## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## RandomDraw.pm - random cell class for Recordare Objects
##
##
## $Id: RandomDraw.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;
use Recordare::RandomCell;

package RandomDraw;

use strict;

@RandomDraw::ISA   = qw(Base);

=pod

=head1 NAME

C<Recordare::RandomDraw> - random drawing class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::RandomDraw;

 my @cells = ( RandomDraw('1', 0.6),
				RandomDraw('2', 0.4) );
 my $lh = new RandomDraw(\@cells);

=cut

sub new($$)
{
	my ($class, $ar_cells) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.2 $));

	$pkg->{'_cells_'} = $ar_cells;

	$pkg->arrange_cells();

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<Recordare::RandomDraw> is the random drawing
class that is able to draw() tags following
the probability directives given in the
cells passed as arguments.

=head1 METHODS

=over 4

=item C<cells()>

returns the reference to the internal array of cells

=cut

sub cells($)
{
	my ($self) = @_;

	return $self->{'_cells_'};
}

=pod

=item C<draw()>

returns a random tag with the given probability

=cut

sub draw($)
{
	my ($self) = @_;
	my $r = rand();
	my $result = '';
	my $i = 0;
	my $drawn = 0;

	for ($i = 0; $i < scalar(@{$self->cells()}); ++$i)
	{
		if ($r >= $self->cells()->[$i]->start() &&
			$r < $self->cells()->[$i]->end())
		{
			$drawn = $i;
			last;
		}
	}

	return $self->cells()->[$drawn];
}

=pod

=back

=head1 PRIVATE_METHODS

=over 4

=item C<arrange_cells()>

this method is called by the constructor to arrange all
cells so that they can be drawn upon by the C<draw()>
method

=cut

sub arrange_cells()
{
	my $self = shift;
	my $i = 0;
	my $factor = $self->normalize();
	my $cur_step = 0;

	for ($i = 0; $i < scalar(@{$self->cells()}); ++$i)
	{
		my $start = $cur_step;
		my $end = $start + ($self->cells()->[$i]->probability()*$factor);

		$self->cells()->[$i]->start($start);
		$self->cells()->[$i]->end($end);

		$cur_step = $end;
	}
}

sub normalize($)
{
	my $self = shift;
	my $result = 0;
	my $i = 0;

	for ($i = 0; $i < scalar(@{$self->cells()}); ++$i)
	{
		$result += $self->cells()->[$i]->probability();
	}

	$result = 1/$result;

	return $result;
}

=pod

=back

=head1 SEE ALSO

C<Recordare::RandomCell>

=head1 VERSION

 $Id: RandomDraw.pm,v 0.2 2000/05/20 08:10:52 nicb Exp $
 
=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: RandomDraw.pm,v $
# Revision 0.2  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.1  1999/08/18 06:05:21  nicb
# corrected bug in Base class creation
#
# Revision 0.0  1999/07/30 10:30:37  nicb
# Initial Revision
#
