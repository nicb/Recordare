## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## Recordare::Index.pm - index class for Recordare Objects
##
##
## $Id: Index.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
##

use	Recordare::Base;

package Index;

use strict;

@Index::ISA   = qw( Base );

=pod

=head1 NAME

C<Recordare::Index> - index class for I<Recordare> objects

=head1 SYNOPSIS

 use Recordare::Index;

 my $idx = new Index($num,$offset);

=cut

sub new($$$)
{
	my ($class, $num, $offset) = @_;

	my $pkg = $class->SUPER::new(qw($Revision: 0.1 $));

	$pkg->{'_num_'} = $num; 	# this is 0-offset
	$pkg->set($offset < $num ? $offset : $pkg->{'_num_'}-1);

	return ($class, $pkg);
}

=pod

=head1 DESCRIPTION

C<Recordare::Index> is the class for I<Recordare>
index objects. It contains the description of a index
and it allows correct next indexing handling.

=head1 METHODS

=over 4

=item C<peek()>

returns the current index without any handling

=cut

sub peek($)
{
	my ($self) = @_;

	return $self->{'_cur_idx_'};
}

=pod

=item C<next()>

bumps the current index and returns it

=cut

sub next($)
{
	my ($self) = @_;
	my $idx = $self->peek();

	++$idx;
	$idx = $idx < $self->num() ? $idx : 0;

	$self->set($idx);

	return $self->peek();
}

=pod

=item C<num()>

returns the total number of items to count

=cut

sub num($)
{
	my ($self) = @_;

	return $self->{'_num_'};
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<set($idx)>

This is an internal method of the object to
set the current index.

=cut

sub set($$)
{
	my ($self,$idx) = @_;

	$self->{_cur_idx_} = $idx;
}

=pod

=back

=head1 VERSION

 $Id: Index.pm,v 0.1 2000/05/20 08:10:52 nicb Exp $
 $Name:  $

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# $Log: Index.pm,v $
# Revision 0.1  2000/05/20 08:10:52  nicb
# no changes made, just adapted to new configuration (no paths)
#
# Revision 0.0  1999/06/25 14:43:16  nicb
# Initial Revision
#
