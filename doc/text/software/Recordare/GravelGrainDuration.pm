## vi:set ts=4 nowrap:
##
## Nicola Bernardini
## Recordare - 1999
##
## GravelGrainDuration.pm - gravel grain duration class for Recordare
##
##
## GravelGrainDuration.pm,v 0.3 2000/06/27 18:10:44 nicb Exp
##

use	Recordare::Base;

package GravelGrainDuration;

use strict;

@GravelGrainDuration::ISA   = qw( Base );

=pod

=head1 NAME

C<GravelGrainDuration> - gravel grain duration class for I<Recordare>

=head1 SYNOPSIS

C<use GravelGrainDuration;>

C<my $lh = new GravelGrainDuration($gdur, $at);>

=cut

sub new($$)
{
	my ($class, $graindurline, $at) = @_;

	my $pkg = $class->SUPER::new('0.3');

	$pkg->{'_start_'} = $at;
	$pkg->parse($graindurline);

	return $pkg;
}

=pod

=head1 DESCRIPTION

C<GravelGrainDuration> is the class for I<Recordare>
gravel grain duration. It parses a grain duration line which has
the following format:

  at:dur:grain duration

- at  = absolute action time
- dur = duration of the grain change
- grain duration = final grain duration value
- the transition, grain duration = final grain duration

=head1 METHODS

=over 4

=item C<at(...)>

sets/gets the absolute action time for this voice

=cut

sub at
{
	my ($this, $at) = @_;
	$this->{'_at_'} = $at if (defined($at));

	return $this->{'_at_'};
}

=pod

=item C<dur(...)>

sets/gets the dur of the voice

=cut

sub dur
{
	my ($this, $dur) = @_;
	$this->{'_dur_'} = $dur if (defined($dur));

	return $this->{'_dur_'};
}

=pod

=item C<factors()>

gets the 2 factors of the function
that regulates the grain duration.

=cut

sub factors($$)
{
	my ($this, $start_gdur) = @_;
	my $end = $this->at()+$this->dur();
	#
	#                                                          (ax+b)
	# select linear_factors for ax+b, exponential_factors for e
	#
	my ($a, $b) = $this->linear_factors($start_gdur, $end);

	return ($a, $b);
}

sub exponential_factors($$$)
{
	my ($this, $start_gdur, $end) = @_;
	#
	# a = (log(y1)-log(y0))/(x1-x0)
	#
	my $a = (log($this->final_grain_duration())-log($start_gdur))/
		    ($end-$this->at());
	#
	# b = log(y1)-(a*x1)
	#
	my $b = log($this->final_grain_duration())-($a*$end);

	return ($a, $b);
}

sub linear_factors($$$)
{
	my ($this, $start_gdur, $end) = @_;
	#
	# a = (y1-y0)/(x1-x0)
	#
	my $a = ($this->final_grain_duration() - $start_gdur) / ($end-$this->at());
	#
	# b = 
	#
	my $b = $this->final_grain_duration() - ($a * $end);

	return ($a, $b);
}

=pod

=item C<final_grain_duration(...)>

sets/gets the final grain duration

=cut

sub final_grain_duration
{
	my ($this, $gdur) = @_;
	$this->{'_final_duration_'} = defined($gdur) ? $gdur :
		$this->{'_final_duration_'};

	return $this->{'_final_duration_'};
}

=pod

=back

=head1 PRIVATE METHODS

=over 4

=item C<parse($voice)>

=cut

sub parse($$)
{
	my ($this, $durline) = @_;
	my $g = new Globals();

	my ($at, $dur, $gdur) = split(':', $durline);

	$this->at($at);
	$this->dur($dur);
	$this->final_grain_duration($gdur);
}

=pod

=back

=head1 VERSION

 GravelGrainDuration.pm,v 0.3 2000/06/27 18:10:44 nicb Exp

=head1 AUTHOR

Nicola Bernardini (nicb@axnet.it)

=cut

1;
#
# GravelGrainDuration.pm,v
# Revision 0.3  2000/06/27 18:10:44  nicb
# adjusted to new path finding logic
#
# Revision 0.2  2000/05/14 18:18:22  nicb
# added linear and exponential factors
#
# Revision 0.1  1999/09/16 07:44:41  nicb
# removed a number of bugs - functional version
#
# Revision 0.0  1999/09/15 11:53:55  nicb
# Initial Revision
#
