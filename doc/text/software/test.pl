#!/usr/bin/perl
#
# test for the Recordare objects
#

use Recordare::Word;

my $tstring = "141|015.143|0.012|ming";

my $word = new Recordare::Word($tstring);

print $word->version(), "\n";
print $word->sound_path(), "\n";
print $word->data_path(), "\n";
print $word->idx(), "\n";
print $word->action_time(), "\n";
print $word->dur(), "\n";
print $word->word(), "\n";
