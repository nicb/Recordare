#!/usr/bin/perl
# vi:set nowrap ts=4:
#
# convert-to-xml.pl,v 0.1 1999/04/16 10:28:01 nicb Exp
#
# This scripts segments the text into single words and applies
# time offsets based on the following criteria:
#
# - each word gets calculated in number of sillables
# - each syllable is counted a fixed time (0.1168 seconds, currently)
# - each end of line is counted 0.5 seconds (this means that an empty
#   line counts 1 second)
#
#
use Getopt::Std;
use strict;

#
# main
#
	my $line = "";
	my @data = ();
	my $speaker = "";

	read_data(\@data);

	foreach $speaker (@data)
	{
		my ($h_input, $h_output) = open_speaker($speaker);
		$main::base_time = 141.760;
		$main::real_total_time = $speaker->{'duration'};
		$main::factor = $main::real_total_time / $main::base_time;
		$main::syl_time = 0.08 * $main::factor;
		$main::nl_time  = 0.6 * $main::factor;
		$main::now      = $main::nl_time;

		print_header($speaker, $h_output);
		while ($line = <$h_input>)
		{
			output($h_output, $line);
		}
		print_footer($speaker, $h_output);

		close_speaker($h_input, $h_output);
	}
#
# end of main
#
#
# list management functions
#
sub read_data($)
{
	my $ra_data = shift;
	my $idx = 0;
	#
	# the list is passed from standard input
	#
	my $line = "";

	while ($line = <>)
	{
		my @fields = split('\|', $line);
		my %data = ();
		
		hashify(\@fields, \%data);

		$ra_data->[$idx++] = \%data;
	}
}

sub hashify($$)
{
	my ($ra_data, $rh_data) = @_;

	# 291.818|catbassa|Caterina|female|caterina-calculated.xml
	$rh_data->{'duration'}			= $ra_data->[0];
	$rh_data->{'audio_filename'}	= $ra_data->[1];
	$rh_data->{'name'}				= $ra_data->[2];
	$rh_data->{'type'}				= $ra_data->[3];
	$rh_data->{'output_filename'}	= $ra_data->[4];
}
#
# file management functions
#

sub open_speaker($)
{
	my $rh_data = shift;
	my $input_filename = 'Mladic-by-sentence.txt';

	open(INPUT, "<$input_filename") || die ("cannot open \"$input_filename\" for reading");
	open(OUTPUT, ">$rh_data->{'output_filename'}") || die("cannot open \"$rh_data->{'output_filename'}\" for writing");

	return (\*INPUT, \*OUTPUT);
}

sub close_speaker($$)
{
	my ($h_input, $h_output) = @_;

	close($h_input);
	close($h_output);
}
#
# output functions
#
sub output($$)
{
	my ($handle, $line) = @_;
	my @words = split(' ', $line);
	my $word = "";

	foreach $word (@words)
	{
		my $num_vowels = num_syllables($word);
		printf $handle ("<Sync time=\"%.3f\"\/>\n%s\n", $main::now, $word);
		$main::now += ($num_vowels * $main::syl_time);
	}

	printf $handle ("<Sync time=\"%.3f\"\/>\n\n", $main::now);
	$main::now += $main::nl_time;
}

sub num_syllables($)
{
	my $word = shift;
	$word = lc($word);
	my @vowels = split(/[bcdfghjklmnpqrstuvwxyz]+/, $word);
	my $result = scalar(@vowels)-1;
	$result = $result > 0 ? $result : 1;

	return $result;
}

sub print_header($$)
{
	my ($speaker, $h_output) = @_;

	print  $h_output "<?xml version=\"1.0\"?>\n";
	print  $h_output "<!DOCTYPE Trans SYSTEM \"trans-13.dtd\">\n";
	printf $h_output ("<Trans scribe=\"Nicola Bernardini\" audio_filename=\"%s\" version=\"3\" version_date=\"990413\">\n", $speaker->{'audio_filename'});
	print  $h_output "<Speakers>\n";
	printf $h_output ("<Speaker id=\"spk1\" name=\"%s\" check=\"no\" type=\"%s\" dialect=\"native\" accent=\"\"/>\n", $speaker->{'name'}, $speaker->{'type'});
	print  $h_output "</Speakers>\n<Episode>\n";
	printf $h_output ("<Section type=\"report\" startTime=\"0\" endTime=\"%.3f\">\n", $speaker->{'duration'});
	printf $h_output ("<Turn startTime=\"0\" endTime=\"%.3f\" speaker=\"spk1\">\n", $speaker->{'duration'});
}

sub print_footer($$)
{
	my ($speaker, $h_output) = @_;
	print $h_output "</Turn>\n</Section>\n</Episode>\n</Trans>\n";
}
#
# convert-to-xml.pl,v
# Revision 0.1  1999/04/16 10:28:01  nicb
# corrected syllable counting bug
# modified to read speaker data from a list (batch job functionality)
#
# Revision 0.0  1999/04/16 08:25:39  nicb
# Initial Revision
#
