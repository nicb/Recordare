#
# $Id: convert2sc.awk 38 2014-02-12 14:44:53Z nicb $
#
# this converts a dat file into an sc (spreadsheet) file
#
BEGIN {
	FS="|";
	n = 5;

	print "format A 3 0 0";
	print "format B 15 0 0";
	print "format C 15 0 0";
	print "format D 15 3 0";
	print "format E 15 3 0";
}
/^#/ {
	# this is a comment, we send it out unchanged;
	print;
	next;
}
{
	printf("let A%d = %d\n", n, $1);
	printf("rightstring C%d = \"%s\"\n", n, $4);
	printf("let D%d = %.3f\n", n, $2);
	printf("let E%d = %.3f\n", n, $3);
	++n;
}
#
# $Log$
