			sr=44100
			kr=44100
			ksmps=1
			nchnls=1

			instr 1

ktime			oscil1i	0,p4,p3,1
krmsr,kamp,kerr,kpitch	lpread	ktime,p5

krmsr			port	krmsr,0.05
kamp			port	kamp,0.2
kerr			port	kerr,0.008
kpitch			port	kpitch,0.001

kselect			= (kerr > 0.015 ? 1 : 0)

			display	krmsr,p3
			display	kamp,p3
			display	kerr,p3
			display	kpitch,p3
			display	kselect,p3

anoise			rand	kamp
avoiced			buzz	kamp,kpitch,30,2

			if kselect == 1 kgoto noised
asig			=	avoiced
			kgoto	synthesize
noised:
asig			=	anoise*0.2

synthesize:
asig			=	asig*3
aout			lpreson	asig
aout			balance aout,asig
			out	aout
			endin
