#
# xml checker: check consistency of xml-calculated file
#
BEGIN {
	last_time = 0;
}
/^<Sync time=.*\/>$/ {
	splitline[0] = split($0, splitline, "\"");
	if (splitline[2] <= last_time)
		print "WARNING: time " splitline[2] " out of sync!";

	last_time = splitline[2];
}
