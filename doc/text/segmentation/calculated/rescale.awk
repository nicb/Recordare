#
# xml checker: check consistency of xml-calculated file
#
BEGIN {
	factor = factor ? factor : 1;
}
/^<Sync time=.*\/>$/ {
	splitline[0] = split($0, splitline, "\"");
	splitline[2] *= factor ;
	print splitline[1] "\"" splitline[2] "\"" splitline[3];
	next;
}
{
	print;
}
