
#include <sys/types.h>
#include <unistd.h>

int
swap(short src, short *dest)
{
	*dest = (src << 8);
	*dest |= ((src >> 8) & 0xff);
}

int
main()
{
	int fd = 0;
	int eof = 1;
	short buf = 0;
	short swapdest = 0;

	while(eof)
	{
		eof = read(fd, &buf, sizeof(buf));
		swap(buf, &swapdest);

		printf("%d\n", swapdest);
	}
}
