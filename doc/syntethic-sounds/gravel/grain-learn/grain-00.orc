	sr=44100
	kr=4410
	ksmps=10
	nchnls=1
 ;;;;;;;;;;;;;;; graintest.orc

    instr 1
    insnd = 10
    isrsnd= 44100
    ibasfrq = isrsnd / ftlen(insnd)   ; Use original sample rate of insnd file
kamp   expseg     8000, p3/2, 8000, p3/2, 16000
kpitch line     ibasfrq, p3, ibasfrq * .8
kdens  line     600, p3, 200
kaoff  line     0, p3, 5000
kpoff  line     0, p3, ibasfrq * .5
kgdur  line 	.4, p3, .1
imaxgdur = .5
ar grain kamp, kpitch, kdens, kaoff, kpoff, kgdur, insnd, 5, imaxgdur, 0.0
       out ar
       endin
