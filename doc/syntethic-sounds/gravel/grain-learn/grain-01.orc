	sr=44100
	kr=4410
	ksmps=10
	nchnls=1
 ;;;;;;;;;;;;;;; graintest.orc

    instr 1
    insnd = 10
    isrsnd= 44100
    ibasfrq = isrsnd / ftlen(insnd)   ; Use original sample rate of insnd file
   ;ibasfrq = ibasfrq/16
    imaxgdur = (ftlen(insnd)/isrsnd)
    istartdur = imaxgdur*.01
    imingdur = imaxgdur*.001
    iamp = ampdb(78)
    iamp2 = iamp*0.5
    iofffrqstart=ibasfrq*0.15
    iofffrqend=ibasfrq*0.8
kamp   expseg     iamp, p3/2, iamp, p3/2, iamp2
kpitch line     ibasfrq, p3, ibasfrq * .4
kdens  expon    300, p3, 3000
kaoff  line     0, p3, iamp2
kpoff  line     iofffrqstart, p3, iofffrqend
kgdur  line 	istartdur, p3, imingdur
ar grain kamp, kpitch, kdens, kaoff, kpoff, kgdur, insnd, 5, imaxgdur, 0
       out ar
       endin
