;
; used to build the sound for granular synthesis
;
; this is just a damped sinewave (a bit like FOF)
;
	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

	instr 1
iamp	=	ampdb(80)
;kamp	line	iamp,p3,0.00001
aout	oscil	iamp,p4,1
	out	aout
	endin
