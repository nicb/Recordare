;;;;;;;;;;;;;;; graintest.sco

f5 0 1024 20 2 ; Hanning window
;f5 0 1024 20 7 1 2 ; Kaiser window (not working)
;f5 0 1024 20 5 1; Blackman-Harris window
;f5 0 1024 20 4 1; Blackman window
;f5 0 1024 20 3 1; Bartlett window
;f5 0 1024 5 0.00001 10 1 1014 0.00001
;f5 0 1024 6 0 25 0.3 25 0.707 25 1 75 0.707 437 0.303 437 0
f10 0 65536 1 "Sound.wav" 0 0 0
;f10 0 65536 9 16 1 0
;
; p4 = amp, p5 = density start, p6 = density end
; p7 = imaxgdur starting multiply
; p8 = imaxgdur ending   multiply
; p9 = distance in meters
; p10= left-right pan (1=left; 0=right)
;
i100 0 18

i1   0 2   80	1000	1900	.001	.0015	4	0.5
i1 1.2 12  80	400	800	.001	.0016	8	0.8
i1   4 2.4 80	900	2000	.001	.0017	2	0.1
i1   8 1.8 80	1000	2000	.001	.0018	1.5	0.9
i1  12 3   80	1100	2000	.001	.0019	12	0
e
