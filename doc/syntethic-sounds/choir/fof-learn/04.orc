; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
a2	expseg  5,  p3*.8,  200,  p3*.2,  150
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof  15000, a2,   650,    0,  40,   .003, .02,  .007,    5,     1,    2,   p3
	out  a1
	endin
