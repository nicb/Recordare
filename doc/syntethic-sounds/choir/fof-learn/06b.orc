; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
a2	line 400,  p3,  800
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof  15000,  5,   a2,     0,   1,   .003,  .5,  .1,      3,     1,    2,    p3,     0,      1
	out  a1
	endin
