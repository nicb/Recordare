; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
;k1	linseg  0,  p3,  10                             ; kband
k1	expseg  0.00001,  p3,  100                      ; kband
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
;a1	fof 15000,   2,   300,    0,  k1,   .003,  .5,    .1,    2,      1,    2,   p3
a1	fof 15000, 440,   300,    6,  k1,   .003,  .5,    .1,  200,      1,    2,   p3
	out  a1
	endin
