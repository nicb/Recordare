; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
k1	linseg  100,  p3/4,  0,  p3/4,  100,  p3/2,  100              ; kband
k2	linseg  .003,  p3/2,  .003,  p3/4,  .01,  p3/4,  .003         ; kris
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof 14000, 100,  440,    0,   k1,    k2,  .02,  .007,    3,     1,    2,   p3
	out  a1
	endin
