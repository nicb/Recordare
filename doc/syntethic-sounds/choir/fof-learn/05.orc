; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
k1   linseg   0,  p3*.1,  0,  p3*.8,  6,  p3*.1,  6
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof  15000, 200,  650,   k1,  40,   .003, .02,  .007,    5,     1,    2,   p3
	out  a1
	endin
