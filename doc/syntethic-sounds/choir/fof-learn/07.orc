; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
acarr     line    400,  p3,  800
index     =       2.0
imodfr    =       400
idev      =       index * imodfr
amodsig   oscil   idev, imodfr, 1
;ar	fof  xamp  xfund   xform      koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof 15000,  5,  acarr+amodsig,  0,  1,    .003,  .5,  .1,      3,     1,   2,     p3,    0,       1
	out  a1
	endin
