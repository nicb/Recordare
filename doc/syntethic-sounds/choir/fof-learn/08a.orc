; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
k1	line .003,  p3,  .1                          ; kris
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof 15000,   2,   300,    0,   0,    k1,  .5,    .1,    2,      1,    2,   p3
	out  a1
	endin
