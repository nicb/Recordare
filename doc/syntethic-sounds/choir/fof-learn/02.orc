; vi:set nowrap:
	sr = 22050
	kr = 441
	ksmps = 50

	instr     1
a2	linseg    0,  p3*.3,  20000,  p3*.3,  15000,  p3*.3,  0
;ar	fof  xamp  xfund xform  koct kband  kris  kdur  kdec  iolaps  ifna  ifnb itotdur [iphs] [ifmode]
a1	fof  a2,    200,  650,    0,  40,   .003, .02,  .007,    5,     1,    2,   p3
	out  a1
	endin
