	sr=22050
	kr=441
	ksmps=50
	nchnls=1

zakinit	1,4

	instr 1
	iamp =	6
	ioff =  iamp
k1	oscil	iamp,0.1,1,0
k2	oscil	iamp,0.1,1,0.25
k3	oscil	iamp,0.1,1,0.50
k4	oscil	iamp,0.1,1,0.75
	zkw	k1+ioff,0
	zkw	k2+ioff,1
	zkw	k3+ioff,2
	zkw	k4+ioff,3
	endin

	instr 2
k1	zkr	0
k2	zkr	1
k3	zkr	2
k4	zkr	3
	display k1,0.5
	display k2,0.5
	display k3,0.5
	display k4,0.5
	endin
