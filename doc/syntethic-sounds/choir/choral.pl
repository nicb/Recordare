#!	/usr/bin/perl
# vi:set ts=4 nowrap:
#
# choral.pl,v 0.0 1999/04/05 17:56:35 nicb Exp
#
# This perl script picks up on standard in a data file which carries
# '#'-initiated comment lines and pipe-separated data fields whose format
# is as follows:
#
# <chord no.>|<voice>|<note>|<vowel>|<dyn>
#
# where:	<chord no.>	is the chord number
#			<voice>		is the voice type (SATB)
#			<note>		is the note to be sung (c3, d#4, bf2, etc.)
#			<vowel>		is the vowel to be sung ('a', 'e', etc.)
#			<dyn>		is the dynamics (pp, p, f etc.)
#
#
use Data::Dumper;
use strict;

#
# main
#{
	my $line = "";
	my @CHORDS = ();
	my %voices = ( 'S' => 4, 'A' => 2, 'T' => 1, 'B' => 3, );

	while ($line = <>)
	{
		read_chords($line, \@CHORDS, \%voices) unless $line =~ /^#/;
	}

	produce_score(\@CHORDS);
#}
# end of main
#

sub read_chords($$$)
{
	my ($line, $ra_chords, $rh_voices) = @_;
	chop($line);
	my ($chordno, $voice, $note, $vowel, $dyn) = split('\|', $line);
	
	if (!defined($ra_chords->[$chordno]))
	{
		my @chord = ();
		$ra_chords->[$chordno] = \@chord;
		reset_random_cache($rh_voices);
		#print "Cache reset...\n";
		#print Dumper($rh_voices);
	}

	my $voicenumber = get_voice_number($voice, $rh_voices);
	$ra_chords->[$chordno]->[$voicenumber] =
		{ 'note' => $note, 'vowel' => $vowel, 'dyn' => $dyn,
		  'voicename' => $voice };

	#print Dumper([ $chordno, $voicenumber, $ra_chords->[$chordno]->[$voicenumber] ]);
}

sub get_voice_number($$)
{
	my ($voice, $rh_voices) = @_;

	return $rh_voices->{$voice};
}

sub round($)
{
	my $num = shift;

	return int($num+0.5);
}

sub reset_random_cache($)
{
	my $rh_voices = shift;
	my @nums = ( 1, 2, 3, 4 );
	my $k = "";

	foreach $k (keys %$rh_voices)
	{
		my $possibles = scalar(@nums)-1;
		my $rand = $possibles ? round(rand($possibles)) : 0;
		$rh_voices->{$k} = $nums[$rand];
		splice(@nums, $rand, 1);
	}
}

#
# produce_score
#

sub produce_score($)
{
	my $ra_chords = shift;
	my $i = 0;
	my $cur_at = 0;
	my $chord_dur = 14;
	my $single_voice_dur = 12;
	
	for ($i = 1; $i < scalar(@$ra_chords); ++$i)
	{
		my $voice = 0;
		my $voice_phase = 1.1;
		my $real_at = $cur_at;

		for ($voice = 1; $voice < scalar(@{$ra_chords->[$i]}); ++$voice)
		{
			produce_line($real_at, $single_voice_dur, $ra_chords->[$i]->[$voice]);
			$real_at += $voice_phase;
		}

		$cur_at += $chord_dur;
	}
}

sub produce_line($$$)
{
	my ($start_at, $dur, $rh_voice) = @_;
	my $fun = get_voice_fun($rh_voice->{'voice'});
	my $note = get_note($rh_voice->{'note'});
	my $dyn = get_dynamics($rh_voice->{'dyn'});
	my $vow = get_vowel($rh_voice->{'vowel'});
	my $numvoices = 10;
	my $i = 0;
	my $voice_phase = 0.8;
	my $at = $start_at;

	print "; voice $rh_voice->{'voicename'} for note $rh_voice->{'note'}\n";

	for ($i = 0; $i < $numvoices; ++$i)
	{
		$note = shake_note($note);
		printf("i1 %7.4f %7.4f %9.6f %d %2d %4.1f\n", $at, $dur, $note, $vow,
			$fun, $dyn);
		$at += $voice_phase;
	}

}

sub get_voice_fun($)
{
	my $voice = shift;
	my @voices = ( 12, 12, 13, 13, );

	return $voices[$voice];
}

sub get_note($)
{
	my $note = shift;
	my $octave = get_octave($note);
	my $semitone = get_semitone($note);

	return $octave+($semitone*.01);
}

sub get_octave($)
{
	my $note = shift;
	$note =~ s/\D+//;

	return $note + 5;
}

sub get_semitone($)
{
	my $note = shift;
	$note =~ s/\d+//;
	my %notes = ( 'c' => 0, 'c#' => 1, 'df' => 1, 'd' => 2, 'd#' => 3,
			'ef' => 3, 'e' => 4, 'e#' => 5, 'ff' => 4, 'f' => 5,
			'f#' => 6, 'gf' => 6, 'g' => 7, 'g#' => 8, 'af' => 8,
			'a' => 9, 'a#' => 10, 'bf' => 10, 'b' => 11, );
	
	die ("Note $note not defined") if (!defined($notes{$note}));

	return $notes{$note};
}

sub get_dynamics($)
{
	my $dyn = shift;
	my $dtop = 90;
	my %dyns = ( 'ppp' => 6, 'pp' => 12, 'p' => 18, 'mp' => 24,
		'mf' => 30, 'f' => 36, 'ff' => 42, 'fff' => 48 );
	my $dbot = 90 - $dyns{'fff'};

	die ("Dynamic $dyn not defined") if (!defined($dyns{$dyn}));

	return $dbot + $dyns{$dyn};
}

sub get_vowel($)
{
	my $vow = shift;
	my %vowels = ( 'a' => 1, 'e' => 2, 'i' => 3, 'o' => 4, 'u' => 5 );

	die ("Vowel $vow not defined") if (!defined($vowels{$vow}));

	return $vowels{$vow};
}
#
#

sub shake_note($)
{
	my $note = shift;
	my $dev = -0.0005 + rand(0.001);

	return $note + $dev;
}

#
#
#

sub print_header($)
{
	my $rev_string = shift;

	print "; Produced automagically by $ARGV[0] $rev_string\n; DO NOT EDIT!\n";
	print "; vi:set nowrap:\n";
	print "f1  0  4096  10  1\n";
	print "f2  0  1024  19  .5  .5  270  .5\n";
	print "; vocali femminili\n";
	print ";f12 0 256 -2 650 1 69 1100 0.38 95 2860 0.23 95 3300 0.26 102 4500 0.11 120 0 0 0  0 0 500 1  75 1750 0.36 104 2450 0.3 123 3350 0.19 140 5000 0.07 165 0 0 0 0 0 330 1 89 2000 0.19 114 2800 0.28 132 3650 0.32 145 5000 0.11 162 0 0 0 0 0 400 1 86 840 0.24 109 2800 0.052 130 3250 0.06 138 4500 0.029 157 0 0 0 0 0 280 1 70 650 0.12 132 2200 0.004 156 3450 .003 224 4500 .002 272\n";
	print ";            |------------------------------a-----------------------------|            |-------------------------------e------------------------------|           |-------------------------------i------------------------------|           |-------------------------------o-------------------------------|           |-------------------------------u------------------------------|\n";
	print "f12 0 256 -2 650 1 69 1100 0.38 95 2960 0.23 95 3600 0.26 102 4900 0.11 120 0 0 0  0 0 500 1  75 1750 0.36 104 2550 0.3 123 3650 0.19 140 5400 0.07 165 0 0 0 0 0 330 1 89 2000 0.19 114 2900 0.28 132 3950 0.32 145 5400 0.11 162 0 0 0 0 0 400 1 86 840 0.24 109 2900 0.052 130 3550 0.06 138 4900 0.029 157 0 0 0 0 0 280 1 70 650 0.12 132 2300 0.004 156 3750 .003 224 4900 .002 272\n";
	print "; vocali maschili\n";
	print ";            |-------------------------------a-----------------------------|           |-------------------------------e----------------------------|           |--------------------------i------------------------------|           |---------------------------o----------------------------|           |-----------------------------u-----------------------------|\n";
	print "f13 0 256 -2 609 1 78 1000 0.5 88 2450 0.25 123 2700 0.28 128 3240 0.065 132 0 0 0 0 0 400 1 64 1700 0.35 81 2300 0.4 101 2900 0.28 119 3400 0.11 134 0 0 0 0 0 238 1 73 1741 .1 102 2450 .15 123 2900 .1 132 4000 .026 150 0 0 0 0 0 325 1 73 700 .25 80 2550 .05 125 2850 .08 131 3100 .04 135 0 0 0 0 0 360 1 51 750 .25 61 2400 .034 168 2675 .048 184 2950 .017 198\n";
	print ";\n;\n";
}

BEGIN {
	print_header("\$Revision\$");
}
#
# choral.pl,v
# Revision 0.0  1999/04/05 17:56:35  nicb
# Initial Revision - functional
#
