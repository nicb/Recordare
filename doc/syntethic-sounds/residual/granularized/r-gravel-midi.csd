<CsoundSynthesizer>
;
; $Id: r-gravel-midi.csd,v 0.0 1999/12/31 13:15:38 nicb Exp nicb $
;
; Recordare - Gravel-choir MIDI test instrument
;
; to use it:
; $ modprobe -k /dev/v_midi
; $ csound --sched -M /dev/midi00 -do devaudio r-gravel-midi.csd & \
;   xphat -m /dev/midi01
;
<CsOptions>
</CsOptions>
<CsInstruments>
	sr=44100
	kr=441
	ksmps=100
	nchnls=1
;
; macros
;
#define	FORMANT(in'a'bw'out') #	indx	=	indx+1
		     		if$a	table	indx,ivowel,0
                    		indx	=	indx+1
		    		ia$a	table	indx,ivowel,0
		    		ia$a	=	ampdb(ia$a)
		    		$out	butbp	$in,if$a,$bw
		    		$out	=	$out*ia$a #

;
; granule instrument
;
	instr 1
	;
	; granule object parameters
	;
	iamp=ampdb(120)
	insnd = 100
	isrsnd=ftsr(insnd)
	iftlen=nsamp(insnd)
	iftdur=iftlen/isrsnd
	iskip=0
	irandskip=(iftdur-iskip)*.6
	inote=cpspch(8.10)
	ibasefrq=cpspch(8.10)
	iratio=inote/ibasefrq
	imode=0
	ivoices=8
	igaprand=100		; percent
	isizerand=90		; percent
	iattack=0		; percent
	idecay=25		; percent
	ipitches=1
	irise=0.1
	irelease=0.1
	;
	; gap parameters and calculation
	;
	ihigap=2
	ilogap=0.0005
	igapamp=ihigap-ilogap
	;
	; size parameters and calculation
	;
	ilosize=0.0005
	ihisize=1
	isizerange=ihisize-ilosize
	iszMrange=isizerange*0.01
	iszmrange=isizerange*0.0001
	iszMstep=iszMrange/127
	iloszM=0
	ihiszM=iszMrange
	iloszm=0
	ihiszm=iszmrange


	iseed=rnd(1)

kgap	midictrl 1,ilogap,ihigap
	printk2	 kgap,0

kgsize	midictrl 0,ilosize,ihisize
kgszM   midictrl 12,iloszM,ihiszM
kgszm   midictrl 13,iloszm,ihiszm
kgsize	=	 kgsize+kgszM+kgszm
	printk2	 kgsize,8

ar	granule	iamp,ivoices,iratio,imode,0,insnd,ipitches,iskip,irandskip,\
		iftdur,kgap,igaprand,kgsize,isizerand,iattack,idecay,iseed

kamp	midictrl 7,ampdb(-80),ampdb(0)
	printk2	 kamp,16

ar	=	ar*kamp

	;
	; filter bank
	;
	ilokbw=0.5
	ihikbw=5000
kbw	midictrl 14,ilokbw,ihikbw
ivowel	midictrl 15,1,13
	printk2	 kbw,24
	printk2  ivowel,32

indx	=	-1
	$FORMANT(ar'1'kbw'a1')
	$FORMANT(ar'2'kbw'a2')
	$FORMANT(ar'3'kbw'a3')
	$FORMANT(ar'4'kbw'a4')
	$FORMANT(ar'5'kbw'a5')

	out	a1+a2+a3+a4+a5
	endin
</CsInstruments>
<CsScore>
;
; vowel tables (from Dodge and Jerse, 2nd Edition, p.230-231)
;
; female vowels
;
f1  0 16 -2 650 0 1100 -8  2860 -13 3300 -12 4500 -19		; A
f2  0 16 -2 500 0 1750 -9  2450 -10 3350 -14 5000 -23		; E
f3  0 16 -2 330 0 2000 -14 2800 -11 3650 -10 5000 -19		; IY 
f4  0 16 -2 400 0  840 -12 2800 -26 3250 -24 4500 -31		; O
f5  0 16 -2 280 0  650 -18 2200 -48 3450 -50 4500 -52            ; OO (=U)
;
; male vowels
;
f6  0 16 -2 609 0 1000 -6  2450 -12 2600 -11 3240 -24		; A
f7  0 16 -2 400 0 1700 -9  2300 -8  2900 -11 3400 -19		; E
f8  0 16 -2 238 0 1741 -20 2450 -16 2900 -20 4000 -32		; IY
f9  0 16 -2 325 0  700 -12 2550 -26 2850 -22 3100 -28		; O
f10 0 16 -2 360 0  750 -12 2400 -29 2675 -26 2950 -35		; OO
f11 0 16 -2 415 0 1400 -12 2200 -16 2800 -18 3300 -27		; U
f12 0 16 -2 300 0 1600 -14 2150 -12 2700 -15 3100 -23		; ER
f13 0 16 -2 400 0 1050 -12 2200 -19 2650 -20 3100 -29		; UH
;
;
f100 0 16777216 1 "residual.wav" 0 0 0
f0 10000
</CsScore>
</CsoundSynthesizer>
