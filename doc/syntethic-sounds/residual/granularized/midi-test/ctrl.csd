<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>
	sr=44100
	kr=441
	ksmps=100
	nchnls=1

	instr 1
kfrq	midictrl	15,110,880
aout	oscil		20000,kfrq,1
	out		aout
	endin
</CsInstruments>
<CsScore>
f1 0 8192 10 1
f0 10000
</CsScore>
</CsoundSynthesizer>
