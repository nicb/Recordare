;
; $Id: r-gravel.orc,v 0.2 1999/12/28 08:29:37 nicb Exp nicb $
;
; Recordare - Gravel-choir instrument
;
	sr=44100
	kr=44100
	ksmps=1
	nchnls=2

zakinit		19,59
garevl	init	0
garevr	init	0
givel	init	340
;
; the zak system holds the variables in the following way:
; 1) there are 20 granule instruments which dump their output
;    in zak a-variables 0-19
; 2) each granule instrument has the following external controllers:
;    a - amplitude controller (k variables  0-19)
;    b - grain gap controller (k variables 20-39)
;    c - grain dur controller (k variables 40-59)

;
; amplitude instruments
;
	instr 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
	ioutindex=p1-1
	iampindex=ioutindex
	iaend=ampdb(p4)

iastart	zir	iampindex

kamp	line	iastart,p3,iaend
	zkw	kamp,iampindex

	endin
;
; grain dur control instruments
;
	instr 41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60
	ioutindex=p1-41
	iampindex=ioutindex
	igapindex=ioutindex+20
	idurindex=igapindex+20
	itype=p5

	idend=p4
idstart	zir	idurindex

	if itype == 0 then kgoto linear
kdur	expon	idstart+0.000001,p3,idend
	kgoto	end
linear:
kdur	line	idstart,p3,idend
end:
	zkw	kdur,idurindex

	endin
;
; granule instrument
;
	instr 81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100
	;
	; zak indexing
	;
	ioutindex=p1-81
	iampindex=ioutindex
	igapindex=ioutindex+20
	idurindex=igapindex+20
	;
	; granule object parameters
	;
	iamp=ampdb(p4)
	insnd = p6
	isrsnd=(insnd > 9 ? ftsr(insnd) : sr)
	iftlen=(insnd > 9 ? nsamp(insnd) : ftlen(insnd))
	iftdur=iftlen/isrsnd
	iskip=p13		; skip first second for vocal files
	iskip=0
	irandskip=(iftdur-iskip)*.6
	inote=cpspch(p5)
	ibasefrq=(insnd == 10 ? cpspch(8.10) : cpspch(8.04))
	iratio=inote/ibasefrq
	imode=0
	ivoices=4
	igaprand=80		; percent
	isizerand=30		; percent
	iattack=25		; percent
	idecay=8		; percent
	ipitches=4
	ipitch1=p9
	ipitch2=p10
	ipitch3=p11
	ipitch4=p12
	irise=p3*0.1
	irelease=p3*0.01
	;
	; gap parameters and calculation
	;
	ihigap=p7
	ilogap=p8
	igapamp=ihigap-ilogap

	iseed=rnd(1)

kgap	oscil1	0,igapamp,p3,30
kgap	=	ihigap-kgap
	zkw	kgap,igapindex		; for display purposes

kgsize	zkr	idurindex

	print	insnd,iftdur,isrsnd,iskip,irandskip,iratio
ar	granule	iamp,ivoices,iratio,imode,0,insnd,ipitches,iskip,irandskip,\
		iftdur,kgap,igaprand,kgsize,isizerand,iattack,idecay,iseed,\
		ipitch1,ipitch2,ipitch3,ipitch4

kamp	zkr	iampindex
ar	linen	ar*kamp,irise,p3,irelease

	zawm	ar,ioutindex
	endin

;
; output instrument (room)
;
	instr 101,102,103,104,105,106,107,108,109,110,\
	      111,112,113,114,115,116,117,118,119,120
	;
	; input indexings
	;
	iaindex=p1-101		; a-value
	iampindex=p1-101	; k-values
	;
	; p4	 p5       p6      p7   p8
	;atten attnrev roomatten xpos ypos
	;
	idur  = p3
	iattadir = ampdb(p4)	; attenuazione di ampiezza [0]
 	iattarev = ampdb(p5)	; attenuazione riverbero [-30]
	iroomatten=ampdb(p6)	; attenuazione room
	ix       = p7
	iy       = p8
	iroom = 1		; table room def (set by engines)
	;
	; audio input
	;
asend	zar	iaindex		; signal
kamp	zkr	iampindex	; amplitude envelope

asig	=	asend*iattadir*kamp;
	zacl	iaindex,iaindex

kx	randi	0.1,0.7		; we move slightly the source
kx	=	ix+(kx*ix)
ky	randi	0.1,0.6
ky	=	iy+(ky*iy)

	                ;DEFINIZIONE INNER ROOM
	ixl   table 1,iroom     ;coordinate(in m) altoparlante
	iyl   table 2,iroom	;sinistro 
	ixr   table 3,iroom     ;coordinate(in m) altoparlante 
	iyr   table 4,iroom     ;destro       
	                ;DEFINIZIONE OUTER ROOM       
	ixmax table 5,iroom     ;larghezza max positiva (in m) 
	ixmin table 6,iroom     ;larghezza max negativa (in m) 
	iymax table 7,iroom     ;lunghezza max positiva (in m)
	iymin table 8,iroom     ;lunghezza max negativa (in m) 
	iy    table 10,iroom	; max coordinata y della sorgente
	;print ixl,iyl,ixr,iyr,ixmax,ixmin,iymax,iymin
 
	ideltaf = sr/2 - 2000
	kfilt = sr/2 + ideltaf*ky/iy
	af0 tone asig, kfilt 
    
	asigf = (ky >= 0 ? asig : af0) 
    
	k1     = kx-ixl                 
	k2     = ky-iyl 
	k3     = kx-ixr
	k4     = ky-iyr 
	k1q    = k1*k1
	k2q    = k2*k2
	k3q    = k3*k3
	k4q    = k4*k4     
	kxmax  = 2*(ixmax-kx)
	kymax  = 2*(iymax-ky)
	kxmin  = 2*(kx-ixmin)
	kymin  = 2*(ky-iymin)
	                          ;CALCOLO SEGNALE DIRETTO 
	kdl = sqrt(k1q+k2q)       ;dist dir>L
	kdr = sqrt(k3q+k4q)       ;dist dir>R
	
;i muri sono numerati in senso orario a partire da quello di fronte 
;all'ascoltatore (m1)
	
	k5  = k2+kymax            ;CALCOLO RIFLESSIONI
	k1l = sqrt(k1q+(k5*k5))   ;dist rif m1>l
	                
	k6  = k4+kymax
	k1r = sqrt(k3q+(k6*k6))   ;dist rif m1>r  
	
	k7  = k1+kxmax          
	k2l = sqrt(k2q+(k7*k7))   ;dist rif m2>l
	
	k8  = k3+kxmax
	k2r = sqrt(k4q+(k8*k8))   ;dist rif m2>r 
	
	k9  = kymin-k2
	k3l = sqrt(k1q+(k9*k9))   ;dist rif m3>l
 
	k10 = kymin-k4
	k3r = sqrt(k3q+(k10*k10)) ;dist rif m3>r 
	
	k11 = kxmin-k1
	k4l = sqrt(k2q+(k11*k11)) ;dist rif m4>l
	
	k12 = kxmin-k3
	k4r = sqrt(k4q+(k12*k12)) ;dist rif m4>r
	
	kdel1  port  kdl/givel,0.1
	kdel2  port  kdr/givel,0.1
	kdel3  port  k1l/givel,0.1
	kdel4  port  k1r/givel,0.1 
	kdel5  port  k2l/givel,0.1
	kdel6  port  k2r/givel,0.1
	kdel7  port  k3l/givel,0.1
	kdel8  port  k3r/givel,0.1
	kdel9  port  k4l/givel,0.1
	kdel10 port  k4r/givel,0.1  
	
	adel   delayr   1          ;Path diretto
	a1     deltapi  kdel1      ;dir l
	a2     deltapi  kdel2      ;dir r
	       delayw   asigf	   ;filtrato se passa dietro 

; enfasi toni medi per il passaggio laterale

	k0 = 0
	kk = 1
	ienfasi = 3  ; 0= enfasi nulla, 1 = regolare
	
	afilt butterbp asig, 3000, 4000
	afilte = afilt*ienfasi
	
	kabsy  = abs(ky)
	kafilt = (kabsy > 1 ? k0 : kk-kabsy)
	kbal   = (kx<0 ? k0 : kk)
	kampl = kafilt*(1-kbal)
	kampr = kafilt*kbal
	al = afilte*kampl     
	ar = afilte*kampr
	
	adel2  delayr   1          ;Path riflessi
	a3     deltapi  kdel3      ;rif m1>l  
	a4     deltapi  kdel4      ;rif m1>r
	a5     deltapi  kdel5      ;rif m2>l  
	a6     deltapi  kdel6      ;rif m2>r             
	a7     deltapi  kdel7      ;rif m3>l  
	a8     deltapi  kdel8      ;rif m3>r
	a9     deltapi  kdel9      ;rif m4>l  
	a10    deltapi  kdel10     ;rif m4>r          
	       delayw   asig*iroomatten
	 
	alpf3l tone a7, 1400     ;filtro lpf su riflessioni muro 3
	alpf3r tone a8, 1400     ;20000/1+65%(ixmin) 
	 	 
	aleft  = (a1+al)/kdl+a3/k1l+a5/k2l+alpf3l/k3l+a9/k4l	 
	aright = (a2+ar)/kdr+a4/k1r+a6/k2r+alpf3r/k3r+a10/k4r
       
	outs aleft, aright
       
	garevl   = garevl+aleft*iattarev*(1/sqrt(kdl)) ;intensita' riverb l 
	garevr   = garevr+aright*iattarev*(1/sqrt(kdr));intensita' riverb R 
	endin

;
; displays instrument
;
; allows displaying one instrument complex at a time
; (starting from instrument 0)
;
	instr 180
	instodisplay=p4
	;
	; zak indexing
	;
	ioutindex=p1-180+instodisplay
	iampindex=ioutindex
	igapindex=ioutindex+20
	idurindex=igapindex+20

kgap	zkr	igapindex
kgsize	zkr	idurindex
kamp	zkr	iampindex
ktime	times
	dumpk4	ktime,kgap,kgsize,kamp,"kvar.log",8,0.03

	endin
	    
;
; reverb instrument
;
	instr 200
al	nreverb	garevl,1.2,0.3
ar	nreverb	garevr,1.23,0.28

	outs	al,ar
garevl	=	0
garevr	=	0
	endin
;
; $Log: r-gravel.orc,v $
; Revision 0.2  1999/12/28 08:29:37  nicb
; added linear/exponential switch
; *** first version orchestra ***
;
; Revision 0.1  1999/09/17 10:48:03  nicb
; added room attenuation
;
; Revision 0.0  1999/09/16 08:26:08  nicb
; Initial Revision
;
; Revision 0.4  1999/09/11 16:22:24  nicb
; added room calculation
; ready for production
;
; Revision 0.3  1999/09/11 15:52:32  nicb
; third version freeze
;
; Revision 0.2  1999/09/11 15:46:40  nicb
; fixed bug in random seeding
; reduced to 13 p-fields
;
; Revision 0.1  1999/09/11 15:34:31  nicb
; second version freeze (with single granule shots)
;
; Revision 0.0  1999/09/11 12:36:50  nicb
; Initial Revision
;
