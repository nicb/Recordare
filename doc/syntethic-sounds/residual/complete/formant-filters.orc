;
; formant-filters.orc,v 0.3 1999/12/28 08:24:48 nicb Exp
;
; formant-filters.orc: formant-displaced filters whose bandwidth is
;                      score controlled
; 
	sr=44100
	kr=4410
	ksmps=10
	nchnls=1

	instr 1
	iskip=p4		; wave skip time
	ivowel=p5		; table number with vowel values
	ibwstart=p6
	ibwend=p7

#define	FORMANT(a) # indx	=	indx+1
		     if$a	table	indx,ivowel,0
                     indx	=	indx+1
		     ia$a	table	indx,ivowel,0
		     ia$a	=	ampdb(ia$a)
		     a$a	butbp	ain,if$a,kbw
		     a$a	=	a$a*ia$a #

ain	soundin	"./residual.wav",iskip

kbw	expseg	ibwstart,p3/2,ibwend,p3/2,ibwend

	;
	; filter bank
	;
indx	=	-1
	$FORMANT(1)
	$FORMANT(2)
	$FORMANT(3)
	$FORMANT(4)
	$FORMANT(5)

	print	if1,ia1,if2,ia2,if3,ia3,if4,ia4,if5,ia5

aout	=	a1+a2+a3+a4+a5
aout	linen	aout,0.1,p3,0.1

	;
	; output stage
	;
aout	balance	aout,ain

	out	aout

	endin
;
; formant-filters.orc,v
; Revision 0.3  1999/12/28 08:24:48  nicb
; moved to butterworth filters
;
; Revision 0.2  1999/12/28 08:18:29  nicb
; removed vibrato from source
; reduced formant filters to macros for better handling
;
; Revision 0.1  1999/12/27 16:22:24  nicb
; added vibrato (just for a test, now removing it)
;
; Revision 0.0  1999/12/27 12:52:10  nicb
; Initial Revision - (crude first version)
;
