;
; $Id: residual.orc,v 0.1 1999/12/27 10:56:17 nicb Exp nicb $
;
; residual.orc: 16 identical instruments which play the residual
;               of the voice files snipped together
; 
	sr=44100
	kr=44100
	ksmps=1
	nchnls=1

gasend	init	0


	instr 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16
	ifile=p1-1
	iskip=p4
	ithreshold=100
	isend=0.15
	iedge=p3*0.2
	idur=p3-(iedge*2)

aout	soundin	ifile,iskip
aout	dam	aout,ithreshold,0,1,0.01,1
aout	linen	aout,iedge,idur,iedge
;krms	rms	aout

;	if 	krms < ithreshold kgoto output
;aout	=	0

;output:
	out	aout
gasend	=	gasend+(aout*isend)

	endin

	instr 99
	idecaystart=p4
	idecayend=p5

ktime	line	idecaystart,p3,idecayend
arev	nreverb	gasend,ktime,0.4

	out	arev
gasend	=	0
	endin
;
; $Log: residual.orc,v $
; Revision 0.1  1999/12/27 10:56:17  nicb
; added 0 for higher signals
; added reverberation
;
; Revision 0.0  1999/12/24 17:55:14  nicb
; Initial Revision
;
