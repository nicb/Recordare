#
# $Id: produce-residual.awk 38 2014-02-12 14:44:53Z nicb $
#
# produce-residual.awk: this awk script picks up the .dat files
# residing in the text/segmentation/real/data directory and picks
# up all the wave spots where no spoken passage is emitted, thus
# picking up all the residual noise of each wave. With this, it
# produces a score
#
# usage:
#
# awkl -f produce-residual.awk [-v idx=n] <dat file>
#
# where <n> is the soundin number for this particular <dat file>
#
function print_sco_line(at, dur, skip, idx)
{
	printf("i%-4d %8.4f %6.4f %8.4f\n", idx, at, dur, skip);
}
function get_file_index()
{
	return __index__;
}
function get_output_pointer()
{
	return __cur_out_ptr__;
}
function set_output_pointer(p)
{
	__cur_out_ptr__ = p;
}
function produce_residual(skip, dur,
	out_ptr, file_idx, next_ptr)
{
	out_ptr = get_output_pointer();
	file_idx = get_file_index();

	print_sco_line(out_ptr, dur, skip, file_idx);
	next_ptr = out_ptr+dur;
	set_output_pointer(next_ptr);
}
function get_fragment_pointer()
{
	return __cur_frag_ptr__;
}
function set_fragment_pointer(p)
{
	__cur_frag_ptr__ = p;
}
function eval(at, dur,
	frag_ptr, res_dur)
{
	frag_ptr = get_fragment_pointer();
	if (at > frag_ptr)		# we have a residual fragment
	{
		res_dur = at - frag_ptr;
		produce_residual(frag_ptr, res_dur);
	}

	set_fragment_pointer(at+dur);
}
function print_header()
{
	print ";\n; score created automagically by $RCSFile$ $Revision: 0.0 $\n;";
}
BEGIN {
	FS="|";
	__index__ = idx ? idx : 1;
	__cur_out_ptr__ = 0;
	__cur_frag_ptr__ = 0;

	print_header();
}
/^#/ {
	# this is a comment, we pass it on the standard output changing
	# its comment argument
	gsub(/^#/, ";");
	print;
	next;
}
{
	temp[0] = split($0, temp, "|");
	eval(temp[2],temp[3]);

	delete_array(temp);
	next;
}
#
# $Log: produce-residual.awk,v $
# Revision 0.0  1999/12/24 16:55:30  nicb
# Initial Revision - functional
#
