<CsoundSynthesizer>
; vi:set nowrap:
; fog.csd: file to test the fog ugen
<CsOptions>
</CsOptions>
<CsInstruments>
	sr=44100
	kr=441
	ksmps=100
	nchnls=1

	instr 1
	;p4 = transposition factor
	;p5 = speed start
	;p6 = speed end
	;p7 = function table for grain data
	;p8 = start octaviation
	;p9 = end octaviation

i1	=	sr/ftlen(p6) ;scaling to reflect sample rate and table length
istart	=	i1*p5
iend	=	i1*p6
a1	line	istart,p3,iend ;index for speed
koct	line	p8,p3,p9
	;     xamp,xdens,xtra,xsp,koct,kband, kris, kdur,kdec, iolaps, ifna, ifnb, itotdur[, iphs[, itmode]
a2	fog	16000, 100, p4, a1,koct, 0,   .01,  .03, .01,    20,    p7,   2,     p3,       0,      1
	out	a2
	endin
</CsInstruments>
<CsScore>
f1 0 8192 11 35 1 1
f2 0 8192 19 1 1 270 1

i1 0 2  1   1   1  1 0 0	; straight operation
i1 3 2  1   0.1 2  1 0 0	; speed to 1/10
i1 6 2  0.1 1   1  1 0 0	; transposing to 1/10
i1 9 20 1   1   1  1 0 3	; going down ten octaves (octaviation)
</CsScore>
</CsoundSynthesizer>
