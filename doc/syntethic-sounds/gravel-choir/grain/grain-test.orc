	sr=44100
	kr=4410
	ksmps=10
	nchnls=1


 ;;;;;;;;;;;;;;; graintest.orc

	instr 1
	insnd = 10
	isrsnd= ftsr(insnd)
	iftlen=nsamp(insnd)
	irept=9
	ibasfrq = isrsnd/iftlen; Use original sample rate of insnd file
	iamp=ampdb(p4)
	ipitch=ibasfrq
	idens=p5
	igdurstart=p6
	igdurend=p7
	imaxgdur=(igdurstart > igdurend ? igdurstart : igdurend)

print	ipitch,ibasfrq,irept,iftlen,isrsnd
kgdur	line	igdurstart,p3,igdurend
ar	grain	iamp, ipitch, idens, 0, 0, kgdur, insnd, 5, imaxgdur, 1
	out	ar
	endin
