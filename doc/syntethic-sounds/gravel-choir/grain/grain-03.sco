;;;;;;;;;;;;;;; graintest.sco

f5 0 1024 20 2 ; Hanning window
;f5 0 1024 20 7 1 2 ; Kaiser window (not working)
;f5 0 1024 20 5 1; Blackman-Harris window
;f5 0 1024 20 4 1; Blackman window
;f5 0 1024 20 3 1; Bartlett window
;f5 0 1024 5 0.00001 10 1 1014 0.00001
;f5 0 1024 6 0 25 0.3 25 0.707 25 1 75 0.707 437 0.303 437 0
f10 0 512 1 "fof.wav" 0 0 0
;f10 0 65536 9 16 1 0
;
i100 0 12
;
; p4 = amp, p5 = density start, p6 = density end
; p7 = imaxgdur starting multiply
; p8 = imaxgdur ending   multiply
; p9 = distance in meters
; p10= left-right pan (1=left; 0=right)
;

;   at dur amp densst densend  gdurst gdurend dist      pan
i1   0 10   80	5	0.1	.001	4	4	0.5
