	sr=44100
	kr=4410
	ksmps=10
	nchnls=2

galeft	init	0			; reverb send
garight	init	0			; reverb send

 ;;;;;;;;;;;;;;; graintest.orc

    instr 1
    insnd = 10
    isrsnd= 44100
    ibasfrq = isrsnd / ftlen(insnd)   ; Use original sample rate of insnd file
    ibasfrq = ibasfrq/4
    imaxgdur = (ftlen(insnd)/isrsnd)
    istartdur = imaxgdur*p7
    imingdur = imaxgdur*p8
    idist = 1/p9
    irevsend=	0.01*(1/sqrt(p9))
    iamp = ampdb(p4)*idist
    iamp2 = iamp*0.5
    idensstart = p5
    idensend   = p6
    iofffrqstart=ibasfrq*4
    iofffrqend=ibasfrq*8
    iattack = p3*.3
    idecay  = p3*.01
    isus1 = (p3-iattack-idecay)/2
    isus2 = isus1
    idensatt=p3*.4
    idensdec=p3*.3
    idenssus=p3-idensatt-idensdec
    ipan = p10
    ileft= ipan
    iright=1-ipan

; formanti
ivoc	=	(p11-1) * 20
ismussa	=	0.01
iffun	=	12
iqstart	=	p12
iqend	=	p13

kq	line	iqstart,p3,iqend
display kq,0.5

if1     table	ivoc,iffun
kf1	=	if1
kf1     port	kf1, ismussa

ia1     table	ivoc+1,iffun
ka1	=	ia1			;kamp*iamp
ka1     port	ka1, ismussa*4

ib1     table	ivoc+2,iffun
kb1	=	ib1 *kq
kb1     port	kb1, ismussa

if2     table	ivoc+3,iffun
kf2	=	if1
kf2     port	kf1, ismussa

ia2     table	ivoc+4,iffun
ka2	=	ia2			;kamp*iamp
ka2     port	ka1, ismussa*4

ib2     table	ivoc+5,iffun
kb2	=	ib1 *kq
kb2     port	kb1, ismussa

if3     table	ivoc+6,iffun
kf3	=	if1
kf3     port	kf1, ismussa

ia3     table	ivoc+7,iffun
ka3	=	ia3			;kamp*iamp
ka3     port	ka1, ismussa*4

ib3     table	ivoc+8,iffun
kb3	=	ib3 *kq
kb3     port	kb3, ismussa

if4     table	ivoc+9,iffun
kf4	=	if4
kf4     port	kf4, ismussa

ia4     table	ivoc+10,iffun
ka4	=	ia4			;kamp*iamp
ka4     port	ka4, ismussa*4

ib4     table	ivoc+11,iffun
kb4	=	ib4 *kq
kb4     port	kb4, ismussa

if5     table	ivoc+12,iffun
kf5	=	if5
kf5     port	kf5, ismussa

ia5     table	ivoc+13,iffun
ka5	=	ia5			;kamp*iamp
ka5     port	ka5, ismussa*4

ib5     table	ivoc+14,iffun
kb5	=	ib5 *kq
kb5     port	kb5, ismussa

kamp   expseg   0.00001,iattack,iamp,isus1,iamp,isus2,iamp2,idecay,0.00001
kpitch line     ibasfrq, p3, ibasfrq * .8
kdens  expseg   1,idensatt,idensstart,idenssus,idensend,idensdec,1
kaoff  line     0, p3, iamp2
kpoff  line     iofffrqstart, p3, iofffrqend
kgdur  line 	istartdur, p3, imingdur
ar grain kamp, kpitch, kdens, kaoff, kpoff, kgdur, insnd, 5, imaxgdur, 0

af1	resonr	ar*ka1,kf1,kb1
af2	resonr	ar*ka2,kf2,kb2
af3	resonr	ar*ka3,kf3,kb3
af4	resonr	ar*ka4,kf4,kb4
af5	resonr	ar*ka5,kf5,kb5

ar	sum	af1,af2,af3,af4,af5

aleft	=	ar*ileft
aright	=	ar*iright

       outs	aleft,aright
       galeft	= galeft+(aleft*irevsend)
       garight	= garight+(aright*irevsend)
       endin

       instr 100
aleft  reverb2	galeft,1.2,0.6
aright reverb2	garight,1.5,0.7
       outs	aleft,aright
       galeft	= 0
       garight	= 0
       endin
