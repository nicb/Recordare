	sr=44100
	kr=4410
	ksmps=10
	nchnls=2

galeft	init	0			; reverb send
garight	init	0			; reverb send

 ;;;;;;;;;;;;;;; graintest.orc

    instr 1
    insnd = 10
    isrsnd= sr
    ibasfrq = isrsnd / ftlen(insnd)   ; Use original sample rate of insnd file
    ;ibasfrq = ibasfrq/4
    istartdur = p7
    ienddur = p8
    imaxgdur = (istartdur > ienddur ? istartdur : ienddur)
    idist = 1/p9
    irevsend=	0.01*(1/sqrt(p9))
    iamp = ampdb(p4)*idist
    iamp2 = iamp*0.5
    idensstart = p5
    idensend   = p6
    iofffrqstart=ibasfrq*4
    iofffrqend=ibasfrq*8
    iattack = p3*.00001
    idecay  = p3*.01
    isus1 = (p3-iattack-idecay)/2
    isus2 = isus1
    idensatt=p3*.4
    idensdec=p3*.3
    idenssus=p3-idensatt-idensdec
    ipan = p10
    ileft= ipan
    iright=1-ipan

kamp	expseg	0.00001,iattack,iamp,isus1,iamp,isus2,iamp2,idecay,0.00001
;kpitch	line	ibasfrq, p3, ibasfrq * .8
kpitch	line	ibasfrq, p3, ibasfrq
kdens	line	idensstart,p3,idensend
kaoff	line	0, p3, iamp2
kpoff	line	iofffrqstart, p3, iofffrqend
kgdur	line 	istartdur, p3, ienddur
ar	grain	kamp, kpitch, kdens, kaoff, kpoff, kgdur, insnd, 5, imaxgdur, 0
aleft	=	ar*ileft
aright	=	ar*iright
       outs	aleft,aright
       galeft	= galeft+(aleft*irevsend)
       garight	= garight+(aright*irevsend)
       endin

       instr 100
aleft  reverb2	galeft,1.2,0.6
aright reverb2	garight,1.5,0.7
       outs	aleft,aright
       galeft	= 0
       garight	= 0
       endin
