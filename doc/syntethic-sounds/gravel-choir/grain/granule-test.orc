	sr=44100
	kr=4410
	ksmps=10
	nchnls=1


 ;;;;;;;;;;;;;;; graintest.orc

	instr 1
	insnd = p10
	isrsnd= ftsr(insnd)
	iftlen=nsamp(insnd)
	iftdur=iftlen/isrsnd
	irandskip=iftdur*.2
	iamp=ampdb(p4)
	iratio=1
	imode=0
	ivoices=8
	igdurstart=p5
	igdurend=p6
	igapstart=p7
	igapend=p8
	ihigap=(igapstart > igapend ? igapstart : igapend)
	ilogap=(igapstart > igapend ? igapend : igapstart)
	isizerand=40
	iattack=1
	idecay=25
	ipitches=p9
	ipitch1=1.002
	ipitch2=0.008
	ipitch3=1.01
	ipitch4=0.09
	irise=p3*0.1
	irelease=p3*0.1


print	ihigap,ilogap
kgsize	line	igdurstart,p3,igdurend
kgap	oscil1	0,ihigap,p3,1
kgap	=	kgap+ilogap
	display	kgap,p3
	display kgsize,p3

ar	granule	iamp,ivoices,iratio,imode,0,insnd,ipitches,0,irandskip,\
		iftdur,kgap,30,kgsize,isizerand,iattack,idecay,ipitch1
ar	linen	ar,irise,p3,irelease

	out	ar
	endin
