; Produced automagically by  $Revision$
; DO NOT EDIT!
; vi:set nowrap:
f1  0  4096  10  1
f2  0  1024  19  .5  .5  270  .5
; vocali femminili
;f12 0 256 -2 650 1 69 1100 0.38 95 2860 0.23 95 3300 0.26 102 4500 0.11 120 0 0 0  0 0 500 1  75 1750 0.36 104 2450 0.3 123 3350 0.19 140 5000 0.07 165 0 0 0 0 0 330 1 89 2000 0.19 114 2800 0.28 132 3650 0.32 145 5000 0.11 162 0 0 0 0 0 400 1 86 840 0.24 109 2800 0.052 130 3250 0.06 138 4500 0.029 157 0 0 0 0 0 280 1 70 650 0.12 132 2200 0.004 156 3450 .003 224 4500 .002 272
;            |------------------------------a-----------------------------|            |-------------------------------e------------------------------|           |-------------------------------i------------------------------|           |-------------------------------o-------------------------------|           |-------------------------------u------------------------------|
f12 0 256 -2 650 1 69 1100 0.38 95 2960 0.23 95 3600 0.26 102 4900 0.11 120 0 0 0  0 0 500 1  75 1750 0.36 104 2550 0.3 123 3650 0.19 140 5400 0.07 165 0 0 0 0 0 330 1 89 2000 0.19 114 2900 0.28 132 3950 0.32 145 5400 0.11 162 0 0 0 0 0 400 1 86 840 0.24 109 2900 0.052 130 3550 0.06 138 4900 0.029 157 0 0 0 0 0 280 1 70 650 0.12 132 2300 0.004 156 3750 .003 224 4900 .002 272
; vocali maschili
;            |-------------------------------a-----------------------------|           |-------------------------------e----------------------------|           |--------------------------i------------------------------|           |---------------------------o----------------------------|           |-----------------------------u-----------------------------|
f13 0 256 -2 609 1 78 1000 0.5 88 2450 0.25 123 2700 0.28 128 3240 0.065 132 0 0 0 0 0 400 1 64 1700 0.35 81 2300 0.4 101 2900 0.28 119 3400 0.11 134 0 0 0 0 0 238 1 73 1741 .1 102 2450 .15 123 2900 .1 132 4000 .026 150 0 0 0 0 0 325 1 73 700 .25 80 2550 .05 125 2850 .08 131 3100 .04 135 0 0 0 0 0 360 1 51 750 .25 61 2400 .034 168 2675 .048 184 2950 .017 198
;
; voice for note e3 (a)
i1  0.0000 23.77  8.04 1 13 96.0
