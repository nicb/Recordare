; vi:set nowrap:
	sr = 44100
	kr = 441
	ksmps = 100

	instr     1
	; FOF parameters
	iamp  = ampdb(p7)
	irise = p3*0.2
	idec  = p3*0.3
	idur  = p3-irise-idec
	ifrq  = cpspch(p4)
	ifrqside = ifrq*0.96
	ivibamp = 2.5
	ioctstart=p8
	ioctend=p9
	ioctrnd=0.2

	iol = 1000			; grain overlaps
	igrise = 0.003			; grain attack
	igdur = 0.02			; grain duration
	igdec = 0.007			; grain decay
kamp	linseg	0,irise,iamp,idur,iamp,idec,0
kvibamp expseg	0.0001,1,0.0001,(p3-1)*.2,ivibamp,(p3-1)*.8,ivibamp
kvib	oscil   kvibamp,  5,  1
kfrq	expseg  ifrqside, p3*.01, ifrq, p3*.98, ifrq, p3*.01,ifrqside
koct1	line	0.00001+(ioctstart*(1+birnd(ioctrnd))),p3,0.00001+(ioctend*(1+birnd(ioctrnd)))
koct2	line	0.00001+(ioctstart*(1+birnd(ioctrnd))),p3,0.00001+(ioctend*(1+birnd(ioctrnd)))
koct3	line	0.00001+(ioctstart*(1+birnd(ioctrnd))),p3,0.00001+(ioctend*(1+birnd(ioctrnd)))
koct4	line	0.00001+(ioctstart*(1+birnd(ioctrnd))),p3,0.00001+(ioctend*(1+birnd(ioctrnd)))
koct5	line	0.00001+(ioctstart*(1+birnd(ioctrnd))),p3,0.00001+(ioctend*(1+birnd(ioctrnd)))

; formanti
ivoc	=	(p5-1) * 20
ismussa	=	0.01
iffun	=	p6
iq	=	1


if1     table	ivoc,iffun
kf1	=	if1
kf1     port	kf1, ismussa

ia1     table	ivoc+1,iffun
ka1	=	kamp*ia1
ka1     port	ka1, ismussa*4

ib1     table	ivoc+2,iffun
kb1	=	ib1 *iq
kb1     port	kb1, ismussa

;               amp    frq     ffrq oct  bw   ris  dur dec  ol f1 f2 dur ph 
aform1	fof	ka1, kfrq+kvib, kf1,koct1,kb1,igrise,igdur,igdec,iol, 1, 2, p3, 0, 0

if2     table	ivoc+3,iffun
kf2	=	if2
kf2     port	kf2, ismussa

ia2     table	ivoc+3+1,iffun
ka2	=	kamp*ia2
ka2     port	ka2, ismussa*4 

ib2     table	ivoc+3+2,iffun
kb2	=	ib2*iq
kb2     port	kb2, ismussa

;               amp    frq     ffrq  oct bw   ris  dur dec ol f1 f2 dur ph 
aform2	fof	ka2, kfrq+kvib, kf2,koct2,kb2,igrise,igdur,igdec,iol, 1, 2, p3, 0.7, 0

if3     table	ivoc+6,iffun
kf3	=	if3
kf3     port	kf3, ismussa

ia3     table	ivoc+6+1,iffun
ka3	=	ia3*kamp
ka3     port	ka3, ismussa*4 

ib3     table	ivoc+6+2,iffun
kb3	=	ib3*iq
kb3     port	kb3, ismussa

;               amp    frq     ffrq  oct bw   ris  dur dec ol f1 f2 dur ph 
aform3	fof	ka3, kfrq+kvib, kf3,koct3,kb3,igrise,igdur,igdec,iol, 1, 2, p3, 1.35, 0

if4     table	ivoc+9,iffun
kf4	=	if4
kf4     port	kf4, ismussa

ia4     table	ivoc+9+1,iffun
ka4	=	ia4*kamp
ka4     port	ka4, ismussa*4 

ib4     table	ivoc+9+2,iffun
kb4	=	ib4*iq
kb4     port	kb4, ismussa

;               amp    frq     ffrq  oct bw   ris dur dec ol f1 f2 dur ph 
aform4	fof	ka4, kfrq+kvib, kf4,koct4,kb4,igrise,igdur,igdec,iol, 1, 2, p3, 1.48, 0

if5     table	ivoc+12,iffun
kf5	=	if5
kf5     port	kf5, ismussa

ia5     table	ivoc+12+1,iffun
ka5	=	ia5*kamp
ka5     port	ka5, ismussa*4 

ib5     table	ivoc+12+2,iffun
kb5	=	ib5*iq
kb5     port	kb5, ismussa

;               amp    frq     ffrq  oct bw   ris dur dec ol f1 f2 dur ph 
aform5	fof	ka5, kfrq+kvib, kf5,koct5,kb5,igrise,igdur,igdec,iol, 1, 2, p3, 2.24, 0

afofsum	sum	aform1,aform2,aform3,aform4,aform5

	; grain part
	insnd = 10
	isrsnd= sr
	ilargerfact = (ioctstart > ioctend ? ioctstart : ioctend) 
	iampfact = 16384/ilargerfact
	ibasfrq = isrsnd / ftlen(insnd)   ; Use original sample rate of insnd file
	ibasfrq = ibasfrq/4
	imaxgdur = (ftlen(insnd)/isrsnd)
	istartdur = imaxgdur*1
	imingdur = imaxgdur*0.1
	idist = 1/(0.5+rnd(0.5))
	iampstart = ioctend*iampfact		; opposite to fof
	iampend = ioctstart*iampfact
	idensstart = ioctend
	idensend   = ioctstart
	iattack = p3*.3
	idecay  = p3*.01
	isus1 = (p3-iattack-idecay)/2
	isus2 = isus1
	idensatt=p3*.4
	idensdec=p3*.3
	idenssus=p3-idensatt-idensdec

kamp	line	iampstart,p3,iampend
kpitch	line    ibasfrq, p3, ibasfrq * .8
kdens	linseg  1,idensatt,idensstart,idenssus,idensend,idensdec,1
kgdur	line 	istartdur, p3, imingdur
agrain	grain	kamp, kpitch, kdens, 0, 300, kgdur, insnd, 5, imaxgdur, 0

	; output
	out	afofsum+agrain
	endin
