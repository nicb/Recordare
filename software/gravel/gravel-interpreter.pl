#!/usr/bin/perl -I Recordare
# vi:set nowrap ts=4:
#
# gravel-interpreter.pl,v 0.1 2000/07/02 16:52:48 nicb Exp
#
# 
#

use Tools;
use Gravel2Csound;
use Globals;

use strict;

#
# start of main
#

srand(140856);							# set a fixed random seed

my $file = shift || 'gravel.data';
my $globalfile = shift || 'global.data';
my $g = new Globals($globalfile);

my $gi = new Gravel2Csound($file);

$gi->print_header();
$gi->print();
#
# end of main
#
#
# gravel-interpreter.pl,v
# Revision 0.1  2000/07/02 16:52:48  nicb
# adjusted to normal paths
#
# Revision 0.0  2000/06/27 11:24:49  nicb
# Initial Revision - taken from last draft-0 revision
#
