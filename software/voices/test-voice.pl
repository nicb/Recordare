#!/usr/bin/perl
# vi:set nowrap ts=4:
#
# $Id$
#
# 
#

use Recordare::Speakers;
use Recordare::Index;
use Recordare::OutputBase;
use Recordare::Paths;

use strict;

#
# start of main
#

$main::ref_idx = 0;
$main::offset = 7.58;

my $paths = new Paths();

$paths->data_path('../segmentation/real/data');

my $Speakers = new Speakers([ 'catbassa' ]);
my $num_words = $Speakers->size($main::ref_idx);
my $i = 0;

for ($i = 0; $i < $num_words; ++$i)
{
	process_word($i, $Speakers, $main::offset);
}

#
# end of main
#

sub process_word($$$)
{
	my ($idx, $speakers, $offset) = @_;
	my $speaker = $speakers->speaker($main::ref_idx);
	my $word = $speaker->word($idx);
	my $dur  = $word->dur();
	my $at   = $word->action_time() + $offset;
	my $output = new OutputBase($at,$dur,$speaker,$word);

	return $output->print();
}
#
# $Log$
