#!/usr/bin/perl -I Recordare
# vi:set nowrap ts=4:
#
# vf-interpreter.pl,v 0.1 2000/06/21 17:24:18 nicb Exp
#
# 
#

use Recordare::Score2VoiceForm;
use Recordare::Paths;
use Recordare::Globals;

use strict;

#
# start of main
#

	srand(140856);							# set a fixed random seed

	my $paths = new Paths();
	$paths->data_path('../../../segmentation/real/data');
	my $file = shift || 'score.data';
	my $globalfile = shift || 'global.data';

	my $globals = new Globals($globalfile);
	my $si = new Score2VoiceForm($file);

	$si->print_head();
	$si->print();
	$si->print_tail();
#
# end of main
#
#
# vf-interpreter.pl,v
# Revision 0.1  2000/06/21 17:24:18  nicb
# added same rand() seed than score-interpreter.pl
#
# Revision 0.0  2000/06/11 08:53:10  nicb
# Initial Revision
#
