# vi:set nowrap:
#
## score.data,v 0.13 2000/07/02 16:56:51 nicb Exp
#
# Recordare - Nicola Bernardini
#
# Score data
#
# the data is in the following format:
#
# any line beginning with a '#' (hash character) is a comment
#
# any line beginning with a '##' (double hash character) is a comment
# which will be carried into the produced csound score
#
# other lines are:
#
# pipe-separated voice records with the following data:
#
# voice|<at>|<word-range>|<ref. speaker>|<speaker list>|<envelope list>|<pos.>|<sdur>
#
# where:
#
# voice		= type recognition for voice
# at 		= action time for this line (in seconds, relative within the
#		  section)
# word range	= the range of words to be produced (in the format nn-nn)
#                 (words are a 0-offset array)
# ref. speaker	= the reference speaker to use as timing template
# speaker list	= a list of speakers to be used, in the following format:
#		  <speaker name>:<probability percentage>,...
# env. list	= a list of envelope points in the following format:
#		  <dB>:<time>,<dB>:<time>,...
#		  where <dB> is an attenuation factor in dB (0=unity gain)
#		  and time is in seconds and is relative to the beginning
#		  of the excerpt; when time is negative it is taken
#		  to be from the end of the excerpt; 'END' as time tag means
#                 the end of the excerpt
# pos		= is a sequence of colon-separated x,y positions for this
#                 fragment in the following format:
#		  <x>,<y>:<x>,<y>, where the virtual listener is a 0,0,
#		  and values are in meters
# sdur		= segment duration (duration of each voice segment)
#
# pipe-separated section records with the following data:
#
# section|<dur>|<info>
#
# where:
#
# section	= type recognition for section
# dur		= global duration of the section
# info		= info about the section
#
##
## section 1 (csound section 1)
##
section|40.490|Coro I (3+1)
voice|15.290|0-28|carlocarlo|carlocarlo:61.8,cecilia:23.6,titino2-soft:9,titino1:5.6|2:0|0,4.5:0,2.8|0.180
voice|16.339|0-28|ninolent|ninolent:61.8,giuliano:23.6,carlo-padre:14.6|0:0|4.1,6.0:1.2,2.5|0.160
voice|20.581|0-28|catbassa|catbassa:61.8,clcbassa:23.6,silvia:14.6|0:0|-4.1,6.0:-1.2,2.5|0.201
voice|17.068|0-28|carlocarlo|nicbsuss:50,clcsuss:50|-90:0,-2:1,-2:-3,-90:END|0,4.5:0,2.8|0.208
##
## section 2+3+4 (csound section 2)
##
section|63.704|Donna - Coro II (3+1) - Donna
voice|00.000|29-66|clcbassa|clcbassa:61.8,silvia:23.6,catbassa:9,clcalta:5.6|2:0|0,2.8:0,2.8|0.201
##
voice|24.999|67-89|giuliano|giuliano:61.8,carlo-padre:23.6,ninovel:14.6|0:0|0,4.5:0,2.8|0.199
voice|26.510|67-89|silvia|silvia:61.8,catbassa:23.6,clcbassa:14.6|0:0|-4.1,6.0:-1.2,2.5|0.204
voice|29.561|75-89|cecilia|cecilia:61.6,titino1:23.6,carlocarlo:14.6|-3:0,0:3,0:END|4.1,6.0:1.2,2.5|0.180
voice|25.308|67-89|silvia|nicbsuss:50,clcsuss:50|-90:0,-2:2,-2:-2,-90:END|0,4.5:0,2.8|0.199
##
voice|42.440|90-118|clcalta|clcalta:61.8,silvia:23.6,catbassa:9,clcbassa:5.6|2:0|0,4.5:0,2.8|0.202
##
## section 5 (csound section 3)
##
section|18.122|Uomo
voice|0|119-148|catbassa|carlo-padre:38.2,ninovel:38.2,giuliano:14.6|3:0|0,2.8:0,2.8|0.175
##
## section 6 (csound section 4)
##
section|24.737|Coro III (5+1)
voice|00.000|149-177|carlocarlo|carlocarlo:61.8,cecilia:23.6,titino2-soft:9,titino1:5.6|0:0|0,4.5:0,2.8|0.180
voice|03.214|149-177|silvia|silvia:61.8,catbassa:23.6,clcbassa:14.6|0:0|4.1,6.0:1.2,2.5|0.2
voice|04.531|149-177|clcbassa|ninolent:61.8,carlo-padre:23.6,giuliano:14.6|0:0|-4.1,6.0:-1.2,2.5|0.230
##
voice|06.873|160-177|cecilia|cecilia:61.8,titino2-soft:23.6,carlocarlo:14.6|0:0|3.1,3.3:1.2,2.5|0.180
voice|10.442|160-177|clcbassa|giuliano:61.8,ninovel:23.6,carlo-padre:14.6|0:0|-3.1,3.3:-1.2,2.5|0.160
##
voice|01.262|149-177|carlocarlo|nicbsuss:50,clcsuss:50|-2:0|0,4.5:0,2.8|0.199
##
## section 7+8 (csound section 5)
##
section|55.825|Bambino - Donna
voice|00.000|178-212|cecilia|titino2-soft:61.8,cecilia:23.6,carlocarlo:14.6|2:0|0,2.8:0,2.8|0.180
voice|31.245|213-245|silvia|silvia:61.8,catbassa:23.6,clcbassa:9,clcalta:5.6|1:0|0,2.8:0,2.8|0.2
##
## section 9 (csound section 6)
##
section|22.662|Coro IV (5+1)
voice|00.450|246-270|cecilia|cecilia:61.8,carlocarlo:23.6,titino1:14.6|0:0|0,4.5:0,2.8|0.180
voice|01.031|246-270|clcbassa|carlo-padre:61.8,giuliano:23.6,ninolent:14.6|0:0|4.1,6.0:1.2,2.5|0.160
voice|02.964|246-270|clcalta|clcalta:61.8,silvia:23.6,catbassa:14.6|0:0|-4.1,6.0:-1.2,2.5|0.202
##
voice|05.912|255-270|cecilia|cecilia:61.8,titino2-soft:23.6,carlocarlo:14.6|0:0|3.1,3.3:1.2,2.5|0.180
voice|08.921|255-270|silvia|silvia:61.8,catbassa:23.6,clcbassa:14.6|0:0|-3.1,3.3:-1.2,2.5|0.199
##
voice|00.000|246-270|cecilia|nicbsuss:50,clcsuss:50|-90:0,-2:2,-2:-2,-90:END|0,4.5:0,2.8|0.1995
##
## section 10 (csound section 7)
##
section|34.685|Bambino
voice|0|271-310|cecilia|cecilia:61.8,carlocarlo:23.6,titino2-soft:14.6|1:0|0,2.8:0,2.8|0.180
##
## section 11 (csound section 8)
##
section|42.020|Coro V (8+1)
voice|00.000|311-348|titino2-soft|titino2-soft:61.8,carlocarlo:23.6,cecilia:9,titino1:5.6|0:0|0,4.5:0,2.8|0.180
voice|08.908|311-348|cecilia|cecilia:61.8,titino1:23.6,titino2-soft:9,carlocarlo:5.6|0:0|-4.1,6.0:-1.2,2.5|0.180
voice|11.046|311-348|carlocarlo|carlocarlo:61.8,cecilia:23.6,titino1:9,titino2-soft:5.6|0:0|4.1,6.0:1.2,2.5|0.180
voice|11.942|311-348|clcalta|clcalta:61.8,silvia:23.6,catbassa:14.6|0:0|3.1,3.3:3.1,3.3|0.202
voice|16.206|311-348|ninovel|ninovel:61.8,carlo-padre:23.6,giuliano:14.6|0:0|-3.1,3.3:-3.1,3.3|0.2
##
voice|21.330|325-348|titino1|titino1:61.8,carlocarlo:23.6,cecilia:9,titino2-soft:5.6|0:0|-6.8,2.7:-1.2,2.5|0.180
voice|23.524|325-348|carlo-padre|carlo-padre:61.8,giuliano:23.6,ninovel:9,ninolent:5.6|0:0|6.8,2.7:1.2,2.5|0.197
##
voice|30.496|334-348|silvia|silvia:61.8,catbassa:23.6,clcbassa:9,clcalta:5.6|0:0|0.0,2.8:0.0,2.8|0.203
##
voice|01.174|311-348|titino2-soft|nicbsuss:50,clcsuss:50|-90:0,-2:5,-2:-5,-90:END|0,2.8:0,2.8|0.180
##
## sections 12 13 14 - coda (csound section 9)
##
section|88.849|Uomo - Donna - Coro VI (13+1)
##
voice|00.000|349-365|clcbassa|giuliano:38.2,ninolent:38.2,carlo-padre:23.6|1:0|0,2.8:0,2.8|0.160
##
voice|14.278|366-387|catbassa|catbassa:61.8,clcbassa:23.6,silvia:9,clcalta:5.6|1:0|0,2.8:0,2.8|0.198
##
voice|25.243|388-454|cecilia|carlocarlo:61.8,titino2-soft:23.6,cecilia:14.6|0:0|-4.1,6.0:-1.2,2.5|0.180
voice|26.927|388-454|cecilia|cecilia:61.8,carlocarlo:23.6,titino1:14.6|0:0|4.1,6.0:1.2,2.5|0.180
##
voice|26.496|388-457|cecilia|carlocarlo:61.8,cecilia:23.6,titino2-soft:14.6|-3:0,0:30,+6:END|0,4.5:0,2.8|0.195
##
voice|29.374|388-454|ninolent|carlo-padre:61.8,giuliano:23.6,ninovel:14.6|0:0|-6.8,2.7:-1.2,2.5|0.160
voice|30.412|388-454|ninolent|ninolent:61.8,carlo-padre:23.6,giuliano:14.6|0:0|6.8,2.7:1.2,2.5|0.160
voice|32.769|388-454|silvia|silvia:61.8,catbassa:23.6,clcbassa:14.6|0:0|0,2.8:0,2.8|0.202
voice|34.471|388-454|clcbassa|clcbassa:61.8,silvia:23.6,catbassa:14.6|0:0|-4.1,6.0:1.2,2.5|0.202
voice|38.481|388-454|catbassa|catbassa:61.8,clcbassa:23.6,silvia:14.6|0:0|4.1,6.0:1.2,2.5|0.202
##
voice|49.846|414-454|cecilia|titino1:61.8,cecilia:23.6,carlocarlo:14.6|0:0|0,4.5:0,2.8|0.180
voice|54.549|414-454|clcbassa|clcbassa:61.8,silvia:23.6,catbassa:14.6|0:0|-6.8,2.7:-1.2,2.5|0.21
voice|54.473|414-454|clcbassa|giuliano:61.8,ninolent:23.6,carlo-padre:9,nicbbassa:5.6|0:0|6.8,2.7:1.2,2.5|0.160
##
voice|64.737|431-454|cecilia|cecilia:61.8,carlocarlo:23.6,titino2-soft:14.6|0:0|-4.1,6.0:-1.2,2.5|0.180
voice|67.394|431-454|silvia|silvia:61.8,catbassa:23.6,clcbassa:14.6|0:0|4.1,6.0:1.2,2.5|0.231
#
voice|30.299|388-454|cecilia|nicbsuss:50,clcsuss:50|-90:0,-2:5,-2:-5,-90:END|0,4.5:0,2.8|0.201
