% vi:set nowrap:
%
% $Id: positions.m,v 0.0 2000/06/02 16:12:30 nicb Exp $
%
% This is a matlab file to calculate in degrees all soloist positions
% in Recordare
% 
Moduli = [ 5 8 13 ];
phi=(sqrt(5)+1)/2;
alpha=(2*pi)/(phi**2);
gamma=alpha/(phi**3);
beta=gamma/(phi**2);

Angles = [ alpha beta gamma ]';
degfactor = 360/(2*pi);

Degs = Angles * degfactor
A = [
	sin(-alpha/2) cos(alpha/2);
	sin(alpha/2)  cos(alpha/2);
	sin(-alpha/4) cos(alpha/4);
	sin(alpha/4) cos(alpha/4);
];

Positions = [
	Moduli(1)*A(1,1) Moduli(1)*A(1,2);
	0                Moduli(1);
	Moduli(1)*A(2,1) Moduli(1)*A(2,2);

	Moduli(2)*A(1,1) Moduli(2)*A(1,2);
	0                Moduli(2);
	Moduli(2)*A(2,1) Moduli(2)*A(2,2);

	Moduli(3)*A(1,1) Moduli(3)*A(1,2);
	Moduli(3)*A(3,1) Moduli(3)*A(3,2);
	Moduli(3)*A(4,1) Moduli(3)*A(4,2);
	Moduli(3)*A(2,1) Moduli(3)*A(2,2);
]
%
% $Log: positions.m,v $
% Revision 0.0  2000/06/02 16:12:30  nicb
% Initial Revision
%
